   OPT f+ h-

    dta $92,$0d,$36,$00

.array msg_0[160] .byte = 0
   [0]   = "          Your name is Flob.            "
   [40]  = "            You came to be              "
   [80]  = "       from a tiny drop of slime        "
   [120] = "       invented by Dr Sven Torff.       "
.enda  

    dta $d2,$1d,$36,$00

.array msg_1[160] .byte = 0
   [0]   = "      Defenceless and vulnerable,       "
   [40]  = "        you die within minutes          "
   [80]  = "    without regular doses of slime.     "
   [120] = "   But things aren't as they seem...    "
.enda  

    dta $d2,$1d,$36,$00

.array msg_2[160] .byte = 0
   [0]   = "       You can turn excess slime        "
   [40]  = "    into a huge gravitational wave      "
   [80]  = "   which turns the world upside down.   "
   [120] = "               Verbatim.                "
.enda  

    dta $14,$ed,$62,$00

.array msg_3[160] .byte = 0
   [0]   = "   When Dr Torff realised your power,   "
   [40]  = "  he decided to hide the slime formula  "
   [80]  = "   and its secret ingredients in four   "
   [120] = "     remote spots around the world.     "
.enda  

    dta $82,$1d,$36,$00

.array msg_4[160] .byte = 0
   [0]   = " Your only clues about the whereabouts  "
   [40]  = " of the ingredients, are drops of slime "
   [80]  = "         left by Doctor Torff.          "
   [120] = "      Collect them to regenerate.       "
.enda  

    dta $32,$3d,$36,$00

.array msg_5[160] .byte = 0
   [0]   = "      Find all secret ingredients       "
   [40]  = "         and the slime formula.         "
   [80]  = "            Become immortal             "
   [120] = "          and rule the world!           "
.enda  



