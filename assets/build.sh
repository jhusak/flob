#!/bin/bash

./_convertDir.sh ./
./_convertDir.sh credits/
./_convertDir.sh intro/
./makestatheader.sh

dd if=_flob_theme.rmt of=tmp.rmt bs=1 skip=6
apultra tmp.rmt flob_theme.apu
rm tmp.rmt

