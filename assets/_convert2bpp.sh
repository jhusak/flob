for IMAGE in $(ls -1 $1)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE[0] -depth 2 -colorspace gray $NAME.gray
    #python swapcolors2bpp.py $NAME.gray $NAME.gray1 1 3
    #python swapcolors2bpp.py $NAME.gray1 $NAME.gfx 1 2
    mv $NAME.gray $NAME.gfx
    ./rle.exe $NAME.gfx $NAME.rle 
    ./makeLZ4.sh $NAME.gfx $NAME.lz4
    ./apultra $NAME.gfx $NAME.apu
    rm $NAME.gray*
done

