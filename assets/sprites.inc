var    
    flobFrames0: array [0..15] of pointer absolute SPRITES_ADDRESS;
    flobFrames1: array [0..15] of pointer absolute SPRITES_ADDRESS+32;
    larrow: array [0..29] of byte absolute SPRITES_ADDRESS+64;
    rarrow: array [0..29] of byte absolute SPRITES_ADDRESS+64+30;
    ach_cursor: array [0..4, 0..11] of byte absolute SPRITES_ADDRESS+64+60;
    ach_off: array [0..31] of byte absolute SPRITES_ADDRESS+64+60+(5*12);
    ach_on: array [0..31] of byte absolute SPRITES_ADDRESS+64+60+(5*12)+32;
    ach_new: array [0..31] of byte absolute SPRITES_ADDRESS+64+60+(5*12)+64;

