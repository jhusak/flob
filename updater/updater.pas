program flob_updater;
{$librarypath '../../blibs/'}
uses atari, crt, b_maxFlash8Mb, sysutils; // b_utils;

const
{$i const.inc}
{$r resources.rc}
{ $i types.inc}
{ $i interrupts.inc}

label pgmend;

var
    sector:byte;
    bank:byte;
    fileNum:byte;
    sectorFiles:array [0..7] of byte;
    sectorFilesCount:byte;
    filename:TString;
    buffer: array[0..8191] of byte absolute BANK_BUFFER;

    GINTLK: byte absolute $03FA;  // @nodoc 
    TRIG3: byte absolute $D013;   // @nodoc 

function IsFlobCartIn:boolean;
begin
    result:=true;
    wsync:=0;
    SetBank(0);
    GINTLK := TRIG3; 
    if Peek(VER_MAIN_ADDR) > 1 then result:=false;
    if Peek(VER_MAJOR_ADDR) > 9 then result:=false;
    if Peek(VER_MINOR_ADDR) > 9 then result:=false;
    if Peek(HEADER_0) <> 0 then result:=false;
    if Peek(HEADER_4) <> 4 then result:=false;
end;

function GetCartVersion:TString;
var a,b,c:byte;
begin
    result:=' .  ';
    wsync:=0;
    SetBank(0);
    GINTLK := TRIG3; 
    a := Peek(VER_MAIN_ADDR);
    b := Peek(VER_MAJOR_ADDR);
    c := Peek(VER_MINOR_ADDR);
    result[1] := char(a+48);
    result[3] := char(b+48);
    result[4] := char(c+48);
end;

procedure BuildFilename(b:byte);
begin
    Str(b,filename);
    filename := Concat('D:BANK_',filename);
    filename := Concat(filename,'.BNK');
end;


procedure GetSectorFiles(sec:byte);
var basenum:byte;
    bnk:byte;
begin
    basenum := sec shl 3;
    bnk := 0;
    sectorFilesCount:=0;
    repeat 
        sectorFiles[bnk] := 0;
        BuildFilename(basenum);
        //Writeln(filename);
        if FileExists(filename) then begin
            sectorFiles[bnk] := 1;
            Inc(sectorFilesCount)
        end; //else Writeln('file not found: ',filename);
        
        inc(basenum);
        inc(bnk);
    until bnk = 8;
end;

procedure ReadBankFile(num:byte;address:word);
var f:file;
    numread:word;
begin   
    BuildFilename(num);
    Assign(f, filename); 
    reset(f, 256);
    blockread(f, buffer, 32, NumRead);
    //writeln(ioresult, ',', numread);
    close(f);
end;

function GetDiskVersion:TString;
var a,b,c:byte;
begin
    result:=' .  ';
    ReadBankFile(0,BANK_BUFFER);
    a := Peek(BANK_BUFFER + $1FF7);
    b := Peek(BANK_BUFFER + $1FF8);
    c := Peek(BANK_BUFFER + $1FF9);
    result[1] := char(a+48);
    result[3] := char(b+48);
    result[4] := char(c+48);
end;



function Verify(vbank:byte;src,dest,len:word):boolean;
begin
    wsync:=0;
    SetBank(vbank);
    GINTLK := TRIG3; 
    result := CompareMem(pointer(src),pointer(dest),len)
end;

begin

    Pause;
    savmsc := VIDEO_RAM_ADDRESS;  // set custom video address
    SDLSTL := DISPLAY_LIST_ADDRESS;
        
    FillByte(pointer(VIDEO_RAM_ADDRESS),40*25,0);
    CursorOff;
    GotoXY(3,1);
    Writeln('Flob cartridge updater 1.0'*);
    Writeln;
    if not IsFlobCartIn then begin
        Writeln('This is (probably) not a genuine');
        Writeln('FloB cartridge.');
        Writeln;
        Writeln('Do you want to continue? (y/n)');
        Writeln;
        if readkey <> 'y' then goto pgmend;
    end else begin
        Write('Version detected on cart:   ');
        Writeln(GetCartVersion);
    end;
    Write('New version to be uploaded: ');
    Writeln(GetDiskVersion);
    Writeln;
    Writeln('Do you want to continue? (y/n)');
    Writeln;
    if readkey <> 'y' then goto pgmend;
    Writeln('Upgrade started:');
    Writeln('Do not turn your ATARI off now!'*);
    Writeln;
    
    sector:=0;
    
    repeat
        Write('Sector: ');
        Write(sector);
        GetSectorFiles(sector);
        Write(' - Banks to update: ');
        Writeln(sectorFilesCount);
        
        if sectorFilesCount>0 then begin
            Write('Erasing Sector ',sector);
            EraseSector(sector);
            Writeln(' - Done.');
            bank:=0;
            repeat
                if sectorFiles[bank]<>0 then begin
                    fileNum := (sector shl 3) + bank;
                    Write('BANK:',fileNum,' ');
                    Write('READ'*' ');
                    ReadBankFile(fileNum,BANK_BUFFER);
                    Write('WRITE'*' ');
                    asm sei end;
                        BurnPages(sector,Hi(BANK_BUFFER),bank shl 5,32);
                    //BurnBlock(fileNum,BANK_BUFFER,$A000,$2000);
                    asm cli end;
                    Write('VERIFY'*' ');
                    if not Verify(filenum,$a000,BANK_BUFFER,$2000) then 
                        Writeln('KO!!!')
                    else Writeln('OK');
                end;
                inc(bank);
            until bank = 8;
        
        end;
        inc(sector);
    until sector = 7;
    Writeln;
    Pause;
    SetBank(0);
    GINTLK := TRIG3;     
    Writeln('Operation finished! Press '+'RESET'*);
    ReadKey;
    
pgmend:

    TextMode(0);
    ClrScr;
    Pause;
end.
