#!/bin/bash 
source buildconfig.conf
NAME=flob
cp $NAME.xex cart_builder
cp starter/start.xex cart_builder
cd cart_builder
./build.sh $NAME.xex 
cp ready.bin ../$NAME.bin
cd ..
$EMU_EXE //cart $NAME.bin //cartmapper 42
