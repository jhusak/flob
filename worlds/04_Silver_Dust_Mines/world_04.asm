    OPT f+ h-
    icl '../../const.inc'
    org $a000
world_start ; -- mines --  
    
    dta 27 ; number of levels             > | < 
    dta WORLD_PLAYABLE ; state
    dta 17,"Silver Dust Mines               "  ; name (keep length untouched)
    dta 30, 60 ; starting x, starting y, starting room
    dta 4; 4 ; starting room (4)
    dta a($0900) ; starting slime amount
    dta 165 ; total slime count
    dta 2 ; secretsCount
    dta a(195); timeTreshold: word;
    dta a(14000) ;scoreTreshold: cardinal;    
    dta $6c,$68 ; goal colors
    dta 0; world bank (leave 0 here);
    dta 1; assets bank offset;
    dta 2; image bank offset;
    dta a(icon); icon pointer
    dta a(rmt); rmt pointer
    
    dta 48 ; iconFlobX
    dta 45 ; iconFlobY
    dta 1  ; iconFlobFrame
    
    ;;; ROOM POINTERS
    dta a(room_00)
    dta a(room_01)
    dta a(room_02)
    dta a(room_03)
    dta a(room_04)
    dta a(room_05)
    dta a(room_06)
    dta a(room_07)
    dta a(room_08)
    dta a(room_09)
    dta a(room_10)
    dta a(room_11)
    dta a(room_12)
    dta a(room_13)
    dta a(room_14)
    dta a(room_15)
    dta a(room_16)
    dta a(room_17)
    dta a(room_18)
    dta a(room_19)
    dta a(room_20)
    dta a(room_21)
    dta a(room_22)
    dta a(room_23)
    dta a(room_24)
    dta a(room_25)
    dta a(room_26)
    
;;; goals  (trigger -> obj)

;; 0 - switch -> light in secret 2  
;; 1 - bulb -> doors to secret 1 
;; 2 - lever -> floor cover  
;; 3 - upper console -> krata hatch  
;; 4 - button -> bridge 
;; 5 - lower console -> cable 
;; 6 - valve -> water
;; 7 - main goal vial 
;; 8 - big console -> light in tunels
;; 9 - big slime 1
;; a - big slime 2
;; b - 
;; c - 
;; d - 


    
icon
    dta $96, $ca, $62 ; icon colors
    ins 'icon.gfx'

room_00    
    ;;  c0   c1   c2   c4
    dta $16, $0e, $52, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 1, 4, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 0;    

    dta PUT_SPRITES, a(sprite_bat) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 8       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY    ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 0     ;    flipable: array[0..1] of byte;
    :2 dta 12   ;    x: array[0..1] of byte;
    :2 dta 25   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 1     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_bat1     ;    param1: array[0..1] of byte; // goal
    :2 dta >path_bat1     ;    param2: array[0..1] of byte; // room

    dta PUT_SLIME,$3E,$09,0
    dta PUT_SLIME,$54,$0B,1
    dta PUT_SLIME,$39,$0F,2
    dta PUT_SLIME,$B3,$10,3
    dta PUT_SLIME,$C2,$13,4

    dta LEVEL_END


room_01    
    ;;  c0   c1   c2   c4
    dta $18, $0e, $12, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 2, 5, 0    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 1;    

    dta PUT_SPRITES, a(sprite_bat) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 8       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY    ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 60    ;    x: array[0..1] of byte;
    :2 dta 11   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 1     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_bat2     ;    param1: array[0..1] of byte; // goal
    :2 dta >path_bat2     ;    param2: array[0..1] of byte; // room

    dta PUT_SLIME,$9D,$06,5
    dta PUT_SLIME,$9D,$0B,6
    dta PUT_SLIME,$B6,$0E,7
    dta PUT_SLIME,$3E,$0F,8
    dta PUT_SLIME,$73,$11,9
    dta PUT_SLIME,$C3,$12,10

    dta LEVEL_END

room_02    
    ;;  c0   c1   c2   c4
    dta $26, $6a, $92, $70 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 3, 6, 1    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 2;    

    dta PUT_SPRITES, a(sprite_birdpoop) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 76,0   ;    x: array[0..1] of byte;
    dta 44,0   ;    y: array[0..1] of byte;
    dta $ff,0     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 4,1     ;    frameOff: array[0..1] of byte;
    dta 0,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_birdpoop, 0      ;    param1: array[0..1] of byte; // room
    dta >path_birdpoop, 0      ;    param2: array[0..1] of byte; //    

    dta PUT_SLIME,$A0,$0C,11
    dta PUT_SLIME,$70,$0E,12
    dta PUT_SLIME,$99,$0F,13
    dta PUT_SLIME,$C3,$12,14

    dta LEVEL_END

room_03    
    ;;  c0   c1   c2   c4
    dta $96, $6a, $12, $70 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 7, 2    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 3;    

    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 117   ;    x: array[0..1] of byte;
    :2 dta 46    ;    y: array[0..1] of byte;
    :2 dta 9     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //     
    
    dta PUT_SLIME,$6D,$0D,15
    dta PUT_SLIME,$36,$0F,16
    dta PUT_SLIME,$68,$10,17
    dta PUT_SLIME,$3D,$12,18    

    dta LEVEL_END

room_04    
    ;;  c0   c1   c2   c4
    dta $16, $1f, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 0, 5, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 4;    
    dta COLOR_COLLISION_OFF
    
    dta PUT_SPRITES, a(sprite_exit_eyes) ;

    dta SPRITE_TWO       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_EXIT, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 14,32   ;    x: array[0..1] of byte;
    dta 54,35   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 3,3     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ONPATH    ;    moves: array[0..1] of byte;
    dta 0,<path_eyes     ;    param1: array[0..1] of byte; // room
    dta 0,>path_eyes     ;    param2: array[0..1] of byte;     
    
    dta SET_TRIGGER, 0, 80, 5, 150, 75, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_entrance);      

    dta PUT_SLIME,$67,$01,19
    dta PUT_SLIME,$37,$02,20
    dta PUT_SLIME,$B3,$03,21
    dta PUT_SLIME,$3D,$0E,22
    dta PUT_SLIME,$9C,$0F,23
    dta PUT_SLIME,$7A,$10,24

    dta LEVEL_END

room_05    
    ;;  c0   c1   c2   c4
    dta $86, $9c, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 1, 6, 9, 4    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 5;    

    dta PUT_SPRITES, a(sprite_lights) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 37,0   ;    x: array[0..1] of byte;
    dta 49,0   ;    y: array[0..1] of byte;
    dta $ff,0     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,1     ;    frameOff: array[0..1] of byte;
    dta 15,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 0, 0      ;    param1: array[0..1] of byte; // room
    dta 0, 0      ;    param2: array[0..1] of byte; //    

    dta PUT_SLIME,$9D,$04,25
    dta PUT_SLIME,$54,$08,26
    dta PUT_SLIME,$9D,$0A,27
    dta PUT_SLIME,$B9,$0E,28
    dta PUT_SLIME,$68,$0F,29

    dta LEVEL_END

room_06    
    ;;  c0   c1   c2   c4
    dta $16, $2a, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 2, 7, 10, 5    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 6;    

    dta PUT_TILE, ON_NO_GOAL, 1, 23, 50, a(tile_doors) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOAL, 2, 6, 67, a(tile_hatch) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOAL, 5, 17, 24, a(tile_cable) ; condition, param, x, y, tile_address         

    dta PUT_SPRITES, a(sprite_fire_console) ;

    dta SPRITE_TWO       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 22,81   ;    x: array[0..1] of byte;
    dta 29,54   ;    y: array[0..1] of byte;
    dta $ff,3   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,6     ;    frameOff: array[0..1] of byte;
    dta 3,15    ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_NONE    ;    moves: array[0..1] of byte;
    dta 0,8     ;    param1: array[0..1] of byte; // room
    dta 0,SFX_CONSOLE     ;    param2: array[0..1] of byte;     

    dta SET_TRIGGER, 1, 105, 5, 150, 75, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 1, 0
    dta SET_TRIGGER, 2, 105, 5, 150, 75, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret1);           


    dta PUT_SLIME,$48,$02,30
    dta PUT_SLIME,$80,$05,31
    dta PUT_SLIME,$37,$06,32
    dta PUT_SLIME,$49,$0A,33
    dta PUT_SLIME,$89,$0E,34
    dta PUT_SLIME,$68,$0F,35

    dta LEVEL_END

room_07    
    ;;  c0   c1   c2   c4
    dta $86, $ea, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 3, ROOM_NONE, ROOM_NONE, 6    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 7;    

    dta PUT_SPRITES, a(sprite_2flies) ;

    dta SPRITE_TWO       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 36,92   ;    x: array[0..1] of byte;
    dta 22,36   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 2,2     ;    frameOff: array[0..1] of byte;
    dta 2,2     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH    ;    moves: array[0..1] of byte;
    dta <path_fly1,<path_fly2     ;    param1: array[0..1] of byte; // room
    dta >path_fly1,>path_fly2     ;    param2: array[0..1] of byte;     

    dta PUT_SLIME,$83,$09,36
    dta PUT_SLIME,$65,$0B,37
    dta PUT_SLIME,$A3,$0C,38
    dta PUT_SLIME,$56,$0F,39

    dta LEVEL_END

room_08    
    ;;  c0   c1   c2   c4
    dta $16, $ea, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 9, 13, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 8;    
    
    dta PUT_TILE, ON_NO_GOAL, 3, 11, 5, a(tile_krata) ; condition, param, x, y, tile_address         
    
    dta PUT_SPRITES, a(sprite_console) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 82,0   ;    x: array[0..1] of byte;
    dta 27,0   ;    y: array[0..1] of byte;
    dta 5,0     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,1     ;    frameOff: array[0..1] of byte;
    dta 15,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 6, 0      ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, 0      ;    param2: array[0..1] of byte; //    

    dta PUT_SLIME,$9A,$03,40
    dta PUT_SLIME,$76,$08,41
    dta PUT_SLIME,$41,$0A,42
    dta PUT_SLIME,$86,$0B,43
    dta PUT_SLIME,$B9,$10,44

    dta LEVEL_END

room_09    
    ;;  c0   c1   c2   c4
    dta $26, $da, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 5, 10, 14, 8    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 9;    

    dta PUT_SPRITES, a(sprite_console2) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 45,0   ;    x: array[0..1] of byte;
    dta 31,0   ;    y: array[0..1] of byte;
    dta $ff,0     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 5,1     ;    frameOff: array[0..1] of byte;
    dta 15,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 0, 0      ;    param1: array[0..1] of byte; // room
    dta 0, 0      ;    param2: array[0..1] of byte; //    

    
    dta SET_TRIGGER, 3, 90, 50, 150, 75, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_shaft);      

    dta PUT_SLIME,$49,$03,45
    dta PUT_SLIME,$87,$08,46
    dta PUT_SLIME,$60,$0B,47
    dta PUT_SLIME,$93,$0C,48
    dta PUT_SLIME,$3F,$10,49

    dta LEVEL_END

room_10    
    ;;  c0   c1   c2   c4
    dta $96, $ba, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 6, 11, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 10;    
    
    dta PUT_SPRITES, a(sprite_valve_drops) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 139,63 ;    x: array[0..1] of byte;
    dta 64,56   ;    y: array[0..1] of byte;
    dta 6,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 11, <path_drop      ;    param1: array[0..1] of byte; // room
    dta SFX_VALVE, >path_drop     ;    param2: array[0..1] of byte; //          

    dta PUT_SLIME,$95,$03,50
    dta PUT_SLIME,$C9,$05,51
    dta PUT_SLIME,$65,$0B,52
    dta PUT_SLIME,$C3,$0E,53
    dta PUT_SLIME,$B2,$11,54
 
    dta LEVEL_END

room_11    
    ;;  c0   c1   c2   c4
    dta $a6, $ca, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, 10    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 11;    
    
    dta PUT_TILE, ON_NO_GOAL, 6, 27, 11, a(tile_water) ; condition, param, x, y, tile_address         
    
    dta PUT_SPRITES, a(sprite_vial) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_GOAL  ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime; 4 - main goal ;
	:2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 126   ;    x: array[0..1] of byte;
    :2 dta 26   ;    y: array[0..1] of byte;
    :2 dta 7     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0     ;    param1: array[0..1] of byte; // room
    :2 dta 0     ;    param2: array[0..1] of byte; //    

    dta SET_TRIGGER, 4, 126, 26, 134, 34, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_goal);       
    
    dta PUT_SLIME,$48,$02,55
    dta PUT_SLIME,$7A,$05,56
    dta PUT_SLIME,$60,$08,57
    dta PUT_SLIME,$92,$10,58
    dta PUT_SLIME,$43,$11,59    
    
    dta LEVEL_END

room_12    
    ;;  c0   c1   c2   c4
    dta $d6, $8a, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 13, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 12;    

    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 8     ;    x: array[0..1] of byte;
    :2 dta 36    ;    y: array[0..1] of byte;
    :2 dta 10    ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //         

    dta SET_TRIGGER, 5, 1, 5, 150, 75, ON_GOAL, 0, REACH_GOAL, SECRETS_OFFSET + 2, 0
    dta SET_TRIGGER, 6, 140, 5, 150, 75, ON_GOAL, 0, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret2);           

    dta SET_TRIGGER, 7, 90, 50, 158, 79, ON_NO_GOAL, 0, SHOW_MSG, 1, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_dark1);   


    dta PUT_SLIME,$6F,$06,60
    dta PUT_SLIME,$AA,$07,61
    dta PUT_SLIME,$57,$08,62
    dta PUT_SLIME,$82,$09,63
    dta PUT_SLIME,$9B,$0B,64

    dta SET_COLORS, ON_NO_GOAL, 0,   0, 0, 0, 0 ; condition, param, c0, c1, c2, c4, pmg0, pmg1, pmg2, pmg3;


    dta LEVEL_END

room_13    
    ;;  c0   c1   c2   c4
    dta $96, $ba, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 8, 14, ROOM_NONE, 12    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 13;    

    dta PUT_SLIME,$60,$02,65
    dta PUT_SLIME,$9C,$03,66
    dta PUT_SLIME,$C0,$04,67
    dta PUT_SLIME,$7E,$05,68
    dta PUT_SLIME,$3D,$06,69
    dta PUT_SLIME,$9D,$07,70
    dta PUT_SLIME,$64,$08,71
    dta PUT_SLIME,$BE,$09,72
    dta PUT_SLIME,$3E,$0A,73
    dta PUT_SLIME,$9D,$0B,74
    dta PUT_SLIME,$61,$0C,75
    dta PUT_SLIME,$40,$0D,76
    dta PUT_SLIME,$BE,$0E,77
    dta PUT_SLIME,$63,$0F,78
    dta PUT_SLIME,$3D,$11,79

    dta LEVEL_END

room_14    
    ;;  c0   c1   c2   c4
    dta $96, $da, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 9, 15, 16, 13    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 14;    
   

    dta PUT_SPRITES, a(sprite_rat) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 4,0   ;    x: array[0..1] of byte;
    dta 3,0   ;    y: array[0..1] of byte;
    dta $ff,0     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 15,1     ;    frameOff: array[0..1] of byte;
    dta 0,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_rat, 0      ;    param1: array[0..1] of byte; // room
    dta >path_rat, 0      ;    param2: array[0..1] of byte; //    

    dta PUT_SLIME,$78,$03,80
    dta PUT_SLIME,$41,$05,81
    dta PUT_SLIME,$C2,$06,82
    dta PUT_SLIME,$7A,$07,83
    dta PUT_SLIME,$9D,$09,84
    dta PUT_SLIME,$3F,$0B,85
    dta PUT_SLIME,$7A,$0C,86
    dta PUT_SLIME,$3D,$10,87
    dta PUT_SLIME,$88,$11,88
    
    dta LEVEL_END

room_15    
    ;;  c0   c1   c2   c4
    dta $86, $ea, $62, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 17, 14    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 15;    
    
    dta PUT_SPRITES, a(sprite_console_drop) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 27,93 ;    x: array[0..1] of byte;
    dta 28,48   ;    y: array[0..1] of byte;
    dta 8,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 23, <path_drop2      ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, >path_drop2     ;    param2: array[0..1] of byte; //              

    dta PUT_SLIME,$99,$03,89
    dta PUT_SLIME,$C0,$04,90
    dta PUT_SLIME,$4B,$05,91
    dta PUT_SLIME,$79,$09,92
    dta PUT_SLIME,$B5,$0A,93
    dta PUT_SLIME,$40,$0D,94
    dta PUT_SLIME,$5C,$0F,95
    dta PUT_SLIME,$7E,$12,96

    dta LEVEL_END

room_16    
    ;;  c0   c1   c2   c4
    dta $b6, $8a, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 14, 17, 20, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 16;    

    dta PUT_TILE, ON_NO_GOAL, 4, 33, 53, a(tile_bridge_up) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_GOAL, 4, 29, 67, a(tile_bridge_down) ; condition, param, x, y, tile_address         

    dta PUT_SPRITES, a(sprite_2flies) ;

    dta SPRITE_SINGLE     ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 44,92   ;    x: array[0..1] of byte;
    dta 35,36   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 2,2     ;    frameOff: array[0..1] of byte;
    dta 2,2     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH    ;    moves: array[0..1] of byte;
    dta <path_fly1,<path_fly2     ;    param1: array[0..1] of byte; // room
    dta >path_fly1,>path_fly2     ;    param2: array[0..1] of byte;     

    dta PUT_SLIME,$A1,$02,97
    dta PUT_SLIME,$AA,$08,98
    dta PUT_SLIME,$9C,$0D,99
    dta PUT_SLIME,$BC,$0F,100
    dta PUT_SLIME,$85,$10,101

    dta LEVEL_END

room_17    
    ;;  c0   c1   c2   c4
    dta $c6, $ea, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 15, ROOM_NONE, ROOM_NONE, 16    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 17;    

    dta PUT_SPRITES, a(sprite_box_fly) ;

    dta SPRITE_TWO     ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 145,120   ;    x: array[0..1] of byte;
    dta 14,42   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 5,2     ;    frameOff: array[0..1] of byte;
    dta 15,2     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ONPATH    ;    moves: array[0..1] of byte;
    dta 0,<path_fly3     ;    param1: array[0..1] of byte; // room
    dta 0,>path_fly3     ;    param2: array[0..1] of byte;     

    dta PUT_SLIME,$AF,$01,102
    dta PUT_SLIME,$67,$02,103
    dta PUT_SLIME,$8A,$06,104
    dta PUT_SLIME,$B4,$09,105
    dta PUT_SLIME,$90,$0B,106
    dta PUT_SLIME,$5F,$10,107

    dta LEVEL_END

room_18    
    ;;  c0   c1   c2   c4
    dta $86, $ba, $62, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 19, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 18;    

    dta PUT_SPRITES, a(sprite_lamp) ;
    
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 65,114   ;    x: array[0..1] of byte;
    dta 49,49   ;    y: array[0..1] of byte;
    dta $ff,1     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 15,6     ;    frameOff: array[0..1] of byte;
    dta 3,3     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_lamp, 6      ;    param1: array[0..1] of byte; // room
    dta >path_lamp, 0      ;    param2: array[0..1] of byte; //    

    dta SET_TRIGGER, 8, 0, 5, 80, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_lost);   

    dta PUT_SLIME,$4D,$02,108
    dta PUT_SLIME,$81,$06,109
    dta PUT_SLIME,$83,$0B,110
    dta PUT_SLIME,$C6,$10,111
    dta PUT_SLIME,$A3,$11,112

    dta LEVEL_END

room_19    
    ;;  c0   c1   c2   c4
    dta $96, $ca, $62, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 20, 21, 18    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 19;    

    dta PUT_SPRITES, a(sprite_lamp_drop) ;
    
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 16,23   ;    x: array[0..1] of byte;
    dta 51,15   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_lamp, <path_drop3      ;    param1: array[0..1] of byte; // room
    dta >path_lamp, >path_drop3      ;    param2: array[0..1] of byte; //    

    dta PUT_SLIME,$8C,$02,113
    dta PUT_SLIME,$39,$03,114
    dta PUT_SLIME,$C4,$05,115
    dta PUT_SLIME,$98,$06,116
    dta PUT_SLIME,$63,$07,117
    dta PUT_SLIME,$8D,$0C,118
    dta PUT_SLIME,$6B,$0F,119
    dta PUT_SLIME,$55,$11,120

    dta LEVEL_END

room_20    
    ;;  c0   c1   c2   c4
    dta $a6, $1a, $72, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 16, ROOM_NONE, 22, 19    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 20;    

    dta PUT_SPRITES, a(sprite_2flies) ;

    dta SPRITE_SINGLE     ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 36,92   ;    x: array[0..1] of byte;
    dta 44,36   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 2,2     ;    frameOff: array[0..1] of byte;
    dta 2,2     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH    ;    moves: array[0..1] of byte;
    dta <path_fly4,<path_fly2     ;    param1: array[0..1] of byte; // room
    dta >path_fly4,>path_fly2     ;    param2: array[0..1] of byte;     

    dta PUT_SLIME,$4E,$04,121
    dta PUT_SLIME,$6D,$08,122
    dta PUT_SLIME,$38,$09,123
    dta PUT_SLIME,$A9,$0A,124
    dta PUT_SLIME,$95,$0F,125
    dta PUT_SLIME,$6D,$10,126

    dta LEVEL_END

room_21    
    ;;  c0   c1   c2   c4
    dta $00, $ea, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 19, 22, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 21;   
    
    dta PUT_SLIME,$76,$03,127
    dta PUT_SLIME,$85,$04,128
    dta PUT_SLIME,$94,$05,129
    dta PUT_SLIME,$A3,$06,130     

    dta LEVEL_END

room_22    
    ;;  c0   c1   c2   c4
    dta $76, $ca, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 20, 23, 25, 21    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 22;    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 9, 52, 65, 55, 66, ALWAYS, 26, TELEPORT_ME, 42, 52   


    dta PUT_SPRITES, a(sprite_button_eyes) ;
    
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 57,77  ;    x: array[0..1] of byte;
    dta 23,27  ;    y: array[0..1] of byte;
    dta 4,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,1     ;    frameOff: array[0..1] of byte;
    dta 15,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 16, <path_rat_eyes      ;    param1: array[0..1] of byte; // room
    dta SFX_SIREN, >path_rat_eyes      ;    param2: array[0..1] of byte; //    

    dta SET_TRIGGER, 10, 80, 5, 158, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_bottom);   

    dta PUT_SLIME,$3A,$03,131
    dta PUT_SLIME,$72,$04,132
    dta PUT_SLIME,$AE,$05,133
    dta PUT_SLIME,$57,$07,134
    dta PUT_SLIME,$7F,$0B,135
    dta PUT_SLIME,$9B,$0F,136
    dta PUT_SLIME,$5D,$10,137
    dta PUT_SLIME,$83,$11,138

    dta LEVEL_END

room_23    
    ;;  c0   c1   c2   c4
    dta $26, $ca, $32, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 26, 24, ROOM_NONE, 22    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 23;    

    dta PUT_SPRITES, a(sprite_switch) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 105,0  ;    x: array[0..1] of byte;
    dta 39,0  ;    y: array[0..1] of byte;
    dta 0,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,1     ;    frameOff: array[0..1] of byte;
    dta 15,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 12, 0      ;    param1: array[0..1] of byte; // room
    dta SFX_LEVER, 0      ;    param2: array[0..1] of byte; //        

    dta SET_TRIGGER, 11, 4, 5, 80, 79, ON_NO_GOAL, 8, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_dark2);   

    dta SET_COLORS, ON_NO_GOAL, 8,   0, 0, 0, 0 ; condition, param, c0, c1, c2, c4, pmg0, pmg1, pmg2, pmg3;

    dta PUT_SLIME,$88,$05,139
    dta PUT_SLIME,$6F,$06,140
    dta PUT_SLIME,$BB,$07,141
    dta PUT_SLIME,$62,$09,142
    dta PUT_SLIME,$8C,$0E,143
    dta PUT_SLIME,$4E,$0F,144
    dta PUT_SLIME,$AC,$10,145

    dta LEVEL_END

room_24    
    ;;  c0   c1   c2   c4
    dta $16, $da, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, 23    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 24;    

    dta PUT_SPRITES, a(sprite_lever) ;
    
    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 118,0  ;    x: array[0..1] of byte;
    dta 55,0  ;    y: array[0..1] of byte;
    dta 2,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,1     ;    frameOff: array[0..1] of byte;
    dta 3,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 6, 0      ;    param1: array[0..1] of byte; // room
    dta SFX_LEVER, 0      ;    param2: array[0..1] of byte; //     

    ;dta SET_COLORS, ON_NO_GOAL, 8,   0, 0, 0, 0 ; condition, param, c0, c1, c2, c4, pmg0, pmg1, pmg2, pmg3;

    dta PUT_SLIME,$AC,$02,146
    dta PUT_SLIME,$38,$03,147
    dta PUT_SLIME,$8A,$05,148
    dta PUT_SLIME,$B8,$07,149
    dta PUT_SLIME,$6C,$08,150
    dta PUT_SLIME,$9A,$0A,151
    dta PUT_SLIME,$C0,$0B,152
    dta PUT_SLIME,$AE,$0D,153
    dta PUT_SLIME,$5E,$10,154
    dta PUT_SLIME,$A9,$11,155

    dta LEVEL_END

room_25    
    ;;  c0   c1   c2   c4
    dta $26, $ea, $32, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 22, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 25;    

    dta PUT_SLIME,$7A,$01,156
    dta PUT_SLIME,$7D,$03,157
    dta PUT_SLIME,$8C,$04,158
    dta PUT_SLIME,$B1,$09,159

    dta LEVEL_END

room_26    
    ;;  c0   c1   c2   c4
    dta $00, $ba, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 23, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 26;    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 12, 37, 47, 42, 48, ALWAYS, 22, TELEPORT_ME, 54, 68   

    
    dta PUT_SPRITES, a(sprite_eyes_eyes) ;
    
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 125,16  ;    x: array[0..1] of byte;
    dta 11,15  ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 7,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_cat_eyes, <path_bad_eyes      ;    param1: array[0..1] of byte; // room
    dta >path_cat_eyes, >path_bad_eyes      ;    param2: array[0..1] of byte; //    

    dta SET_TRIGGER, 13, 80, 5, 158, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_cat);   

    dta PUT_SLIME,$96,$08,160
    dta PUT_SLIME,$5A,$0C,161
    dta PUT_SLIME,$98,$0F,162
    dta PUT_SLIME,$65,$11,163
    dta PUT_SLIME,$9E,$12,164

    dta LEVEL_END


rmt
    ins 'worldmusic.apu'


    org $A000,$C000


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SPRITES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

sprite_bigslime
    icl 'sprite_bigslime.a65'

sprite_bat
    icl 'sprite_bat.a65'

sprite_exit_eyes
    icl 'sprite_exit_eyes.a65'

sprite_lights
    icl 'sprite_lights.a65'

sprite_birdpoop
    icl 'sprite_birdpoop.a65'

sprite_fire_console
    icl 'sprite_fire_console.a65'
    
sprite_2flies
    icl 'sprite_2flies.a65'
    
sprite_console
    icl 'sprite_console.a65'

sprite_console2
    icl 'sprite_console2.a65'
    
sprite_valve_drops
    icl 'sprite_valve_drops.a65'

sprite_vial
    icl 'sprite_vial.a65'
    
sprite_rat
    icl 'sprite_rat.a65'

sprite_console_drop
    icl 'sprite_console_drop.a65'

sprite_box_fly
    icl 'sprite_box_fly.a65'

sprite_lamp
    icl 'sprite_lamp.a65'

sprite_lamp_drop
    icl 'sprite_lamp_drop.a65'

sprite_button_eyes
    icl 'sprite_button_eyes.a65'

sprite_switch
    icl 'sprite_switch.a65'

sprite_lever
    icl 'sprite_lever.a65'
    
sprite_eyes_eyes
    icl 'sprite_eyes_eyes.a65'
    


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TILES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


tile_doors
    dta 4,13
    ins 'door.gfx'
    
tile_hatch
    dta 4,7
    ins 'cover.gfx'

tile_krata
    dta 6,35
    ins 'krata.gfx'
    
tile_bridge_up
    dta 2,14
    ins 'bridge_up.gfx'
    
tile_bridge_down
    dta 4,4
    ins 'bridge_down.gfx'

tile_cable
    dta 3,35
    ins 'cable.gfx'

tile_water
    dta 2,28
    ins 'water.gfx'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PATHS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  

path_bat1
    dta 30 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta 1 ; delta x
    dta -1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta 1 ; delta x
    dta -1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta -1 ; delta x
    dta 1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta -1 ; delta x
    dta -1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta -1 ; delta x
    dta 1  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta -1 ; delta x
    dta -1  ; delta y
    dta 1 ; delta frame or $70 + absolute
        
    dta 0

path_bat2
    dta 80 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 0  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 80 ; times  ; 0 ends path
    dta -1 ; delta x
    dta 0  ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 0 

path_birdpoop

    dta 40, 0, 0, 0

    dta 1, 0, 0, $71
    dta 1, 0, 0, $72
    dta 17, 0, 2, 0

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0   
    
    dta 1, 0, 0, $73
    dta 1, 0, -34, $70
    
    dta 20, 0, 0, 0

    dta 1, 0, 0, $71
    dta 1, 0, 0, $72
    dta 17, 0, 2, 0

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0   

    
    dta 1, 0, 0, $73
    dta 1, 0, -34, $70
    
    dta 60, 0, 0, 0
    
    dta 0
    
path_eyes
    dta 30, 0, 0, 0
    dta 4,0,0,1
    dta 4,0,0,-1

    dta 20, 0, 0, 0
    dta 4,0,0,1
    dta 4,0,0,-1

    dta 15, 0, 0, 0
    dta 4,0,0,1
    dta 4,0,0,-1
    
    dta 25, 0, 0, 0
    dta 4,0,0,1
    dta 4,0,0,-1
    
    dta 0


path_fly1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 20, 1, 1, 1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0       
    dta 20, -2, 0, 1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 20, 1, -1, 1

    dta 0


path_fly2

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 22, 1, -1, 1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 22, -2, 0, 1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0       
    dta 22, 1, 1, 1

    dta 0

path_drop
    dta 16 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1,0,-16,$70    
    
    dta 30,0,0,0

    dta 0

path_drop2
    dta 25 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1,0,-25,$70    
    
    dta 30,0,0,0

    dta 0


path_rat
    dta 30,0,0,$70

    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    


    dta 7,1,0,1
    dta 1,1,0,$70
    dta 7,1,0,1
    dta 1,1,0,$70
    dta 7,1,0,1
    dta 1,1,0,$70
    dta 7,1,0,1
    dta 1,1,0,$70
    dta 7,1,0,1
    dta 2,1,0,$70
    dta 7,1,0,1
    dta 2,1,0,$70
    dta 7,1,0,1
    dta 2,1,0,$70
    dta 7,1,0,1
    dta 2,1,0,$70
    dta 7,1,0,1
    dta 2,1,0,$70

    dta 1,0,0,$78

    dta 30,0,0,0
    
    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    
   

    dta 7,-1,0,1
    dta 1,-1,0,$78
    dta 7,-1,0,1
    dta 1,-1,0,$78
    dta 7,-1,0,1
    dta 1,-1,0,$78
    dta 7,-1,0,1
    dta 1,-1,0,$78
    dta 7,-1,0,1
    dta 2,-1,0,$78
    dta 7,-1,0,1
    dta 2,-1,0,$78
    dta 7,-1,0,1
    dta 2,-1,0,$78
    dta 7,-1,0,1
    dta 2,-1,0,$78
    dta 7,-1,0,1
    dta 2,-1,0,$78

    dta 1,0,0,$70


    dta 0 


path_fly3
    dta 30,0,0,$71

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 27, -1, 1, 1

    dta 30,0,0,$72

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 25, -1, -1, 1

    dta 30,0,0,$71

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 23, -1, 1, 1

    dta 30,0,0,$72

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 23, -1, -1, 1


    dta 30,0,0,$71

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 23, 1, 1, 1

    dta 30,0,0,$72

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 23, 1, -1, 1

    dta 30,0,0,$71

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 25, 1, 1, 1

    dta 30,0,0,$72

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 27, 1, -1, 1

    dta 0 

path_lamp
    dta 20,0,0,$70
    dta 5,0,0,1
    dta 40,0,0,$70
    dta 3,0,0,1
    dta 30,0,0,$70
    dta 4,0,0,$71
    dta 10,0,0,$70
    dta 5,0,0,1

    dta 0

path_drop3

    dta 30,0,0,0

    dta 11 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1,50,-11,$70    
    
    dta 40,0,0,0

    dta 16 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1,-50,-16,$70    

    dta 0

path_fly4
    dta 30,0,0,$71

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 24, 1, 1, 1

    dta 30,0,0,$72

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 24, 1, -1, 1

    dta 30,0,0,$71

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 24, -1, 1, 1

    dta 30,0,0,$72

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0   
    dta 24, -1, -1, 1

    dta 0

path_rat_eyes
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 15,0,0,$70
    dta 1,0,0,$71
    dta 25,0,0,$70
    dta 1,0,0,$71
    dta 10,0,0,$70
    dta 1,0,0,$71

    dta 0

path_cat_eyes
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 30,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 25,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 15,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 0
    
    
path_bad_eyes

    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 3,0,0,$73
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 1,128,30,$70

    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 3,0,0,$73
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 1,-128,0,$70
    
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 3,0,0,$73
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 1,128,30,$70
    
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 3,0,0,$73
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 1,-128,-60,$70
    
    dta 0
    
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; MESSAGES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.array msg_secret1[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "        The Dustmine Back Yard          "
.enda

.array msg_secret2[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "       The Mysterious Dark Room         "
.enda
    
.array msg_goal[80] .byte = 0
   [0]  = "  You have found the Last Ingredient!   "
   [40] = "       Let's get out of here.           "
.enda

.array msg_entrance[80] .byte = 0
   [0]  = "           Here are the mines.          "
   [40] = "  Let's look for that last ingredient.  "
.enda

.array msg_shaft[80] .byte = 0
   [0]  = "      It looks like a main shaft.       "
   [40] = "        I wonder if it's deep.          "
.enda

.array msg_dark1[80] .byte = 0
   [0]  = "                Ouch!                   "
   [40] = "      It is pretty dark in here.        "
.enda

.array msg_cat[80] .byte = 0
   [0]  = "     Damn, there's that cat again!      "
   [40] = "           Really strange...            "
.enda

.array msg_bottom[80] .byte = 0
   [0]  = "       This shaft is pretty deep.        "
   [40] = "      I want to get to the bottom.       "
.enda


.array msg_lost[80] .byte = 0
   [0]  = "      It is easy to get lost here.       "
   [40] = "            Better hurry up.             "
.enda

.array msg_dark2[80] .byte = 0
   [0]  = "               Beware!                  "
   [40] = "      It is really dark in here.        "
.enda
