    OPT f+ h-
    icl '../../const.inc'
    org $a000
world_start ; -- sewers --  
    
    dta 21  ; number of levels             > | < 
    dta WORLD_PLAYABLE ; state
    dta 17,"Abominable Sewers               "  ; name (keep length untouched)
    dta 20,70 ;20,70 ; starting x, starting y, starting room
    dta 0 ; starting room (0)
    dta a($0600) ; starting slime amount
    dta 148 ; total slime count
    dta 2 ; secretsCount
    dta a(120); timeTreshold: word;
    dta a(14000) ;scoreTreshold: cardinal;    
    dta $8c,$88 ; goal colors
    dta 0; world bank (leave 0 here);
    dta 1; assets bank offset;
    dta 2; image bank offset;
    dta a(icon); icon pointer    
    dta a(rmt); rmt pointer    
    
    dta 93 ; iconFlobX
    dta 31 ; iconFlobY
    dta FRAME_FALL  ; iconFlobFrame
    
    dta a(room_00)
    dta a(room_01)
    dta a(room_02)
    dta a(room_03)
    dta a(room_04)
    dta a(room_05)
    dta a(room_06)
    dta a(room_07)
    dta a(room_08)
    dta a(room_09)
    dta a(room_10)
    dta a(room_11)
    dta a(room_12)
    dta a(room_13)
    dta a(room_14)
    dta a(room_15)
    dta a(room_16)
    dta a(room_17)
    dta a(room_18)
    dta a(room_19)
    dta a(room_20)

;;; goals

;; 0 - valve -> waterfall
;; 1 - main goal -> vhatch
;; 2 - valve -> pool
;; 3 - upper console -> socket
;; 4 - lever -> cables
;; 5 - lower console counter
;; 6 - lower console -> upper console
;; 7 - upper console counter
;; 8 - big slime 1
;; 9 - big slime 2
;; a - 


icon
    dta $96, $de, $12 ; icon colors
    ins 'icon.gfx'

room_00    
    ;;  c0   c1   c2   c4
    dta $88, $cb, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 1, 2, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 0;    


    dta PUT_SLIME, $41, $01, 0 
    dta PUT_SLIME, $50, $04, 1 
    dta PUT_SLIME, $C3, $05, 2 
    dta PUT_SLIME, $3C, $09, 3 
    dta PUT_SLIME, $AE, $0A, 4 
    dta PUT_SLIME, $3A, $0D, 5
    
    dta PUT_TILE, ON_NO_GOAL, 0, 34, 42, a(tile_waterfall) ; condition, param, x, y, tile_address    
    
    dta PUT_SPRITES, a(sprite_exit_drop) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_EXIT, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 3,38   ;    x: array[0..1] of byte;
    dta 62,14   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 0, <path_drop1      ;    param1: array[0..1] of byte; // room
    dta 0, >path_drop1     ;    param2: array[0..1] of byte; //    

	dta SET_TRIGGER, 0, 10, 30, 80, 50, ONCE, NONE, SHOW_MSG, 0, 250   
    dta SET_MESSAGE, MSG_INFO, a(msg_welcome_sewer);

    dta LEVEL_END

room_01    
    ;;  c0   c1   c2   c4
    dta $98, $8e, $72, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 0, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 1;    


    dta PUT_SLIME,$65,$03,6
    dta PUT_SLIME,$90,$05,7
    dta PUT_SLIME,$4F,$08,8
    dta PUT_SLIME,$AF,$0B,9
    dta PUT_SLIME,$98,$10,10
    dta PUT_SLIME,$C4,$13,11


    dta PUT_SPRITES, a(sprite_valve_thunder) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 41,78   ;    x: array[0..1] of byte;
    dta 45,9   ;    y: array[0..1] of byte;
    dta 0,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,10     ;    frameOff: array[0..1] of byte;
    dta 7,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 0, <path_electro1      ;    param1: array[0..1] of byte; // room
    dta SFX_VALVE, >path_electro1     ;    param2: array[0..1] of byte; //    

    dta LEVEL_END

room_02    
    ;;  c0   c1   c2   c4
    dta $96, $9b, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 10, 3, ROOM_NONE, 0    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 2;    

    dta PUT_SPRITES, a(sprite_drop1) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 69,78   ;    x: array[0..1] of byte;
    dta 25,9   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,10     ;    frameOff: array[0..1] of byte;
    dta 0,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_drop2,0      ;    param1: array[0..1] of byte; // room
    dta >path_drop2,0     ;    param2: array[0..1] of byte; // 


    dta PUT_SLIME, $43, $01, 12
    dta PUT_SLIME, $B9, $03, 13
    dta PUT_SLIME, $70, $07, 14
    dta PUT_SLIME, $AC, $0A, 15
    dta PUT_SLIME, $58, $0E, 16
    dta PUT_SLIME, $AF, $0F, 17

    dta LEVEL_END

room_03    
    ;;  c0   c1   c2   c4
    dta $a6, $2e, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 9, 8, 4, 2    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 3;    

    dta PUT_SLIME, $B1, $01, 18
    dta PUT_SLIME, $5A, $04, 19
    dta PUT_SLIME, $B1, $08, 20
    dta PUT_SLIME, $93, $09, 21
    dta PUT_SLIME, $B4, $0E, 22
    dta PUT_SLIME, $9F, $10, 23

    dta PUT_SPRITES, a(sprite_vent_fly) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_PORTAL, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1    ;    flipable: array[0..1] of byte;
    dta 76,28   ;    x: array[0..1] of byte;
    dta 29,52   ;    y: array[0..1] of byte;
    dta 18,$ff   ;    goal
    dta 17,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 32,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 0,1     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_vent,<path_fly3  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_vent,>path_fly3  ;    param2: array[0..1] of byte; // addr, goal to check if active     
    

    dta PUT_TILE, ON_NO_GOAL, 1, 35, 54, a(tile_vhatch) ; ; condition, param, x, y, tile_address    

    dta LEVEL_END

room_04    
    ;;  c0   c1   c2   c4
    dta $96, $cd, $72, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 3, 5, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 4;    

    dta PUT_SPRITES, a(sprite_bubbles) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1    ;    flipable: array[0..1] of byte;
    dta 60,95   ;    x: array[0..1] of byte;
    dta 73,51   ;    y: array[0..1] of byte;
    dta $ff,$ff  ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 7,1     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ANIMATE, MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    dta 0,0  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta 0,0  ;    param2: array[0..1] of byte; // addr, goal to check if active    


    dta PUT_SLIME, $AF, $05, 24
    dta PUT_SLIME, $3B, $06, 25
    dta PUT_SLIME, $C7, $0C, 26
    dta PUT_SLIME, $63, $0D, 27
    dta PUT_SLIME, $3D, $10, 28

    dta PUT_TILE, ON_NO_GOAL, 2, 10, 30, a(tile_pool) ; condition, param, x, y, tile_address    
 
	dta SET_TRIGGER, 1, 10, 40, 50, 79, ON_NO_GOAL, 2, SHOW_MSG, 0, 50   
    dta SET_MESSAGE, MSG_INFO, a(msg_pool);
 
    dta LEVEL_END

room_05    
    ;;  c0   c1   c2   c4
    dta $98, $2e, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 8, 6, ROOM_NONE, 4    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 5;    

    dta PUT_SLIME, $6C, $02, 29
    dta PUT_SLIME, $98, $03, 30
    dta PUT_SLIME, $C0, $06, 31
    dta PUT_SLIME, $61, $09, 32
    dta PUT_SLIME, $AE, $0C, 33
    dta PUT_SLIME, $3C, $10, 34    

    dta PUT_TILE, ON_NO_GOAL, 3, 27, 0, a(tile_socket) ; condition, param, x, y, tile_address    
  
 
    dta PUT_SPRITES, a(sprite_electro2) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 108,70   ;    x: array[0..1] of byte;
    dta 2,21   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 1,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_electro21, <path_electro22      ;    param1: array[0..1] of byte; // room
    dta >path_electro21, >path_electro22     ;    param2: array[0..1] of byte; //    
 
    dta LEVEL_END

room_06    
    ;;  c0   c1   c2   c4
    dta $96, $3e, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 7, ROOM_NONE, ROOM_NONE, 5    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 6;    
    
    dta PUT_SLIME, $4E, $04, 35
    dta PUT_SLIME, $7C, $06, 36
    dta PUT_SLIME, $C3, $0C, 37
    dta PUT_SLIME, $64, $0E, 38
    dta PUT_SLIME, $A0, $10, 39

    dta PUT_SPRITES, a(sprite_electro3) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 38,29   ;    x: array[0..1] of byte;
    dta 8,24   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 0,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_electro31, <path_console1      ;    param1: array[0..1] of byte; // room
    dta >path_electro31, >path_console1     ;    param2: array[0..1] of byte; //    
 
	dta SET_TRIGGER, 2, 30, 25, 95, 35, ONCE, NONE, SHOW_MSG, 0, 250   
    dta SET_MESSAGE, MSG_INFO, a(msg_greensymbol);

    dta LEVEL_END

room_07    
    ;;  c0   c1   c2   c4
    dta $aa, $ee, $b4, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 6, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 7;    

    dta PUT_SLIME, $7F, $0E, 40
    dta PUT_SLIME, $BC, $0F, 41
    dta PUT_SLIME, $4E, $10, 42
    dta PUT_SLIME, $97, $12, 43

    dta PUT_SPRITES, a(sprite_lever2) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 78   ;    x: array[0..1] of byte;
    :2 dta 41   ;    y: array[0..1] of byte;
    :2 dta 4     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 10     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE    ;    moves: array[0..1] of byte;
    :2 dta 17     ;    param1: array[0..1] of byte; // room
    :2 dta SFX_SIREN     ;    param2: array[0..1] of byte;  // sfx

    dta LEVEL_END

room_08    
    ;;  c0   c1   c2   c4
    dta $d6, $bb, $00, $72 ; level colors
    ;;  gnd  kill bck  brdr
    dta 18, ROOM_NONE, 5, 3    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 8;    

    dta PUT_SLIME, $4A, $01, 44
    dta PUT_SLIME, $97, $04, 45
    dta PUT_SLIME, $67, $07, 46
    dta PUT_SLIME, $95, $08, 47
    dta PUT_SLIME, $C5, $0A, 48
    dta PUT_SLIME, $6E, $0B, 49
    dta PUT_SLIME, $80, $0C, 50
    dta PUT_SLIME, $AC, $0D, 51
    dta PUT_SLIME, $C3, $0E, 52
    dta PUT_SLIME, $4C, $0F, 53
    
    dta PUT_SPRITES, a(sprite_vial) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_GOAL  ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime; 4 - main goal ;
	:2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 10   ;    x: array[0..1] of byte;
    :2 dta 12   ;    y: array[0..1] of byte;
    :2 dta 1     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0     ;    param1: array[0..1] of byte; // room
    :2 dta 0     ;    param2: array[0..1] of byte; //        
 
 

	dta SET_TRIGGER, 3, 5, 10, 15, 30, ONCE, NONE, SHOW_MSG, 0, 250   
    dta SET_MESSAGE, MSG_INFO, a(msg_timetoleave);

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 4, 144, 19, 150, 20, ALWAYS, 20, TELEPORT_ME, 18, 16   


    dta LEVEL_END

room_09    
    ;;  c0   c1   c2   c4
    dta $18, $ce, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 19, 18, 3, 10    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 9;    
    
    dta PUT_SLIME, $5B, $05, 54
    dta PUT_SLIME, $9A, $06, 55
    dta PUT_SLIME, $59, $0C, 56
    dta PUT_SLIME, $B2, $0E, 57
    dta PUT_SLIME, $73, $10, 58
    dta PUT_SLIME, $7E, $11, 59    

    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 104   ;    x: array[0..1] of byte;
    :2 dta 61   ;    y: array[0..1] of byte;
    :2 dta 8     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //   


;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, aparam
    dta SET_TRIGGER, 5, 1, 4, 40, 30, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 1, 0   
    dta SET_TRIGGER, 6, 1, 4, 40, 30, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret1);      

	dta SET_TRIGGER, 7, 65, 50, 115, 75, ONCE, NONE, SHOW_MSG, 1, 250   
    dta SET_MESSAGE, MSG_INFO, a(msg_deepwell);

    dta LEVEL_END

room_10    
    ;;  c0   c1   c2   c4
    dta $28, $dc, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 11, 9, 2, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 10;    
    
    
    dta PUT_SPRITES, a(sprite_stone_drops) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 153,48   ;    x: array[0..1] of byte;
    dta 12,10   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 0,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 0, <path_drop4      ;    param1: array[0..1] of byte; // room
    dta 0, >path_drop4     ;    param2: array[0..1] of byte; //    
 
    
    
    dta PUT_SLIME, $43, $02, 60
    dta PUT_SLIME, $AA, $05, 61
    dta PUT_SLIME, $41, $07, 62
    dta PUT_SLIME, $8D, $0B, 63
    dta PUT_SLIME, $43, $0D, 64
    dta PUT_SLIME, $42, $12, 65  

    dta LEVEL_END

room_11    
    ;;  c0   c1   c2   c4
    dta $08, $1e, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 14, ROOM_NONE, 10, 12    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 11;    
    
    dta PUT_SLIME, $8A, $01, 66
    dta PUT_SLIME, $79, $04, 67
    dta PUT_SLIME, $C0, $05, 68
    dta PUT_SLIME, $9C, $09, 69
    dta PUT_SLIME, $35, $0A, 70
    dta PUT_SLIME, $C1, $10, 71
    dta PUT_SLIME, $43, $12, 72    

    dta PUT_SPRITES, a(sprite_mice_console) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_DIAL     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 39,81   ;    x: array[0..1] of byte;
    dta 69,16   ;    y: array[0..1] of byte;
    dta $ff,5   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,2     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 0,0     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_mice, 6     ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_mice, $ff   ;    param2: array[0..1] of byte; // addr, goal to check if active     

    dta LEVEL_END

room_12    
    ;;  c0   c1   c2   c4
    dta $96, $8b, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 13, 11, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 12;    

    dta PUT_SLIME, $C5, $02, 73
    dta PUT_SLIME, $4D, $04, 74
    dta PUT_SLIME, $67, $07, 75
    dta PUT_SLIME, $99, $0A, 76
    dta PUT_SLIME, $5A, $0C, 77
    dta PUT_SLIME, $92, $0D, 78
    dta PUT_SLIME, $A8, $0F, 79
    dta PUT_SLIME, $40, $10, 80
    
    dta PUT_SPRITES, a(sprite_valve_drops) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 149,114 ;    x: array[0..1] of byte;
    dta 62,18   ;    y: array[0..1] of byte;
    dta 2,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 4, <path_drop5      ;    param1: array[0..1] of byte; // room
    dta SFX_VALVE, >path_drop5     ;    param2: array[0..1] of byte; //        
    

    dta LEVEL_END

room_13    
    ;;  c0   c1   c2   c4
    dta $d8, $be, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 14, 12, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 13;    
    
    dta PUT_SLIME, $77, $01, 81
    dta PUT_SLIME, $8D, $02, 82
    dta PUT_SLIME, $45, $03, 83
    dta PUT_SLIME, $C6, $04, 84
    dta PUT_SLIME, $3A, $05, 85
    dta PUT_SLIME, $65, $06, 86
    dta PUT_SLIME, $AC, $07, 87
    dta PUT_SLIME, $51, $08, 88
    dta PUT_SLIME, $A1, $09, 89
    dta PUT_SLIME, $C3, $0A, 90
    dta PUT_SLIME, $AA, $0C, 91
    dta PUT_SLIME, $4C, $0D, 92
    dta PUT_SLIME, $38, $0E, 93
    dta PUT_SLIME, $6A, $0F, 94
    dta PUT_SLIME, $B3, $10, 95
    dta PUT_SLIME, $4A, $11, 96    
    
    dta SET_TRIGGER, 8, 90, 0, 150, 30, ONCE, NONE, SHOW_MSG, 0, 250   
    dta SET_MESSAGE, MSG_INFO, a(msg_tricky);
    

    dta LEVEL_END

room_14    
    ;;  c0   c1   c2   c4
    dta $96, $9e, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 15, 11, 13    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 14;    

    dta PUT_SLIME, $68, $03, 97
    dta PUT_SLIME, $39, $04, 98
    dta PUT_SLIME, $BF, $0A, 99
    dta PUT_SLIME, $A1, $0B, 100
    dta PUT_SLIME, $BF, $10, 101
    dta PUT_SLIME, $4E, $11, 102
    dta PUT_SLIME, $7B, $12, 103

    dta PUT_SPRITES, a(sprite_fly_console) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_DIAL     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 0,1     ;    flipable: array[0..1] of byte;
    dta 6,82   ;    x: array[0..1] of byte;
    dta 36,71   ;    y: array[0..1] of byte;
    dta $ff,7   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,2     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 1,0     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_fly, 3     ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_fly, 6     ;    param2: array[0..1] of byte; // addr, goal to check if active     

	dta SET_TRIGGER, 9, 70, 70, 90, 79, ON_NO_GOAL, 6, SHOW_MSG, 0, 250   
    dta SET_MESSAGE, MSG_INFO, a(msg_consoleoff);

    dta LEVEL_END

room_15    
    ;;  c0   c1   c2   c4
    dta $b6, $cb, $52, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 16, ROOM_NONE, 14    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 15;    

    dta PUT_SLIME, $79, $03, 104
    dta PUT_SLIME, $9E, $04, 105
    dta PUT_SLIME, $4C, $05, 106
    dta PUT_SLIME, $5F, $06, 107
    dta PUT_SLIME, $8A, $07, 108
    dta PUT_SLIME, $B3, $08, 109
    dta PUT_SLIME, $75, $09, 110
    dta PUT_SLIME, $9D, $0A, 111
    dta PUT_SLIME, $4A, $0B, 112
    dta PUT_SLIME, $61, $0C, 113
    dta PUT_SLIME, $8A, $0D, 114
    dta PUT_SLIME, $B4, $0E, 115
    dta PUT_SLIME, $76, $0F, 116
    dta PUT_SLIME, $9A, $10, 117
    dta PUT_SLIME, $64, $11, 118

    dta LEVEL_END

room_16    
    ;;  c0   c1   c2   c4
    dta $18, $0e, $00, $82 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 17, 15    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 16;    

    dta PUT_SLIME, $81, $06, 119
    dta PUT_SLIME, $4A, $09, 120
    dta PUT_SLIME, $6B, $0D, 121
    dta PUT_SLIME, $C2, $0F, 122
    dta PUT_SLIME, $97, $11, 123

    dta LEVEL_END

room_17    
    ;;  c0   c1   c2   c4
    dta $98, $8e, $00, $62 ; level colors
    ;;  gnd  kill bck  brdr
    dta 16, ROOM_NONE, 18, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 17;    

    dta PUT_SLIME, $50, $03, 124
    dta PUT_SLIME, $B8, $04, 125
    dta PUT_SLIME, $81, $05, 126
    dta PUT_SLIME, $C0, $09, 127
    dta PUT_SLIME, $9C, $0F, 128
    
    dta PUT_TILE, ON_NO_GOAL, 4, 7, 20, a(tile_cableson) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_GOAL, 4, 27, 20, a(tile_cablesoff2) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_GOAL, 4, 6, 20, a(tile_cablesoff1) ; condition, param, x, y, tile_address         

    dta LEVEL_END

room_18    
    ;;  c0   c1   c2   c4
    dta $16, $1e, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 17, ROOM_NONE, 8, 9    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 18;    

    dta PUT_SLIME, $97, $04, 129
    dta PUT_SLIME, $BC, $07, 130
    dta PUT_SLIME, $86, $0D, 131
    dta PUT_SLIME, $4B, $0E, 132
    dta PUT_SLIME, $B0, $11, 133
    
    dta PUT_SPRITES, a(sprite_vent_fly) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_PORTAL, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,0     ;    flipable: array[0..1] of byte;
    dta 8,60   ;    x: array[0..1] of byte;
    dta 25,20   ;    y: array[0..1] of byte;
    dta 3,$ff   ;    goal
    dta 87,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 34,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 0,1     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_vent,<path_fly2  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_vent,>path_fly2  ;    param2: array[0..1] of byte; // addr, goal to check if active     
    
    

    dta LEVEL_END

room_19   
    ;;  c0   c1   c2   c4
    dta $98, $ce, $00, $12 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 9, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 19;    

    dta PUT_SLIME, $8C, $02, 134
    dta PUT_SLIME, $3A, $03, 135
    dta PUT_SLIME, $67, $04, 136
    dta PUT_SLIME, $5A, $0C, 137
    dta PUT_SLIME, $3D, $0D, 138
    dta PUT_SLIME, $5A, $13, 139
    
    
    dta PUT_SPRITES, a(sprite_eyes_tail) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 117,127   ;    x: array[0..1] of byte;
    dta 10,7   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 3,3     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_cateyes,<path_cattail  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_cateyes,>path_cattail  ;    param2: array[0..1] of byte; // addr, goal to check if active         

    dta LEVEL_END


room_20   
    ;;  c0   c1   c2   c4
    dta $16, $cc, $00, $92 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 20;    
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 10, 10, 10, 80, 80, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 2, 0   
    dta SET_TRIGGER, 11, 10, 10, 80, 80, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret2);         
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 12, 17, 13, 21, 14, ALWAYS, 8, TELEPORT_ME, 145, 24   

    dta PUT_SLIME,$97,$04,140
    dta PUT_SLIME,$77,$06,141
    dta PUT_SLIME,$42,$07,142
    dta PUT_SLIME,$BD,$08,143
    dta PUT_SLIME,$42,$0B,144
    dta PUT_SLIME,$A7,$0E,145
    dta PUT_SLIME,$3F,$10,146
    dta PUT_SLIME,$BF,$11,147

    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 102   ;    x: array[0..1] of byte;
    :2 dta 35    ;    y: array[0..1] of byte;
    :2 dta 9     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //   

    dta LEVEL_END

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;********************     MUSIC

rmt 
    ins 'worldmusic.apu'
    

;;;;;;;;******************************************                   second bank 

    org $A000, $C000 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;********************     TILES


tile_waterfall
    dta 5,38
    ins 'waterfall.gfx'

tile_vhatch
    dta 2,15
    ins 'vhatch.gfx'

tile_pool
    dta 19,29
    ins 'pool.gfx'

tile_socket
    dta 3,7
    ins 'socket.gfx'

tile_cableson
    dta 28,9
    ins 'cableson.gfx'


tile_cablesoff1
    dta 10,47
    ins 'cablesoff1.gfx'

tile_cablesoff2
    dta 4,48
    ins 'cablesoff2.gfx'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;********************     SPRITES


sprite_valve_thunder
    icl 'sprite_valve.a65'

sprite_vial
    icl 'sprite_vial.a65'

sprite_exit_drop
    icl 'sprite_exit_drop.a65'

sprite_electro2
    icl 'sprite_electro2.a65'

sprite_electro3
    icl 'sprite_electro3.a65'

sprite_lever2
    icl 'sprite_lever2.a65'

sprite_bigslime
    icl 'sprite_bigslime.a65'

sprite_stone_drops
    icl 'sprite_stone_drops.a65'

sprite_mice_console
    icl 'sprite_mice_console.a65'

sprite_fly_console
    icl 'sprite_console.a65'

sprite_valve_drops
    icl 'sprite_valve_drops.a65'

sprite_vent_fly
    icl 'sprite_vent_fly.a65'

sprite_eyes_tail
    icl 'sprite_eyes_tail.a65'

sprite_drop1
    icl 'sprite_drop1.a65'
    
sprite_bubbles
    icl 'sprite_bubbles.a65'
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;********************     PATHS

path_electro1

    dta $ff; play fx
	dta SFX_CIRCUIT;
	dta 0,0

    dta 10 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 14 ; delta x
    dta 19 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_CIRCUIT;
	dta 0,0

    dta 10 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -4 ; delta x
    dta -15 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 20 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -30 ; delta x
    dta -4 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 0

path_electro21

    dta 50 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_CIRCUIT;
	dta 0,0

    dta 21 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 0

path_electro22

    dta $ff; play fx
	dta SFX_CIRCUIT;
	dta 0,0

    dta 16 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -16 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 40 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute

    dta 0


path_drop1

    dta 30 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 3 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 58 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta 0 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -58 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 0


path_electro31

    dta $ff; play fx
	dta SFX_CIRCUIT;
	dta 0,0

    dta 25 ; times  ; 0 ends path
    dta 3 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; 
    dta -(3*25)
    dta 0 
    dta $70;
    
    dta 20 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute
    
    dta 0
    

path_console1 
    dta 5 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute
    
    dta 1 ; times  ; 0 ends path
    dta 30 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 5 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 30 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 5 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -60 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 0 
    
path_drop4
    dta 18 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 18 ; delta x
    dta -18 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 18 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -18 ; delta x
    dta -18 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 18 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute
    
    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 47 ; delta x
    dta -18 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 18 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -29 ; delta x
    dta -18 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 18 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute
    
    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -18 ; delta x
    dta -18 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 0 
    
path_mice
    dta 3,0,0,$70

    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    


    dta 7,2,0,1
    dta 1,2,0,$70
    dta 7,2,0,1
    dta 1,2,0,$70
    dta 7,2,0,1
    dta 1,2,0,$70
    dta 7,2,0,1
    dta 1,2,0,$70
    dta 7,2,0,1
    dta 2,2,0,$70

    dta 1,0,-14,$78


    dta 3,0,0,$78
    
    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    
   

    dta 7,-2,0,1
    dta 1,-2,0,$78
    dta 7,-2,0,1
    dta 1,-2,0,$78
    dta 7,-2,0,1
    dta 1,-2,0,$78
    dta 7,-2,0,1
    dta 1,-2,0,$78
    dta 7,-2,0,1
    dta 2,-2,0,$78

    dta 1,0,14,$70


    dta 0 

path_fly

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    

    dta 8,1,1,1;
    dta 8,1,-1,1;
    dta 8,1,1,1;
    dta 8,1,-1,1;
    dta 8,1,1,1;

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    

    
    dta 8,1,-1,1;
    dta 8,1,1,1;
    dta 8,1,-1,1;
    dta 8,1,1,1;
    dta 8,1,-1,1;

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    

    dta 8,-1,1,1;
    dta 8,-1,-1,1;
    dta 8,-1,1,1;
    dta 8,-1,-1,1;
    dta 8,-1,1,1;

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    

    dta 8,-1,-1,1;
    dta 8,-1,1,1;
    dta 8,-1,-1,1;
    dta 8,-1,1,1;
    dta 8,-1,-1,1;

    dta 0
    
    
path_drop5
    dta 26 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 13 ; delta x
    dta -26 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 26 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -13 ; delta x
    dta -26 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 26 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 26 ; delta x
    dta -26 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 26 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -13 ; delta x
    dta -26 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 26 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0    

    dta 2 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -13 ; delta x
    dta -26 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 10,0,0,0

    dta 0     


path_vent
    dta 50,0,0,$71
    dta 1,0,0,$73
    dta 150,0,0,$71
    dta 1,0,0,$73
    dta 1,0,0,$70
    dta 1,0,0,$72
    dta 3,0,0,$70
    dta 1,0,0,$72
    dta 80,0,0,$71
    dta 1,0,0,$73
    dta 120,0,0,$71
    dta 1,0,0,$72
    dta 10,0,0,$70
    dta 1,0,0,$72
    dta 0

path_fly2

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    

    dta 10,1,1,1;
    dta 5,1,-1,1;
    dta 10,1,1,1;
    dta 5,1,-1,1;
    dta 10,1,1,1;

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    


    dta 5,1,-1,1;
    dta 10,1,1,1;
    dta 5,1,-1,1;
    dta 10,1,1,1;
    dta 5,1,-1,1;

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    


    dta 5,-1,1,1;
    dta 10,-1,-1,1;
    dta 5,-1,1,1;
    dta 10,-1,-1,1;
    dta 5,-1,1,1;

    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    

    dta 10,-1,-1,1;
    dta 5,-1,1,1;
    dta 10,-1,-1,1;
    dta 5,-1,1,1;
    dta 10,-1,-1,1;

    dta 0

path_fly3

    dta 10,0,0,$71 ;// frames,deltaX,deltaY,deltaFrame ($7x - set frame x)
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,1,1,1;
    dta 10,0,0,$72
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,1,-1,1;
    dta 10,0,0,$71
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,1,1,1;
    dta 10,0,0,$72
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,1,-1,1;

    dta 10,0,0,$71
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,-1,1,1;
    dta 10,0,0,$72
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,-1,-1,1;
    dta 10,0,0,$71
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,-1,1,1;
    dta 10,0,0,$72
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 17,-1,-1,1;

    dta 0


path_cateyes
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 15,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 40,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 30,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 10,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 25,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 0


path_cattail
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 2,0,0,$72
    dta 3,0,0,$73
    dta 2,0,0,$72
    dta 1,0,0,$71
    dta 60,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 2,0,0,$73
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 40,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 2,0,0,$73
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 0


path_drop2
    dta 30,0,0,$70
    dta 16,0,1,$70
    dta $ff,SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,0,-16,$70
    dta 0

.array msg_secret1[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "            WELL WELL WELL              "
.enda

.array msg_secret2[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "              GROOVY PIPE               "
.enda
    
.array msg_welcome_sewer[80] .byte = 0
   [2] =  "         What a filthy place."
   [42] = "  Let's find this ingredient quickly."
.enda

.array msg_pool[80] .byte = 0
   [2] =  "          Pool of acid. Nice."
   [42] = "    There must be a way to empty it."
.enda

.array msg_greensymbol[80] .byte = 0
   [2] =  "      One of the symbols is green."
   [42] = "             I wonder why..."
.enda

.array msg_consoleoff[80] .byte = 0
   [2] =  "          This one seems off."
   [42] = "   It probably turns on elsewhere."
.enda

.array msg_deepwell[80] .byte = 0
   [2] =  "                Well..."
   [42] = "          It is really deep."
.enda

.array msg_timetoleave[80] .byte = 0
   [2] =  "    Good job! It's time to leave."
   [42] = "    Go back to where you started."
.enda

.array msg_tricky[80] .byte = 0
   [2] =  "           That was tricky."
   [42] = "     I didn't know you could fly."
.enda
