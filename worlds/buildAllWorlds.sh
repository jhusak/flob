#!/bin/bash
. ../buildconfig.conf
for d in */; do
    $PYTHON reindex_slimes.py $d/world*.asm
    ./_buildDir.sh $d
done
