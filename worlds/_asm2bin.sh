#!/bin/bash
. ../buildconfig.conf
for WORLD in $(ls -1 $1/*.asm)
do
    echo \*\*\* CONVERTING $WORLD
    $MADS_EXE $WORLD 
    NAME="${WORLD%.*}"
    #dd bs=1 skip=6 if=$NAME.obx of=$NAME.bin
    gsplit -b 8K $NAME.obx ${NAME}
    mv ${NAME}aa $NAME.bin
    mv ${NAME}ab ${NAME}_assets.bin
    rm $NAME.obx
done

