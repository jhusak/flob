. ../buildconfig.conf
TEMPLZ4=tmp.tzl4
$SMALLZ4 ./$1 $TEMPLZ4
dd if=$TEMPLZ4 of=$2 bs=1 count=$(($(gstat -c '%s' $TEMPLZ4) - 12)) skip=11
rm $TEMPLZ4
