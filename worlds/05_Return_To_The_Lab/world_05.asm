    OPT f+ h-
    icl '../../const.inc'
    org $a000
world_start ; -- lab return --  
    
    dta 23 ; number of levels             > | < 
    dta WORLD_PLAYABLE ; state
    dta 17,"Return to the Lab               "  ; name (keep length untouched)
    dta 140, 60 ; starting x, starting y, starting room 140,60
    dta 4; starting room (4)
    dta a($1000) ; starting slime amount
    dta 171 ; total slime count
    dta 2 ; secretsCount
    dta a(330); timeTreshold: word;
    dta a(13500) ;scoreTreshold: cardinal;    
    dta $1c,$18 ; goal colors
    dta 0; world bank (leave 0 here);
    dta 1; assets bank offset;
    dta 2; image bank offset;
    dta a(icon); icon pointer
    dta a(rmt); rmt pointer
    
    dta 76 ; iconFlobX
    dta 32 ; iconFlobY
    dta 0  ; iconFlobFrame
    
    ;;; ROOM POINTERS
    dta a(room_00)
    dta a(room_01)
    dta a(room_02)
    dta a(room_03)
    dta a(room_04)
    dta a(room_05)
    dta a(room_06)
    dta a(room_07)
    dta a(room_08)
    dta a(room_09)
    dta a(room_10)
    dta a(room_11)
    dta a(room_12)
    dta a(room_13)
    dta a(room_14)
    dta a(room_15)
    dta a(room_16)
    dta a(room_17)
    dta a(room_18)
    dta a(room_19)
    dta a(room_20)
    dta a(room_21)
    dta a(room_22)
;//    dta a(room_23)

;;; goals  (trigger -> obj)

;; 0 - door1
;; 1 - door2
;; 2 - door3
;; 3 - door4
;; 4 - door5
;; 5 - dr up bridge
;; 6 - big slime 1
;; 7 - big slime 2
;; 8 - key -> desk
;; 9 - main goal
;; a - 
;; b - 
;; c - 
;; d -     
    
icon
    dta $16, $be, $22 ; icon colors
    ins 'icon.gfx'

room_00    
    ;;  c0   c1   c2   c4
    dta $96, $ca, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 6, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 0;    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 0, 105, 0, 138, 4, ALWAYS, 19, TELEPORT_ME, 102, 74   

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, aparam
    dta SET_TRIGGER, 1, 10, 0, 150, 30, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 1, 0   
    dta SET_TRIGGER, 2, 10, 0, 150, 30, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret1);      

    dta PUT_SLIME,$47,$02,0
    dta PUT_SLIME,$BA,$03,1
    dta PUT_SLIME,$5D,$05,2
    dta PUT_SLIME,$BD,$07,3
    dta PUT_SLIME,$72,$08,4
    dta PUT_SLIME,$5F,$09,5
    dta PUT_SLIME,$8E,$0C,6
    dta PUT_SLIME,$72,$0D,7
    dta PUT_SLIME,$3B,$0E,8
    dta PUT_SLIME,$46,$0F,9
    dta PUT_SLIME,$BD,$11,10


    dta PUT_SPRITES, a(sprite_button_valve) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 138,144   ;    x: array[0..1] of byte;
    dta 43,17   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 15,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 0, 0      ;    param1: array[0..1] of byte; // room
    dta 0, 0     ;    param2: array[0..1] of byte; //    


    dta LEVEL_END

room_01    
    ;;  c0   c1   c2   c4
    dta $16, $ea, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 7, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 1;    
    
    
    dta PUT_SLIME,$5A,$03,11
    dta PUT_SLIME,$A9,$08,12
    dta PUT_SLIME,$BB,$0B,13
    dta PUT_SLIME,$50,$0C,14
    dta PUT_SLIME,$97,$0F,15
    dta PUT_SLIME,$C0,$10,16
    dta PUT_SLIME,$62,$11,17    

    dta PUT_SPRITES, a(sprite_2consolas) ;
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 19,105   ;    x: array[0..1] of byte;
    dta 31,22   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 7,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    dta 0, 0      ;    param1: array[0..1] of byte; // room
    dta 0, 0     ;    param2: array[0..1] of byte; //    

    dta LEVEL_END

room_02    
    ;;  c0   c1   c2   c4
    dta $98, $3c, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 8, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 2;    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 3, 142, 51, 155, 54, ALWAYS, 5, TELEPORT_ME, 140, 10   


    dta PUT_SLIME,$6D,$0C,18
    dta PUT_SLIME,$3D,$0D,19
    dta PUT_SLIME,$91,$0E,20
    dta PUT_SLIME,$C2,$11,21

    dta PUT_SPRITES, a(sprite_card_meteor) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 121,10   ;    x: array[0..1] of byte;
    dta 64,0   ;    y: array[0..1] of byte;
    dta 1,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 9, <path_meteor      ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, >path_meteor     ;    param2: array[0..1] of byte; //    



    dta LEVEL_END

room_03    
    ;;  c0   c1   c2   c4
    dta $76, $0a, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 4, 9, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 3;    

    dta PUT_SLIME,$74,$05,22
    dta PUT_SLIME,$36,$07,23
    dta PUT_SLIME,$3D,$0A,24
    dta PUT_SLIME,$B9,$0C,25
    dta PUT_SLIME,$85,$0D,26
    dta PUT_SLIME,$67,$0E,27
    dta PUT_SLIME,$9E,$10,28
    dta PUT_SLIME,$75,$11,29

    dta PUT_SPRITES, a(sprite_2flames) ;
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 99,126   ;    x: array[0..1] of byte;
    dta 58,59   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 1,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_flame1, <path_flame2      ;    param1: array[0..1] of byte; // room
    dta >path_flame1, >path_flame2     ;    param2: array[0..1] of byte; //    

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 4, 60, 0, 120, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_cementary);

    dta LEVEL_END

room_04    
    ;;  c0   c1   c2   c4
    dta $16, $0c, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, 3    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 4;    

    dta PUT_SLIME,$95,$05,30
    dta PUT_SLIME,$AF,$06,31
    dta PUT_SLIME,$A9,$0C,32
    dta PUT_SLIME,$45,$0D,33
    dta PUT_SLIME,$5D,$10,34

    dta PUT_SPRITES, a(sprite_exit_heart) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_EXIT, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 138,65   ;    x: array[0..1] of byte;
    dta 53,50   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 3,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 0, <path_heart      ;    param1: array[0..1] of byte; // room
    dta 0, >path_heart     ;    param2: array[0..1] of byte; //    
    
    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 5, 60, 0, 120, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_start);


    dta LEVEL_END

room_05    
    ;;  c0   c1   c2   c4
    dta $96, $ba, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 5;    
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 6, 137, 5, 152, 8, ALWAYS, 2, TELEPORT_ME, 143, 59   

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 7, 3, 10, 12, 30, ALWAYS, 14, TELEPORT_ME, 135, 49   
    

    dta PUT_SLIME,$53,$02,35
    dta PUT_SLIME,$B9,$04,36
    dta PUT_SLIME,$86,$06,37
    dta PUT_SLIME,$4D,$09,38
    dta PUT_SLIME,$9D,$0B,39
    dta PUT_SLIME,$C2,$10,40
    dta PUT_SLIME,$3A,$11,41    
    

    dta PUT_SPRITES, a(sprite_fly_drop) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 31,82   ;    x: array[0..1] of byte;
    dta 54,33   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 1,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_fly1, <path_drop1      ;    param1: array[0..1] of byte; // room
    dta >path_fly1, >path_drop1     ;    param2: array[0..1] of byte; //    
    

    dta LEVEL_END

room_06    
    ;;  c0   c1   c2   c4
    dta $d6, $ba, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 0, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 6;  
    
    dta PUT_SLIME,$A7,$02,42
    dta PUT_SLIME,$8A,$03,43
    dta PUT_SLIME,$4C,$04,44
    dta PUT_SLIME,$5B,$06,45
    dta PUT_SLIME,$87,$07,46
    dta PUT_SLIME,$52,$0A,47
    dta PUT_SLIME,$B6,$0B,48
    dta PUT_SLIME,$75,$0D,49
    dta PUT_SLIME,$C0,$0F,50
    dta PUT_SLIME,$46,$10,51
        
    
    dta PUT_SPRITES, a(sprite_bigslime) ;
    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 140   ;    x: array[0..1] of byte;
    :2 dta 24    ;    y: array[0..1] of byte;
    :2 dta 6     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //     
      

    dta LEVEL_END

room_07    
    ;;  c0   c1   c2   c4
    dta $88, $1c, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 1, 8, 13, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 7;    


    dta PUT_SLIME,$B0,$02,52
    dta PUT_SLIME,$41,$03,53
    dta PUT_SLIME,$83,$07,54
    dta PUT_SLIME,$AC,$09,55
    dta PUT_SLIME,$68,$0B,56
    dta PUT_SLIME,$A8,$0D,57
    dta PUT_SLIME,$B6,$0F,58
    dta PUT_SLIME,$59,$12,59


    dta PUT_SPRITES, a(sprite_card_spray) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 145,110   ;    x: array[0..1] of byte;
    dta 13,22   ;    y: array[0..1] of byte;
    dta 0,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 9, <path_spray      ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, >path_spray     ;    param2: array[0..1] of byte; //    
    

    dta LEVEL_END

room_08    
    ;;  c0   c1   c2   c4
    dta $16, $1c, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 2, 9, 14, 7    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 8;    

    dta PUT_SLIME,$6F,$02,60
    dta PUT_SLIME,$B5,$06,61
    dta PUT_SLIME,$99,$0B,62
    dta PUT_SLIME,$51,$0D,63
    dta PUT_SLIME,$BA,$0F,64
    dta PUT_SLIME,$72,$10,65
    dta PUT_SLIME,$AF,$11,66


    dta PUT_SPRITES, a(sprite_fly_drop) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 124,82   ;    x: array[0..1] of byte;
    dta 42,33   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_fly2, 0      ;    param1: array[0..1] of byte; // room
    dta >path_fly2, 0     ;    param2: array[0..1] of byte; //    


    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 8, 10, 0, 150, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_familiar);


    dta LEVEL_END

room_09    
    ;;  c0   c1   c2   c4
    dta $16, $be, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 3, 10, ROOM_NONE, 8    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 9;    

    dta PUT_SLIME,$47,$04,67
    dta PUT_SLIME,$B4,$08,68
    dta PUT_SLIME,$3A,$09,69
    dta PUT_SLIME,$CA,$0D,70
    dta PUT_SLIME,$38,$0E,71


    dta PUT_TILE, ON_NO_GOAL, 0, 20, 36, a(tile_door1) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOAL, 1, 21, 36, a(tile_door2) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOAL, 2, 23, 36, a(tile_door3) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOAL, 3, 24, 36, a(tile_door4) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOAL, 4, 25, 36, a(tile_door5) ; condition, param, x, y, tile_address         

    dta PUT_TILE, ON_GOALS_UP_TO, 4, 34, 39, a(tile_dr_desk_off) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_NO_GOALS_UP_TO, 4, 34, 39, a(tile_dr_desk_on) ; condition, param, x, y, tile_address         

    dta PUT_SPRITES, a(sprite_console_dot) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 1,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 126,145   ;    x: array[0..1] of byte;
    dta 9,27   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 15,31     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    dta 0,0     ;    param1: array[0..1] of byte; // room
    dta 0,0     ;    param2: array[0..1] of byte; //    


    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 9, 10, 0, 80, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_multidoors);

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 10, 110, 0, 150, 79, ONCE, NONE, SHOW_MSG, 1, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_where);



    dta LEVEL_END

room_10    
    ;;  c0   c1   c2   c4
    dta $26, $0a, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 16, 9    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 10;    


    dta PUT_SLIME,$C8,$02,72
    dta PUT_SLIME,$61,$06,73
    dta PUT_SLIME,$AA,$08,74
    dta PUT_SLIME,$3B,$0D,75
    dta PUT_SLIME,$B7,$0E,76


    dta PUT_TILE, ON_NO_GOAL, 5, 29, 3, a(tile_dr_up_bridge) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_GOAL, 5, 29, 3, a(tile_dr_down_bridge) ; condition, param, x, y, tile_address         

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 11, 100, 0, 120, 50, ONCE, NONE, REACH_GOAL, 5, 0
    
    dta SET_TRIGGER, 12, 110, 0, 120, 30, ONCE, NONE, SHOW_SCENE, 0, 0
    dta SET_SCENE, 23, a(msg_scene0), $28, $0d, $34;

	dta SET_TRIGGER, 13, 10, 0, 80, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_doc);

	dta SET_TRIGGER, 14, 117, 0, 150, 79, ONCE, NONE, SHOW_MSG, 1, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_bridge);

    dta LEVEL_END

room_11    
    ;;  c0   c1   c2   c4
    dta $96, $0e, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 12, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 11;    


    dta PUT_SLIME,$7A,$02,77
    dta PUT_SLIME,$3A,$03,78
    dta PUT_SLIME,$C5,$05,79
    dta PUT_SLIME,$8B,$0E,80
    dta PUT_SLIME,$4F,$0F,81
    dta PUT_SLIME,$C4,$11,82


;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 15, 4, 47, 10, 52, ALWAYS, 17, TELEPORT_ME, 10, 13


    dta PUT_SPRITES, a(sprite_fly_drop) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 112,80   ;    x: array[0..1] of byte;
    dta 23,33   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 1,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_fly3, 0      ;    param1: array[0..1] of byte; // room
    dta >path_fly3, 0     ;    param2: array[0..1] of byte; //    
    

    dta LEVEL_END

room_12    
    ;;  c0   c1   c2   c4
    dta $74, $6a, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 13, ROOM_NONE, 11    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 12;    


    dta PUT_SLIME,$A9,$02,83
    dta PUT_SLIME,$65,$05,84
    dta PUT_SLIME,$8B,$06,85
    dta PUT_SLIME,$3B,$09,86
    dta PUT_SLIME,$76,$0D,87
    dta PUT_SLIME,$B9,$11,88


    dta PUT_SPRITES, a(sprite_drop) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 32    ;    x: array[0..1] of byte;
    :2 dta 19    ;    y: array[0..1] of byte;
    :2 dta $ff   ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 0     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_drop     ;    param1: array[0..1] of byte; // room
    :2 dta >path_drop     ;    param2: array[0..1] of byte; 

    dta LEVEL_END

room_13    
    ;;  c0   c1   c2   c4
    dta $16, $bb, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 7, 14, ROOM_NONE, 12    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 13;    


    dta PUT_SLIME,$C2,$02,89
    dta PUT_SLIME,$8F,$03,90
    dta PUT_SLIME,$A7,$04,91
    dta PUT_SLIME,$88,$06,92
    dta PUT_SLIME,$B3,$08,93
    dta PUT_SLIME,$38,$09,94
    dta PUT_SLIME,$AA,$0D,95
    dta PUT_SLIME,$3F,$0F,96
    dta PUT_SLIME,$5D,$11,97

    dta PUT_SPRITES, a(sprite_drop) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 76    ;    x: array[0..1] of byte;
    :2 dta 36    ;    y: array[0..1] of byte;
    :2 dta $ff   ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 0     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_drop2     ;    param1: array[0..1] of byte; // room
    :2 dta >path_drop2     ;    param2: array[0..1] of byte; 


    dta LEVEL_END

room_14    
    ;;  c0   c1   c2   c4
    dta $a6, $ce, $00, $b2 ; level colors
    ;;  gnd  kill bck  brdr
    dta 8, 15, 20, 13    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 14;    
    

    dta PUT_SLIME,$C0,$03,98
    dta PUT_SLIME,$44,$06,99
    dta PUT_SLIME,$77,$0A,100
    dta PUT_SLIME,$5B,$0D,101
    dta PUT_SLIME,$B7,$0E,102
    dta PUT_SLIME,$7B,$11,103
    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 16, 142, 45, 152, 49, ALWAYS, 5, TELEPORT_ME, 15, 29   

    dta PUT_SPRITES, a(sprite_key_lamp) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 130,145  ;    x: array[0..1] of byte;
    dta 70,26   ;    y: array[0..1] of byte;
    dta 8,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 8,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 18, <path_lamp      ;    param1: array[0..1] of byte; // room
    dta SFX_GEAR, >path_lamp     ;    param2: array[0..1] of byte; //        
    
    dta LEVEL_END

room_15    
    ;;  c0   c1   c2   c4
    dta $16, $ea, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 16, ROOM_NONE, 14    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 15;    
    
    
    dta PUT_SLIME,$67,$03,104
    dta PUT_SLIME,$B6,$04,105
    dta PUT_SLIME,$93,$05,106
    dta PUT_SLIME,$3F,$06,107
    dta PUT_SLIME,$A6,$07,108
    dta PUT_SLIME,$69,$08,109
    dta PUT_SLIME,$41,$0B,110
    dta PUT_SLIME,$86,$0C,111
    dta PUT_SLIME,$54,$0D,112
    dta PUT_SLIME,$6D,$0F,113
    dta PUT_SLIME,$C1,$10,114
    dta PUT_SLIME,$9D,$11,115    

    dta LEVEL_END

room_16    
    ;;  c0   c1   c2   c4
    dta $16, $0c, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 10, ROOM_NONE, ROOM_NONE, 15    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 16;   
    

    dta PUT_SLIME,$C5,$02,116
    dta PUT_SLIME,$69,$04,117
    dta PUT_SLIME,$4D,$05,118
    dta PUT_SLIME,$34,$06,119
    dta PUT_SLIME,$53,$0A,120
    dta PUT_SLIME,$8F,$0C,121
     

    dta PUT_SPRITES, a(sprite_eyes_glasses) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 72,108  ;    x: array[0..1] of byte;
    dta 60,64   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_eyes,0      ;    param1: array[0..1] of byte; // room
    dta >path_eyes,0     ;    param2: array[0..1] of byte; //     

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 17, 100, 0, 150, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_survived);

	dta SET_TRIGGER, 18, 10, 0, 80, 79, ONCE, NONE, SHOW_MSG, 1, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_cat);


    dta LEVEL_END

room_17    
    ;;  c0   c1   c2   c4
    dta $00, $16, $02, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 17;    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 20, 7, 5, 17, 10, ALWAYS, 11, TELEPORT_ME, 10, 16
    

    dta PUT_SLIME,$79,$02,122
    dta PUT_SLIME,$92,$03,123
    dta PUT_SLIME,$5C,$04,124
    dta PUT_SLIME,$AE,$05,125
    dta PUT_SLIME,$5A,$08,126
    dta PUT_SLIME,$BA,$09,127
    dta PUT_SLIME,$68,$0B,128
    dta PUT_SLIME,$79,$0C,129
    dta PUT_SLIME,$B9,$0D,130
    dta PUT_SLIME,$A4,$10,131
    dta PUT_SLIME,$56,$11,132    

    dta PUT_SPRITES, a(sprite_card) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 75,145  ;    x: array[0..1] of byte;
    dta 27,26   ;    y: array[0..1] of byte;
    dta 2,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 9, 0     ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, 0     ;    param2: array[0..1] of byte; //            
    

    dta LEVEL_END

room_18    
    ;;  c0   c1   c2   c4
    dta $88, $1e, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 19, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 18;    

    dta PUT_TILE, ON_NO_GOAL, 8, 25, 56, a(tile_desk_close) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_GOAL, 8, 25, 56, a(tile_desk_open) ; condition, param, x, y, tile_address         

    dta PUT_SLIME,$67,$03,133
    dta PUT_SLIME,$B5,$06,134
    dta PUT_SLIME,$7C,$09,135
    dta PUT_SLIME,$9A,$0B,136
    dta PUT_SLIME,$A4,$11,137

    dta PUT_SPRITES, a(sprite_card_recipe) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_GOAL     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 60,102  ;    x: array[0..1] of byte;
    dta 31,58   ;    y: array[0..1] of byte;
    dta 3,9     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 3,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    dta 9,0      ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE,0     ;    param2: array[0..1] of byte; //     

	dta SET_TRIGGER, 19, 102, 58, 106, 66, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_found);

    dta LEVEL_END

room_19    
    ;;  c0   c1   c2   c4
    dta $16, $da, $00, $22 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 20, ROOM_NONE, 18    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 19;    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 21, 110, 70, 120, 74, ALWAYS, 0, TELEPORT_ME, 112, 8   

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 22, 5, 8, 14, 10, ALWAYS, 22, TELEPORT_ME, 7, 18

    dta PUT_SLIME,$A6,$02,138
    dta PUT_SLIME,$6F,$03,139
    dta PUT_SLIME,$BE,$0A,140
    dta PUT_SLIME,$47,$0E,141
    dta PUT_SLIME,$82,$11,142


    dta PUT_SPRITES, a(sprite_fly_drop) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 69,82   ;    x: array[0..1] of byte;
    dta 24,33   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 1,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_fly4, 0      ;    param1: array[0..1] of byte; // room
    dta >path_fly4, 0     ;    param2: array[0..1] of byte; //    

    

    dta LEVEL_END

room_20    
    ;;  c0   c1   c2   c4
    dta $96, $be, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 14, 21, ROOM_NONE, 19    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 20;    

    dta PUT_SLIME,$A5,$02,143
    dta PUT_SLIME,$B3,$05,144
    dta PUT_SLIME,$6C,$06,145
    dta PUT_SLIME,$BF,$0B,146
    dta PUT_SLIME,$70,$0D,147
    dta PUT_SLIME,$90,$10,148
    dta PUT_SLIME,$42,$11,149

    dta PUT_SPRITES, a(sprite_eyes_drop) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 146,105  ;    x: array[0..1] of byte;
    dta 67,5   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_eyes2, <path_drop3      ;    param1: array[0..1] of byte; // room
    dta >path_eyes2, >path_drop3     ;    param2: array[0..1] of byte; //    


    dta LEVEL_END

room_21    
    ;;  c0   c1   c2   c4
    dta $86, $B8, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, 20    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 21;    

    dta PUT_SLIME,$74,$02,150
    dta PUT_SLIME,$7E,$04,151
    dta PUT_SLIME,$AB,$06,152
    dta PUT_SLIME,$83,$08,153
    dta PUT_SLIME,$5D,$0A,154

    dta PUT_SPRITES, a(sprite_card2) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 145,145  ;    x: array[0..1] of byte;
    dta 50,26   ;    y: array[0..1] of byte;
    dta 4,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 9, 0     ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, 0     ;    param2: array[0..1] of byte; //            
    
    dta LEVEL_END

room_22    
    ;;  c0   c1   c2   c4
    dta $16, $ea, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 22;    

    dta PUT_SLIME,$61,$02,155
    dta PUT_SLIME,$A1,$03,156
    dta PUT_SLIME,$6D,$04,157
    dta PUT_SLIME,$80,$05,158
    dta PUT_SLIME,$C5,$06,159
    dta PUT_SLIME,$97,$07,160
    dta PUT_SLIME,$39,$08,161
    dta PUT_SLIME,$5D,$09,162
    dta PUT_SLIME,$AA,$0A,163
    dta PUT_SLIME,$78,$0B,164
    dta PUT_SLIME,$3B,$0C,165
    dta PUT_SLIME,$C4,$0D,166
    dta PUT_SLIME,$86,$0F,167
    dta PUT_SLIME,$6D,$10,168
    dta PUT_SLIME,$4C,$11,169
    dta PUT_SLIME,$B9,$12,170

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 23, 3, 7, 13, 16, ALWAYS, 19, TELEPORT_ME, 6, 16

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, aparam
    dta SET_TRIGGER, 24, 0, 0, 50, 50, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 2, 0   
    dta SET_TRIGGER, 25, 0, 0, 50, 50, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret2);      

    dta PUT_SPRITES, a(sprite_bigslime) ;
    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 10   ;    x: array[0..1] of byte;
    :2 dta 66    ;    y: array[0..1] of byte;
    :2 dta 7     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //     

    dta LEVEL_END




rmt
    ins 'worldmusic.apu'


    org $A000,$C000


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SPRITES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

sprite_bigslime
    icl 'sprite_bigslime.a65'

sprite_button_valve
    icl 'sprite_button_valve.a65'

sprite_2consolas
    icl 'sprite_2consolas.a65'

sprite_drop
    icl 'sprite_drop.a65'

sprite_2flames
    icl 'sprite_2flames.a65'

sprite_exit_heart
    icl 'sprite_exit_heart.a65'

sprite_fly_drop
    icl 'sprite_fly_drop.a65'
    
sprite_card_spray
    icl 'sprite_card_spray.a65'
        
sprite_console_dot
    icl 'sprite_console_dot.a65'

sprite_card_meteor
    icl 'sprite_card_meteor.a65'

sprite_key_lamp
    icl 'sprite_key_lamp.a65'

sprite_eyes_glasses
    icl 'sprite_eyes_glasses.a65'
    
sprite_card
    icl 'sprite_card.a65'

sprite_card2
    icl 'sprite_card2.a65'
    
sprite_card_recipe
    icl 'sprite_card_recipe.a65'

sprite_eyes_drop
    icl 'sprite_eyes_drop.a65'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TILES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

tile_door1
tile_door5
    dta 1,20
    ins 'door1.gfx'

tile_door2
    dta 2,20
    ins 'door2.gfx'

tile_door3
    dta 1,20
    ins 'door3.gfx'

tile_door4
    dta 1,20
    ins 'door4.gfx'
    
tile_dr_desk_on
    dta 4,21
    ins 'dr_desk_on.gfx'

tile_dr_desk_off
    dta 4,21
    ins 'dr_desk_off.gfx'

tile_dr_up_bridge
    dta 4,25
    ins 'dr_up_bridge.gfx'

tile_dr_down_bridge
    dta 4,25
    ins 'dr_down_bridge.gfx'
    
tile_desk_open
    dta 4,11
    ins 'desk_open.gfx'
    
tile_desk_close
    dta 3,11
    ins 'desk_close.gfx'
    
    

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PATHS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


path_spray
    dta 30,0,0,$72
    dta $ff,SFX_GAS,0,0
    dta 30,0,0,1
    dta 50,0,0,$72
    dta $ff,SFX_GAS,0,0
    dta 20,0,0,1
    dta 1,0,35,$72
    dta 20,0,0,$72
    dta $ff,SFX_GAS,0,0
    dta 10,0,0,1
    dta 40,0,0,$72
    dta $ff,SFX_GAS,0,0
    dta 30,0,0,1
    dta 1,0,-35,$72
    
    dta 0

path_flame1
    dta 1,0,0,$70
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 5,0,0,1

    dta 1,0,0,$70
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 5,0,0,1

    dta 1,0,0,$70
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 5,0,0,1
    dta 0

    
path_flame2
    dta 1,0,0,$70
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 5,0,0,1

    dta 1,0,0,$70
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 5,0,0,1

    dta 1,0,0,$70
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 2,0,0,1
    dta 2,0,0,-1
    dta 5,0,0,1
    dta 0
    

path_drop

    dta 50,0,0,$70
    dta 6,0,0,1
    dta 22,0,1,0

	dta $ff, SFX_WATERDROP, 0, 0

    dta 5,0,0,1

    dta 1,0,10,$7c

    dta 50,0,0,$70
    dta 6,0,0,1
    dta 17,0,1,0

	dta $ff, SFX_WATERDROP, 0, 0

    dta 5,0,0,1
    dta 1,0,-49,$7c

    
    dta 0
  
path_heart

    dta 30,0,0,$70
    dta 2,0,0,1
    dta 50,0,0,$70
    dta 2,0,0,1
    dta 80,0,0,$70
    dta 2,0,0,1

    dta 0

path_drop1   ;      o -14- o -11- o -19- o  
    
    dta 2,0,0,$70;
    dta 14,0,1,$70
    dta $ff, SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,-30,-14,$70

    dta 10,0,0,$70;
    dta 14,0,1,$70
    dta $ff, SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,11,-14,$70

    dta 6,0,0,$70;
    dta 14,0,1,$70
    dta $ff, SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,-25,-14,$70

    dta 10,0,0,$70;
    dta 14,0,1,$70
    dta $ff, SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,25,-14,$70
        
    dta 4,0,0,$70;
    dta 14,0,1,$70
    dta $ff, SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,-11,-14,$70

    dta 12,0,0,$70;
    dta 14,0,1,$70
    dta $ff, SFX_WATERDROP,0,0
    dta 2,0,0,1
    dta 1,30,-14,$70
    
    dta 0 ;
    
path_fly1
    dta 10,0,0,$71
    dta $ff,SFX_INSECT,0,0
    dta 16,1,1,1
    dta 20,0,0,$72
    dta $ff,SFX_INSECT,0,0
    dta 16,1,-1,1
    dta 15,0,0,$71
    dta $ff,SFX_INSECT,0,0
    dta 16,1,1,1
    dta 8,0,0,$72
    dta $ff,SFX_INSECT,0,0
    dta 16,1,-1,1
    dta 24,0,0,$71
    dta $ff,SFX_INSECT,0,0
    dta 15,1,1,1

    dta 10,0,0,$72
    dta $ff,SFX_INSECT,0,0
    dta 15,-1,-1,1
    dta 20,0,0,$71
    dta $ff,SFX_INSECT,0,0
    dta 16,-1,1,1
    dta 8,0,0,$72
    dta $ff,SFX_INSECT,0,0
    dta 16,-1,-1,1
    dta 15,0,0,$71
    dta $ff,SFX_INSECT,0,0
    dta 16,-1,1,1
    dta 22,0,0,$72
    dta $ff,SFX_INSECT,0,0
    dta 16,-1,-1,1

    dta 0


path_fly2
    dta $ff,SFX_INSECT,0,0
    dta 12,-1,-2,1
    dta $ff,SFX_INSECT,0,0
    dta 12,-2,1,1
    dta $ff,SFX_INSECT,0,0
    dta 12,3,1,1
    
    dta 0

path_fly3
    dta $ff,SFX_INSECT,0,0
    dta 20,0,1,1
    dta $ff,SFX_INSECT,0,0
    dta 20,-1,-1,1
    dta $ff,SFX_INSECT,0,0
    dta 20,0,1,1
    dta $ff,SFX_INSECT,0,0
    dta 20,1,-1,1
    
    dta 0


path_fly4
    
    dta $ff,SFX_INSECT,0,0
    dta 40,0,1,1
    dta $ff,SFX_INSECT,0,0
    dta 40,0,-1,1

    dta 0

path_meteor
    dta 120,0,0,$70
    dta 4,0,0,1
    dta 20,2,1,0
    dta $ff,SFX_GAS,0,0
    dta 4,2,1,-1
   
    dta 1, 20,-24,$70

    dta 170,0,0,$70
    dta 1,0,0,$75
    dta 3,0,0,1
    dta 20,-2,1,0
    dta $ff,SFX_GAS,0,0
    dta 3,-2,1,-1

    dta 1, -22,-23,$70
    
    dta 0


path_drop2

    dta 50,0,0,$70
    dta 6,0,0,1
    dta 32,0,1,0

	dta $ff, SFX_WATERDROP, 0, 0

    dta 5,0,0,1

    dta 1,0,-32,$7c
   
    dta 0

path_lamp
    dta 80,0,0,$71
    dta 6,0,0,1
    dta 120,0,0,$71
    dta 2,0,0,1
    dta 30,0,0,$71
    dta 8,0,0,1
    dta 60,0,0,$71
    dta 2,0,0,1
    dta 40,0,0,$71
    dta 12,0,0,1

    dta 0

path_eyes
    
    dta 20,0,0,$71
    dta 8,0,0,$70
    dta 40,0,0,$71
    dta 1,0,0,$72
    dta 40,0,0,$71
    dta 8,0,0,$70
    dta 10,0,0,$71
    dta 2,0,0,$72
    dta 12,0,0,$70
    dta 1,0,0,$72

    dta 0

path_eyes2
    dta 30,0,0,$70
    dta 3,0,0,1
    dta 10,0,0,0
    dta 3,0,0,-1
    dta 50,0,0,$70
    dta 3,0,0,1
    dta 10,0,0,0
    dta 3,0,0,-1

    dta 0

path_drop3


    dta 50,0,0,$70
    dta 62,0,1,0

	dta $ff, SFX_WATERDROP, 0, 0

    dta 2,0,0,1
    dta 1,0,-62,$70
   
    dta 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; MESSAGES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    
.array msg_scene0[160] .byte = 0
   [0] =  "    ...there is no strict evidence"
   [40] = "  if Dr Sven Torff was pushed or not."
   [80] = "               Anyway,"
   [120]= "     he unexpectedly dropped down."
.enda      

.array msg_secret1[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "            LIFT ME ALONE               "
.enda

.array msg_secret2[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "              PIT 2 FALL                "
.enda

.array msg_start[80] .byte = 0
   [0]  = "              Let's go!                 "
   [40] = "    Time to find this damn recipe!      "
.enda

.array msg_cementary[80] .byte = 0
   [0]  = "    You need to find Dr Torff first     "
   [40] = "                                        "
.enda

.array msg_multidoors[80] .byte = 0
   [0]  = "   Wow! That was quite easy. But...     "
   [40] = " I need to unlock these doors somehow.  "
.enda


.array msg_familiar[80] .byte = 0
   [0]  = "   This place looks really familiar.    "
   [40] = "                                        "
.enda

.array msg_where[80] .byte = 0
   [0]  = "     Hmmmm... I'm sure that doctor      "
   [40] = "          is somewhere here.            "
.enda

.array msg_doc[80] .byte = 0
   [0]  = "        Hah, what's up, doc?            "
   [40] = "        Where is the Formula?           "
.enda

.array msg_bridge[80] .byte = 0
   [0]  = "              Oooooops!                 "
   [40] = "        Who is on the top now?          "
.enda

.array msg_survived[80] .byte = 0
   [0]  = "                Damn!                   "
   [40] = "      It looks like he survived!        "
.enda

.array msg_cat[80] .byte = 0
   [0]  = "  This creepy cat is blocking the way.  "
   [40] = "        The Doctor escaped.             "
.enda

.array msg_nevermind[80] .byte = 0
   [0]  = "             Nevermind!                 "
   [40] = "  The Formula must be somewhere here.   "
.enda

.array msg_found[80] .byte = 0
   [0]  = "  Great! You've got the slime formula.  "
   [40] = "       Leave this lab, quickly!         "
.enda






