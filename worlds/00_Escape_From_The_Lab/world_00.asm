    OPT f+ h-
    icl '../../const.inc'
    org $a000
world_start ; -- lab escape --  
    
    dta 25 ; number of levels             > | < 
    dta WORLD_PLAYABLE ; state
    dta 19,"Escape from the Lab             "  ; name (keep length untouched)
    dta 20, 55 ;20, 55 ; starting x, starting y
    dta 0; 0 ; starting room (0)
    dta a($0500) ; starting slime amount
    dta 109 ; total slime count
    dta 2 ; secretsCount
    dta a(120); timeTreshold: word;
    dta a(13000) ;scoreTreshold: cardinal;
    dta $0c,$08 ; goal colors
    dta 0; world bank (leave 0 here);
    dta 1; assets bank offset;
    dta 2; image bank offset;
    dta a(icon); icon pointer
    dta a(rmt); rmt pointer
    
    dta 74 ; iconFlobX
    dta 33 ; iconFlobY
    dta 1  ; iconFlobFrame
    
    ;;; ROOM POINTERS
    dta a(room_00)
    dta a(room_01)
    dta a(room_02)
    dta a(room_03)
    dta a(room_04)
    dta a(room_05)
    dta a(room_06)
    dta a(room_07)
    dta a(room_08)
    dta a(room_09)
    dta a(room_10)
    dta a(room_11)
    dta a(room_12)
    dta a(room_13)
    dta a(room_14)
    dta a(room_15)
    dta a(room_16)
    dta a(room_17)
    dta a(room_18)
    dta a(room_19)
    dta a(room_20)
    dta a(room_21)
    dta a(room_22)    
    dta a(room_23)    
    dta a(room_24)    
    
;;; goals

;; 0 - cage
;; 1 - cables / cables
;; 2 - radioactive / console
;; 3 - smoke / extinguisher
;; 4 - hatch / phone
;; 5 - door to extinguisher & rats / bell
;; 6 - stars / telescope 
;; 7 - map
;; 8 - stats
;; 9 - big slime secret 1
;; a - big slime secret 2    
    
    
icon
    dta $88, $1e, $12 ; icon colors
    ins 'icon.gfx'


room_00    
    ;;  c0   c1   c2   c4
    dta $e6, $ba, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 1, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 0;    

    dta PUT_SLIME,$47,$05,0 
    dta PUT_SLIME,$AE,$0A,1

    dta PUT_SPRITES, a(sprite_cagedoor) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 60,78   ;    x: array[0..1] of byte;
    dta 25,9   ;    y: array[0..1] of byte;
    dta 0,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 10,10     ;    frameOff: array[0..1] of byte;
    dta 3,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta $ff, 0      ;    param1: array[0..1] of byte; // room
    dta SFX_LEVER,0     ;    param2: array[0..1] of byte; //    
                   
    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 0, 25, 50, 40, 70, ONCE, NONE, SHOW_MSG, 0, 250   
	dta SET_TRIGGER, 1, 120, 0, 159, 79, ONCE, NONE, SHOW_MSG, 1, 150  
	dta SET_MESSAGE, MSG_INFO, a(msg_flipcage);
	dta SET_MESSAGE, MSG_WARNING, a(msg_cankillyou);

    dta LEVEL_END





room_01    
;;  c0   c1   c2   c4
    dta $86, $be, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 2, ROOM_NONE, 0    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 1;    
    
    dta PUT_SLIME,$C0,$02,2
    dta PUT_SLIME,$4A,$0F,3
    dta PUT_SLIME,$AF,$11,4
    
    dta PUT_SPRITES, a(sprite_bench_portal) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_PORTAL     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 45,35   ;    x: array[0..1] of byte;
    dta 70,70   ;    y: array[0..1] of byte;
    dta $ff,22 ;    goal
    dta 0,36    ;    frameOn: array[0..1] of byte;
    dta 0,16    ;    frameOff: array[0..1] of byte;
    dta 0,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 0,0   ;    param1: array[0..1] of byte; // room
    dta 0,0     ;    param2: array[0..1] of byte; //    

    
    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 2, 10, 0, 40, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_TRIGGER, 3, 70, 0, 100, 79, ONCE, NONE, SHOW_MSG, 1, 200 
	dta SET_TRIGGER, 4, 130, 0, 150, 79, ONCE, NONE, SHOW_MSG, 2, 200 
	dta SET_MESSAGE, MSG_INFO, a(msg_avoidbright);
	dta SET_MESSAGE, MSG_INFO, a(mgs_explode);
	dta SET_MESSAGE, MSG_INFO, a(mgs_anddie);

    dta LEVEL_END




room_02    
;;  c0   c1   c2   c4
    dta $88, $1e, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 3, ROOM_NONE, 1    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 2;    

    dta PUT_SLIME,$5E,$02,5
    dta PUT_SLIME,$9B,$0B,6
    dta PUT_SLIME,$44,$11,7

    

    dta PUT_SPRITES, a(sprite_drop_eyes) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_DECOR, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 85,70   ;    x: array[0..1] of byte;
    dta 44,66   ;    y: array[0..1] of byte;
    dta $ff,$ff ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 1,3     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_droplab, <path_cateyes  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_droplab, >path_cateyes  ;    param2: array[0..1] of byte; // addr, goal to check if active    


    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 5, 40, 0, 80, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_labo);

    dta LEVEL_END




room_03    
;;  c0   c1   c2   c4
    dta $16, $da, $00, $22 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 4, ROOM_NONE, 2    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 3;    
    
    dta PUT_SLIME,$A8,$02,8
    dta PUT_SLIME,$56,$08,9
    dta PUT_SLIME,$8A,$11,10        
    

    dta PUT_SPRITES, a(sprite_fly) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1    ;    flipable: array[0..1] of byte;
    dta 66,61   ;    x: array[0..1] of byte;
    dta 32,34   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 2,1     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_fly2,0  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_fly2,0  ;    param2: array[0..1] of byte; // addr, goal to check if active      
    
    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 6, 10, 0, 40, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_energy);

    dta LEVEL_END





room_04    
;;  c0   c1   c2   c4
    dta $96, $be, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 5, 24, ROOM_NONE, 3    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 4;    

    dta PUT_SLIME,$C5,$02,11
    dta PUT_SLIME,$3B,$06,12
    dta PUT_SLIME,$7C,$11,13


    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 7, 132, 44, 140, 60, ON_NO_GOAL, 1, SHOW_MSG, 0, 50 
	dta SET_MESSAGE, MSG_INFO, a(msg_warn1);
    
    dta PUT_TILE, ON_NO_GOAL, 1, 33, 20, a(tile_cables) ; condition, param, x, y, tile_address         
    dta PUT_TILE, ON_GOAL, 1, 33, 20, a(tile_cables_off) ; condition, param, x, y, tile_address         


    dta LEVEL_END




 
room_05    
;;  c0   c1   c2   c4
    dta $a6, $ce, $00, $b2 ; level colors
    ;;  gnd  kill bck  brdr
    dta 10, ROOM_NONE, 4, 6    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 5;    
    
    dta PUT_SLIME,$C1,$06,14
    dta PUT_SLIME,$6C,$0A,15
    dta PUT_SLIME,$90,$0D,16    

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 8, 0, 0, 50, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_follow);

	dta SET_TRIGGER, 9, 60, 36, 70, 46, ONCE, NONE, SHOW_MSG, 1, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_map);

	dta SET_TRIGGER, 20, 20, 36, 50, 46, ON_NO_GOAL, 5, SHOW_MSG, 2, 250 
    dta SET_MESSAGE, MSG_INFO, a(msg_rat);
        
    dta PUT_TILE, ON_NO_GOAL, 5, 10, 40, a(tile_mouse) ; condition, param, x, y, tile_address    
    
    dta PUT_SPRITES, a(sprite_map) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_GOAL  ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime; 4 - main goal ;
	:2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 71   ;    x: array[0..1] of byte;
    :2 dta 37   ;    y: array[0..1] of byte;
    :2 dta 7     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 0     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE    ;    moves: array[0..1] of byte;
    :2 dta 0     ;    param1: array[0..1] of byte; // room
    :2 dta 0     ;    param2: array[0..1] of byte; //    
    
    
    dta LEVEL_END




room_06    
;;  c0   c1   c2   c4
    dta $16, $bb, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 5, ROOM_NONE, 7    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 6;    
    
    dta PUT_SLIME,$C3,$02,17
    dta PUT_SLIME,$9A,$05,18
    dta PUT_SLIME,$C1,$06,19
    dta PUT_SLIME,$63,$07,20
    dta PUT_SLIME,$3B,$0A,21
    dta PUT_SLIME,$73,$0D,22    
    
    dta PUT_TILE, ON_NO_GOAL, 2, 4, 50, a(tile_radioactive) ; condition, param, x, y, tile_address  

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 10, 60, 0, 100, 79, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_squeze);

    dta LEVEL_END
 

room_07
;;  c0   c1   c2   c4
    dta $74, $6a, $b2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 6, ROOM_NONE, 8    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 7;    

    dta PUT_SLIME,$BD,$02,23
    dta PUT_SLIME,$C2,$06,24
    dta PUT_SLIME,$80,$0A,25
    dta PUT_SLIME,$4C,$0D,26
    dta PUT_SLIME,$A3,$11,27    

    dta PUT_SPRITES, a(sprite_xvdial) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 119,54   ;    x: array[0..1] of byte;
    dta 36,49   ;    y: array[0..1] of byte;
    dta 2,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 6,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 6, <path_drop1      ;    param1: array[0..1] of byte; // room
    dta SFX_CONSOLE, >path_drop1     ;    param2: array[0..1] of byte; //    

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 11, 100, 0, 159, 30, ONCE, NONE, SHOW_MSG, 0 , 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_switchit);

    dta LEVEL_END
    
     


room_08
;;  c0   c1   c2   c4
    dta $96, $0e, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 7, ROOM_NONE, 9    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 8;    
    
    dta PUT_SLIME,$8C,$02,28
    dta PUT_SLIME,$4E,$06,29
    dta PUT_SLIME,$8A,$11,30    

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
	dta SET_TRIGGER, 12, 100, 0, 159, 40, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_lines);

	dta SET_TRIGGER, 13, 75, 64, 90, 79, ONCE, NONE, SHOW_MSG, 1, 250 
	dta SET_MESSAGE, MSG_INFO, a(msg_cats);


    dta PUT_SPRITES, a(sprite_fly) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3 
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1    ;    flipable: array[0..1] of byte;
    dta 84,61   ;    x: array[0..1] of byte;
    dta 26,34   ;    y: array[0..1] of byte;
    dta $ff,$ff   ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 0,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 1,1     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ONPATH, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta <path_fly,0  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta >path_fly,0  ;    param2: array[0..1] of byte; // addr, goal to check if active         


    dta LEVEL_END

 


room_09
;;  c0   c1   c2   c4
    dta $86, $0e, $62, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 14, 8, ROOM_NONE, 21    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 9;    
    
    dta PUT_SLIME,$AD,$08,31
    dta PUT_SLIME,$46,$09,32
    dta PUT_SLIME,$C6,$0C,33
    dta PUT_SLIME,$7F,$11,34    

    dta PUT_TILE, ON_NO_GOAL, 3, 5, 0, a(tile_smokeleft) ; condition, param, x, y, tile_address  
    dta PUT_TILE, ON_NO_GOAL, 3, 30, 0, a(tile_smokeright) ; condition, param, x, y, tile_address  
    dta PUT_TILE, ON_NO_GOAL, 3, 16, 34, a(tile_smokemid) ; condition, param, x, y, tile_address  

    dta PUT_SPRITES, a(sprite_flame) ;
    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3 
    :2 dta STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 77    ;    x: array[0..1] of byte;
    :2 dta 47    ;    y: array[0..1] of byte;
    :2 dta 3     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 6     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta 0  ;    param2: array[0..1] of byte; //   



    dta LEVEL_END
    

     
    

room_10
;;  c0   c1   c2   c4
    dta $16, $1c, $02, $00 ; level colors
    ;;  gnd  kill  bck  brdr
    dta 13, ROOM_NONE, 5, 11    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 10;    

    dta PUT_SLIME,$6E,$02,35
    dta PUT_SLIME,$3C,$05,36
    dta PUT_SLIME,$C4,$0B,37
    dta PUT_SLIME,$7E,$11,38

    dta PUT_TILE, ON_NO_GOAL, 4, 17, 7, a(tile_hatch1) ; condition, param, x, y, tile_address  
    dta PUT_TILE, ON_NO_GOAL, 5, 36, 22, a(tile_trapdoor1) ; condition, param, x, y, tile_address  
    dta PUT_TILE, ON_NO_GOAL, 5, 33, 62, a(tile_mouse2) ; condition, param, x, y, tile_address  



    dta PUT_SPRITES, a(sprite_fireext) ;
    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3 
    :2 dta STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 132   ;    x: array[0..1] of byte;
    :2 dta 16    ;    y: array[0..1] of byte;
    :2 dta 3     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 5     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE     ;    moves: array[0..1] of byte;
    :2 dta 9      ;    param1: array[0..1] of byte; // room
    :2 dta SFX_SIREN  ;    param2: array[0..1] of byte; //   

    dta LEVEL_END

room_11
;;  c0   c1   c2   c4
    dta $88, $1c, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 10, ROOM_NONE, 12    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 11;    

    dta PUT_SLIME,$62,$03,39
    dta PUT_SLIME,$42,$04,40
    dta PUT_SLIME,$80,$05,41
    dta PUT_SLIME,$A0,$06,42
    dta PUT_SLIME,$62,$07,43
    dta PUT_SLIME,$41,$08,44
    dta PUT_SLIME,$80,$09,45
    dta PUT_SLIME,$A0,$0A,46
    dta PUT_SLIME,$62,$0B,47
    dta PUT_SLIME,$41,$0C,48
    dta PUT_SLIME,$80,$0D,49
    dta PUT_SLIME,$A1,$0E,50
    dta PUT_SLIME,$62,$0F,51
    dta PUT_SLIME,$41,$10,52

    dta PUT_SPRITES, a(sprite_sprays) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3     
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 110,17   ;    x: array[0..1] of byte;
    dta 57,22   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 2,2     ;    frameOff: array[0..1] of byte;
    dta 1,1     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_sprayRight, <path_sprayLeft   ;    param1: array[0..1] of byte; // room
    dta >path_sprayRight, >path_sprayLeft     ;    param2: array[0..1] of byte; //    

    dta LEVEL_END

room_12
;;  c0   c1   c2   c4
    dta $b8, $1c, $c2, $82 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 11, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 12;    

    dta PUT_SLIME,$91,$0C,53
    dta PUT_SLIME,$AC,$11,54

    dta PUT_SPRITES, a(sprite_phone) ;
    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 2       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3     
    :2 dta STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 100   ;    x: array[0..1] of byte;
    :2 dta 57    ;    y: array[0..1] of byte;
    :2 dta 4     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 8     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE     ;    moves: array[0..1] of byte;
    :2 dta 10      ;    param1: array[0..1] of byte; // room
    :2 dta SFX_CONSOLE  ;    param2: array[0..1] of byte; //   

    dta LEVEL_END

room_13
;;  c0   c1   c2   c4
    dta $98, $3c, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 10, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 13;    
 
    dta PUT_SLIME,$5D,$0C,55
    dta PUT_SLIME,$C5,$11,56
    dta PUT_SLIME,$7C,$12,57

    dta PUT_SPRITES, a(sprite_screen_lever) ;

    dta SPRITE_TWO   ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 112,146  ;    x: array[0..1] of byte;
    dta 55,51   ;    y: array[0..1] of byte;
    dta 6,1     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,5     ;    frameOff: array[0..1] of byte;
    dta 1,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 19, 4   ;    param1: array[0..1] of byte; // room
    dta 0, SFX_GEAR     ;    param2: array[0..1] of byte; //    

    dta LEVEL_END


room_14    
;;  c0   c1   c2   c4
    dta $26, $bc, $a2, $82 ; level colors
    ;;  gnd  kill bck  brdr
    dta 15, ROOM_NONE, 9, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 14;    

    dta PUT_SLIME,$7F,$06,58
    dta PUT_SLIME,$B2,$07,59
    dta PUT_SLIME,$4B,$08,60
    dta PUT_SLIME,$B2,$0C,61
    dta PUT_SLIME,$4B,$0D,62
    dta PUT_SLIME,$B2,$11,63
    dta PUT_SLIME,$4B,$12,64

    dta LEVEL_END

room_15    
;;  c0   c1   c2   c4
    dta $28, $76, $94, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta 16, ROOM_NONE, 14, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 15;    
    
    dta COLOR_COLLISION_OFF    
    
    dta PUT_SLIME,$93,$08,65
    dta PUT_SLIME,$6E,$09,66
    dta PUT_SLIME,$B7,$0A,67
    dta PUT_SLIME,$4F,$0B,68


    dta LEVEL_END


room_16
;;  c0   c1   c2   c4
    dta $88, $8c, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 19, ROOM_NONE, 15, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 16;    

    dta COLOR_COLLISION_OFF

    dta PUT_SLIME,$A2,$08,69
    dta PUT_SLIME,$53,$0B,70    
    
    dta LEVEL_END


room_17
;;  c0   c1   c2   c4
    dta $26, $74, $12, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta 20, ROOM_NONE, 18, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 17;    
    
    dta COLOR_COLLISION_OFF    
        
    dta PUT_SLIME,$80,$05,71
    dta PUT_SLIME,$AF,$06,72
    dta PUT_SLIME,$C5,$07,73
    dta PUT_SLIME,$37,$08,74
    dta PUT_SLIME,$B8,$0A,75
    dta PUT_SLIME,$73,$0D,76
    dta PUT_SLIME,$50,$0E,77
    dta PUT_SLIME,$38,$0F,78
    dta PUT_SLIME,$AB,$11,79

    dta LEVEL_END


room_18
;;  c0   c1   c2   c4
    dta $24, $7a, $82, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 17, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 18;    
    
    dta COLOR_COLLISION_OFF

    dta PUT_SLIME,$58,$02,80
    dta PUT_SLIME,$BF,$10,81
    dta PUT_SLIME,$44,$11,82

    dta PUT_SPRITES, a(sprite_exit) ;

    dta SPRITE_SINGLE    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_EXIT, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 123,38   ;    x: array[0..1] of byte;
    dta 65,14   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 3,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_NONE     ;    moves: array[0..1] of byte;
    dta 0, 0      ;    param1: array[0..1] of byte; // room
    dta 0, 0     ;    param2: array[0..1] of byte; //    

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 14, 10, 40, 150, 79, ON_NO_GOAL, 7, SHOW_MSG, 0, 50
	dta SET_MESSAGE, MSG_WARNING, a(msg_getmap);


    dta LEVEL_END


room_19
;;  c0   c1   c2   c4
    dta $88, $8c, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 16, ROOM_NONE, 20, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 19;    

    dta COLOR_COLLISION_OFF

    dta PUT_SLIME,$49,$02,83
    dta PUT_SLIME,$8F,$05,84
    dta PUT_SLIME,$B4,$0C,85
    dta PUT_SLIME,$66,$0E,86    

    dta LEVEL_END

room_20    
;;  c0   c1   c2   c4
    dta $28, $76, $94, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta 19, ROOM_NONE, 17, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 15; 
       
    dta COLOR_COLLISION_OFF       
       
    dta PUT_SLIME,$A2,$08,87
    dta PUT_SLIME,$53,$0B,88          

    dta LEVEL_END
    
    
room_21
    dta $96, $8c, $62, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 9, ROOM_NONE, ROOM_NONE   ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 20; 
      
    dta PUT_SLIME,$A4,$0A,89
    dta PUT_SLIME,$94,$11,90      


    dta PUT_SPRITES, a(sprite_bell_flame) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 91,91   ;    x: array[0..1] of byte;
    dta 45,62   ;    y: array[0..1] of byte;
    dta 5,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 8,0     ;    frameOff: array[0..1] of byte;
    dta 7,7     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    dta 10, 0      ;    param1: array[0..1] of byte; // room
    dta SFX_BELL, 0     ;    param2: array[0..1] of byte; //    
      
    dta LEVEL_END

room_22
    dta $16, $8a, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 23, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 21; 

    dta PUT_SLIME,$81,$02,91
    dta PUT_SLIME,$3A,$06,92
    dta PUT_SLIME,$6F,$09,93
    dta PUT_SLIME,$8D,$0B,94
    dta PUT_SLIME,$4A,$0E,95
    dta PUT_SLIME,$39,$11,96
    dta PUT_SLIME,$B7,$12,97

    dta PUT_SPRITES, a(sprite_stats_portal) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_STATS, STYPE_PORTAL     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 15,39   ;    x: array[0..1] of byte;
    dta 36,6    ;    y: array[0..1] of byte;
    dta $ff,1   ;    goal
    dta 0,54    ;    frameOn: array[0..1] of byte;
    dta 0,72    ;    frameOff: array[0..1] of byte;
    dta 15,8     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    dta 0,0   ;    param1: array[0..1] of byte; // room
    dta 0,0   ;    param2: array[0..1] of byte; //    
       
    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 15, 30, 5, 50, 20, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 1, 0
    dta SET_TRIGGER, 16, 30, 5, 50, 20, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret1);           

    dta LEVEL_END

room_23
    dta $26, $B8, $92, $80 ; level colors
    ;;  gnd  kill bck  brdr
    dta 22, ROOM_NONE, ROOM_NONE, ROOM_NONE    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 22; 
    
    dta PUT_SLIME,$7D,$03,98
    dta PUT_SLIME,$3B,$04,99
    dta PUT_SLIME,$94,$06,100
    dta PUT_SLIME,$B0,$07,101
    dta PUT_SLIME,$6C,$0A,102
    dta PUT_SLIME,$44,$0B,103
    dta PUT_SLIME,$59,$0E,104
    dta PUT_SLIME,$9A,$10,105
    dta PUT_SLIME,$B9,$11,106
      
    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 144   ;    x: array[0..1] of byte;
    :2 dta 12   ;    y: array[0..1] of byte;
    :2 dta 9     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //         

    dta LEVEL_END
    

room_24
    dta $86, $B8, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, ROOM_NONE, 4    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 23; 
    
    dta PUT_SLIME,$7D,$01,107
    dta PUT_SLIME,$71,$02,108    
      
    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 142   ;    x: array[0..1] of byte;
    :2 dta 48   ;    y: array[0..1] of byte;
    :2 dta $a     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //         

    ;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
    dta SET_TRIGGER, 17, 140, 55, 160, 70, ONCE, NONE, SHOW_MSG, 0, 250 
    dta SET_TRIGGER, 18,  5, 10, 30, 70, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 2, 0
    dta SET_TRIGGER, 19,  5, 10, 30, 70, ONCE, NONE, SHOW_MSG, 1, 250
    
	dta SET_MESSAGE, MSG_WARNING, a(msg_yesyouare);
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret2);        

    dta LEVEL_END

rmt
    ins 'worldmusic.apu'

    org $A000,$C000
    
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SPRITE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    

sprite_cagedoor
    icl 'sprite_cagedoor.a65'

sprite_xvdial
    icl 'sprite_xvdial.a65'
    

sprite_flame
    icl 'sprite_flame.a65'
    
sprite_fly
    icl 'sprite_fly.a65'

sprite_fireext
    icl 'sprite_fireext.a65'
    
sprite_sprays
    icl 'sprite_sprays.a65'

sprite_phone
    icl 'sprite_phone.a65'
        
sprite_screen_lever
    icl 'sprite_screen_lever.a65'
    
sprite_map
    icl 'sprite_map.a65'
    
sprite_exit
    icl 'sprite_goal.a65'
    
sprite_bench_portal
    icl 'sprite_bench_portal.a65'

sprite_stats_portal
    icl 'sprite_stats_portal.a65'
    
sprite_bigslime
    icl 'sprite_bigslime.a65'
    
sprite_bell_flame
    icl 'sprite_bell_flame.a65'
    
sprite_drop_eyes
    icl 'sprite_drop_eyes.a65'
    
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TILES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    
tile_smokeleft
    dta 5,37
    ins 'smokeleft.gfx'
    
tile_smokeright    
    dta 4,37
    ins 'smokeright.gfx'
    
tile_smokemid
    dta 8,13
    ins 'smokemid.gfx'
    
    
tile_cables
    dta 5,25
    ins 'cables.gfx'

tile_cables_off
    dta 5,23
    ins 'cables_off.gfx'

    
tile_radioactive    
    dta 8,23
    ins 'radioactive.gfx'
    
tile_trapdoor1
    dta 3,12
    ins 'trapdoor1.gfx'

tile_hatch1
    dta 3,22
    ins 'hatch.gfx'

tile_mouse
    dta 4,6
    ins 'mouse.gfx'

tile_mouse2
    dta 3,9
    ins 'mouse2.gfx'


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PATHS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


path_fly
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0
    dta 40,0,1,1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0
    dta 20,-1,-1,1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0
    dta 20,1,-1,1
    dta 0

path_fly2
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0
    dta 36,1,0,1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0
    dta 18,-1,2,1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0
    dta 18,-1,-2,1
    dta 0


path_drop1
    dta 22, 0, 1, $70
    
    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0
    
    dta 2, 0, 0, 1
    dta 1, 0, -22, $70

    dta 20, 0, 0, 0
    
    dta 22, 0, 1, $70

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 2, 0, 0, 1
    dta 1, 0, -22, $70

    dta 50, 0, 0, 0

    dta 22, 0, 1, $70

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 2, 0, 0, 1
    dta 1, 0, -22, $70

    dta 30, 0, 0, 0

    dta 22, 0, 1, $70

    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 2, 0, 0, 1
    dta 1, 0, -22, $70

    dta 10, 0, 0, 0
    
    dta 0

path_sprayLeft

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 10,0,0,$72
    dta 1,31,35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 20,0,0,$72
    dta 1,31,-35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 30,0,0,$72
    dta 1,-62,35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 20,0,0,$72
    dta 1,31,-35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 10,0,0,$72
    dta 1,31,35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 30,0,0,$72
    dta 1,-62,-35,0

    dta 0

path_sprayRight

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 40,0,0,$72
    dta 1,-62,-35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 10,0,0,$72
    dta 1,31,35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 20,0,0,$72
    dta 1,31,-35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 30,0,0,$72
    dta 1,-62,35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 20,0,0,$72
    dta 1,31,-35,0

    dta $ff; play fx
	dta SFX_GAS;
	dta 0,0
    dta 30,0,0,1
    dta 10,0,0,$72
    dta 1,31,35,0
    
    dta 0

path_cateyes
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 15,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 40,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 30,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 10,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71

    dta 25,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 0

path_droplab
    dta 80,0,0,$70
    
    dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 5,0,0,1
    dta 0

   
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TILES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


.array msg_secret1[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "      Statistical Research Center       "
.enda

.array msg_secret2[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "          Q&A Landfill Site             "
.enda

.array msg_flipcage[80] .byte = 0
   [2] = "Look for a way to escape!"
   [57] = "Try to flip the cage."
.enda

.array msg_cankillyou[80] .byte = 0
   [2] = "Be careful... you are fragile!"
   [48] = "Many things can kill you here."
.enda

.array msg_avoidbright[80] .byte = 0
   [2] = "These bright things look dangerous."
   [47] = "You'd better avoid them..."
.enda

.array mgs_explode[80] .byte = 0
   [2] = "Unless you want to explode..."
.enda

.array mgs_anddie[80] .byte = 0
   [2] = "...and die."
   [54] = "Drained of vital energy."
.enda

.array msg_labo[80] .byte = 0
   [2] = "It seems to be where this"
   [53] = "tasty pink slime is made."
.enda

.array msg_energy[80] .byte = 0
   [8] = "You'd better hurry up!"
   [42] = "Energy is running out all the time."
.enda

.array msg_warn1[80] .byte = 0
   [2] = "I wouldn't even recommend trying."
.enda

.array msg_follow[80] .byte = 0
   [2] =  "Follow slimes to catch the Doctor."
   [46] = "The Recipe must be yours!"
.enda

.array msg_map[80] .byte = 0
   [2] =  "      You have found the Map!"
   [42] = "Time to leave and find 4 ingredients."
.enda

.array msg_rat[80] .byte = 0
   [2] =  "           Filthy rats!"
   [42] = "  They are afraid of loud noises."
.enda

.array msg_squeze[80] .byte = 0
   [2] =  "You can squeeze through narrow gaps."
   [54] = "Just sayin'."
.enda    

.array msg_switchit[80] .byte = 0
   [2] =  "Hmm... interesting."
   [51] = "I would press some buttons."
.enda   

.array msg_lines[80] .byte = 0
   [2] =  "You can slip through the thin lines."
   [52] = "Just be warned."
.enda    

.array msg_cats[80] .byte = 0
   [3] =  "Can you smell cats in this alley?"
.enda   

.array msg_getmap[80] .byte = 0
   [8] =  "You need to find a map,"
   [45] = "otherwise you will get lost!"
.enda  

.array msg_yesyouare[80] .byte = 0
   [17] =  "Yes."
   [55] = "You are."
.enda   

    
