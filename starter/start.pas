program madStrap;
{$librarypath '../../blibs/'}
uses atari, aplib, b_system, rmt;

const
{$i const.inc}
{$i '../cart_builder/bank_0_files.inc'}

{$r resources.rc}


var
    banks: array [0..128] of byte absolute $D500;       
    strig0: byte absolute $D010;
    c:byte;
    frame:word;
    msx: TRMT;
begin
    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);
    SystemOff($fe);
    banks[0]:=0;
    colpf2 := 0;
    colpf1 := 0;
    colbk := 0;
    frame := 0;
    msx.Init(0);
    unApl(pointer(B0_TGSTUDIO.APU), pointer(VIDEO_RAM_ADDRESS));   
    Waitframe;
    Dpoke($D402, DISPLAY_LIST_ADDRESS);
    dmactl :=  %00100010;
    c:=0;
    repeat
        colpf1 := c;
        if frame and 1 = 0 then begin
            if (frame<50) and (c<12) then inc(c);
            if (frame>140) and (c>0) then dec(c);
        end;
        inc(frame);
        WaitFrame;
        msx.Play;
        msx.Play;
        msx.Play;
    until (strig0 = 0) or (frame > 170);
    waitframe;
    colpf2 := 0;
    colpf1 := 0;
    msx.Stop;
    Fillbyte(pointer(VIDEO_RAM_ADDRESS),80*40,0);
    unApl(pointer(B0_MQLOGO.APU), pointer(VIDEO_RAM_ADDRESS+9*40));   
    msx.Init(1);
    frame := 0;
    c:=0;
    repeat
        colpf1 := c;
        if frame and 1 = 0 then begin
            if (frame<50) and (c<12) then inc(c);
            if (frame>120) and (c>0) then dec(c);
        end;
        inc(frame);
        WaitFrame;
        if frame>20 then begin
            msx.Play;
            msx.Play;
            msx.Play;
        end;
    until (strig0 = 0) or (frame > 150);
    WaitFrame;
    msx.Stop;
    WaitFrame;
    dmactl := %00100000;
    nmien := 0;
    asm { rts };
end.
