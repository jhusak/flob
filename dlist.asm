; Here is place for your custom display list definition.
; Handy constants are defined first:

DL_BLANK1 = 0; // 1 blank line
DL_BLANK2 = %00010000; // 2 blank lines
DL_BLANK3 = %00100000; // 3 blank lines
DL_BLANK4 = %00110000; // 4 blank lines
DL_BLANK5 = %01000000; // 5 blank lines
DL_BLANK6 = %01010000; // 6 blank lines
DL_BLANK7 = %01100000; // 7 blank lines
DL_BLANK8 = %01110000; // 8 blank lines

DL_DLI = %10000000; // Order to run DLI
DL_LMS = %01000000; // Order to set new memory address
DL_VSCROLL = %00100000; // Turn on vertical scroll on this line
DL_HSCROLL = %00010000; // Turn on horizontal scroll on this line

DL_MODE_40x24T2 = 2; // Antic Modes
DL_MODE_40x24T5 = 4;
DL_MODE_40x12T5 = 5;
DL_MODE_20x24T5 = 6;
DL_MODE_20x12T5 = 7;
DL_MODE_40x24G4 = 8;
DL_MODE_80x48G2 = 9;
DL_MODE_80x48G4 = $A;
DL_MODE_160x96G2 = $B;
DL_MODE_160x192G2 = $C;
DL_MODE_160x96G4 = $D;
DL_MODE_160x192G4 = $E;
DL_MODE_320x192G2 = $F;

DL_JMP = %00000001; // Order to jump
DL_JVB = %01000001; // Jump to begining
    
; It's always useful to include you program global constants here
    icl 'const.inc'

; and declare display list itself

; example (BASIC mode 0 + display list interrupt at top):
display_list
  dta $70, $50, $70 + DL_DLI, $42 + DL_DLI, a(TXT_RAM_ADDRESS), $70 + DL_DLI
  
  dta $4d, a(VIDEO_RAM_ADDRESS), $4d, a(VIDEO_RAM_ADDRESS+$28), $4d, a(VIDEO_RAM_ADDRESS+$50), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$78)
  dta $4d, a(VIDEO_RAM_ADDRESS+$a0), $4d, a(VIDEO_RAM_ADDRESS+$c8), $4d, a(VIDEO_RAM_ADDRESS+$f0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$118)
  dta $4d, a(VIDEO_RAM_ADDRESS+$140), $4d, a(VIDEO_RAM_ADDRESS+$168), $4d, a(VIDEO_RAM_ADDRESS+$190), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$1b8)
  dta $4d, a(VIDEO_RAM_ADDRESS+$1e0), $4d, a(VIDEO_RAM_ADDRESS+$208), $4d, a(VIDEO_RAM_ADDRESS+$230), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$258)
  dta $4d, a(VIDEO_RAM_ADDRESS+$280), $4d, a(VIDEO_RAM_ADDRESS+$2a8), $4d, a(VIDEO_RAM_ADDRESS+$2d0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$2f8) 

  dta $4d, a(VIDEO_RAM_ADDRESS+$320), $4d, a(VIDEO_RAM_ADDRESS+$348), $4d, a(VIDEO_RAM_ADDRESS+$370), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$398)
  dta $4d, a(VIDEO_RAM_ADDRESS+$3c0), $4d, a(VIDEO_RAM_ADDRESS+$3e8), $4d, a(VIDEO_RAM_ADDRESS+$410), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$438)
  dta $4d, a(VIDEO_RAM_ADDRESS+$460), $4d, a(VIDEO_RAM_ADDRESS+$488), $4d, a(VIDEO_RAM_ADDRESS+$4b0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$4d8)
  dta $4d, a(VIDEO_RAM_ADDRESS+$500), $4d, a(VIDEO_RAM_ADDRESS+$528), $4d, a(VIDEO_RAM_ADDRESS+$550), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+$578)
  dta $4d, a(VIDEO_RAM_ADDRESS+$5a0), $4d, a(VIDEO_RAM_ADDRESS+$5c8), $4d, a(VIDEO_RAM_ADDRESS+$5f0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(39*40))

  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$28), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$50), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$78)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$a0), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$c8), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$f0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$118)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$140), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$168), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$190), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$1b8)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$1e0), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$208), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$230), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$258)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$280), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$2a8), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$2d0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$2f8)

  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$320), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$348), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$370), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$398) 
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$3c0), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$3e8), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$410), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$438)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$460), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$488), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$4b0), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$4d8)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$500), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$528), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$550), $4d + DL_DLI, a(VIDEO_RAM_ADDRESS+(40*40)+$578)
  dta $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$5a0), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$5c8), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$5f0), $4d, a(VIDEO_RAM_ADDRESS+(40*40)+$618)
      
  dta $40 + DL_DLI, $42, a(TXT_RAM_ADDRESS + 40), $20, $02
      
  dta $41, a(DISPLAY_LIST_ADDRESS)
