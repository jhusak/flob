(* declare your interrupt routines here *)

procedure dliachieve;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.titleColors+3 atari.colpf0
    mva adr.titleColors+4 atari.colpf1
    mva adr.titleColors+5 atari.colpf2
    mva #0 atari.colbk

@
    sta wsync
    lda vcount
    cmp #22
    bmi @-

    sta wsync
    mva achievementsBarCol atari.colpf0
    
    :5 sta wsync
    
    mva adr.titleColors   atari.colpf0
    mva adr.titleColors+1 atari.colpf1
    mva adr.titleColors+2 atari.colpf2

@
    sta wsync
    lda vcount
    cmp #92
    bmi @-

    sta wsync
    mva adr.titleColors+6 atari.colpf0
    mva adr.titleColors+7 atari.colpf1
    mva adr.titleColors+8 atari.colpf2

@
    sta wsync
    lda vcount
    cmp #104
    bmi @-

    sta wsync
    mva achievementsBarCol atari.colpf0
    
    :3 sta wsync

    sta wsync
    mva #4 atari.colpf1
    mva #0 atari.colpf2

    :8 sta wsync
    mva #12 atari.colpf1

    mwa #dliselect1 __dlivec 
   
    pla ; restore registers    
};
    
end;


procedure dliselect;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.titleColors+3 atari.colpf0
    mva adr.titleColors+4 atari.colpf1
    mva adr.titleColors+5 atari.colpf2
    mva #0 atari.colbk

@
    sta wsync
    lda vcount
    cmp #30
    bmi @-
    
    sta wsync
    mva adr.titleColors   atari.colpf0
    mva adr.titleColors+1 atari.colpf1
    mva adr.titleColors+2 atari.colpf2

@
    sta wsync
    lda vcount
    cmp #74
    bmi @-

    sta wsync
    mva adr.titleColors+3 atari.colpf0
    mva adr.titleColors+4 atari.colpf1
    mva adr.titleColors+5 atari.colpf2
    
    mwa #dliselect1 __dlivec 

    pla ; restore registers
};
end;

procedure dliselect1;assembler;interrupt;
asm {
    pha ; store registers
    
    :8 sta wsync
    
    mva world.goalcolor1 atari.colpf0
    mva world.goalcolor2 atari.colpf1

    :18 sta wsync
    
    mva adr.titleColors+6 atari.colpf0
    mva adr.titleColors+7 atari.colpf1
    mva adr.titleColors+8 atari.colpf2

    mwa #dliselect __dlivec 

   
    pla ; restore registers
};
end;

procedure dlicredits;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.titleColors   atari.colpf0
    mva adr.titleColors+1 atari.colpf1
    mva adr.titleColors+2 atari.colpf2
    mva #0 atari.colbk
    mwa #dlicredits2 __dlivec 
   
    pla ; restore registers
};
end;

procedure dlicredits2;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.titleColors+3  atari.colpf0
    mva adr.titleColors+4 atari.colpf1
    mva adr.titleColors+5 atari.colpf2
    mva #0 atari.colbk
    mwa #dlicredits __dlivec 
   
    pla ; restore registers
};
end;

procedure dliintro;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.levelColors   atari.colpf0
    mva adr.levelColors+1 atari.colpf1
    mva adr.levelColors+2 atari.colpf2
    mva #0 atari.colbk
    mwa #dliintro2 __dlivec 
   
    pla ; restore registers
};
end;

procedure dliintro2;assembler;interrupt;
asm {
    pha ; store registers

    mva #0 atari.colpf2
    mva adr.messageTypes atari.colpf1
    :11 sta wsync
    mva adr.messageTypes+1 atari.colpf1
    :11 sta wsync
    mva adr.messageTypes+2 atari.colpf1
    :11 sta wsync
    mva adr.messageTypes+3 atari.colpf1
    
    mwa #dliintro __dlivec 
   
    pla ; restore registers
};
end;


procedure dlititle;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.titleColors   atari.colpf0
    mva adr.titleColors+1 atari.colpf1
    mva adr.titleColors+2 atari.colpf2
    mva #0 atari.colbk
    mwa #dlititle2 __dlivec 
   
    pla ; restore registers
};
end;

procedure dlititle2;assembler;interrupt;
asm {
    pha ; store registers
    
l1
    sta wsync
    lda vcount
    cmp #90
    bmi l1 

    mva adr.titleColors+3 atari.colpf0
    mva adr.titleColors+4 atari.colpf1
    mva adr.titleColors+5 atari.colpf2

    mwa #dlititle __dlivec 
   
    pla ; restore registers
};
end;


procedure dli1;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.levelColors+3 atari.colpf2 
    mva #TOP_FOREGROUND atari.colpf1 
    mwa #dli2 __dlivec 
    
    pla ; restore registers
};
end;

procedure dli2;assembler;interrupt;
asm {
    pha ; store registers
    
    sta wsync
    mva adr.levelColors+3 atari.colpf1+3
    mva adr.levelColors+2 atari.colpf0+2
    mva adr.levelColors+1 atari.colpf0+1
    mva adr.levelColors atari.colpf0

    mwa #dli3 __dlivec

    pla
};
end;

procedure dli3;assembler;interrupt;
asm {
    pha ; store registers
    txa
    pha 
     
    ldx dlrow
    lda adr.slimeX,x 
    add dxslime0
    sta wsync
    sta hposm0
    lda adr.slimeX,x 
    add dxslime1
    sta hposm1
.def :dl_operation
    inc dlrow

    lda dlrow
    bmi switch_interrupt
    cmp #20
    beq switch_interrupt
    sne
switch_interrupt 
    mwa #dli4 __dlivec 

    pla ; restore registers
    tax
    pla
};
end;

procedure dli4;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.levelColors+3 atari.colpf2 
    mva topBarForeground atari.colpf1
    
    pla ; restore registers
};
end;


procedure dlistats;assembler;interrupt;
asm {
    pha ; store registers

    mva adr.titleColors   atari.colpf0
    mva adr.titleColors+1 atari.colpf1
    mva adr.titleColors+2 atari.colpf2
    
    mwa #dlistats2 __dlivec
    
    pla ; restore registers
};
end;

procedure dlistats2;assembler;interrupt;
asm {
    pha ; store registers

    mva #0 atari.colpf2
    mva #$f atari.colpf1

    :4 sta wsync
    sta wsync
    mva #$d atari.colpf1
    sta wsync
    mva #$b atari.colpf1
    sta wsync
    mva #$9 atari.colpf1
    sta wsync
    mva #$7 atari.colpf1
    sta wsync
    mva #STAT_ON atari.colpf1
     
@
    sta wsync
    lda vcount
    cmp #52
    bmi @-
    mva statBestsBack atari.colpf1
    
@
    sta wsync
    lda vcount
    cmp #68
    bmi @-
    mva #STAT_ON atari.colpf1
    
@
    sta wsync
    lda vcount
    cmp #96
    bmi @-
    mva statGlobalBack atari.colpf1
    
    
    mwa #dlistats __dlivec
    
    pla ; restore registers
};
end;



procedure vbl;assembler;interrupt;
asm {
    phr ; store registers
        
    mwa dlivec __dlivec
    mwa dlvec $D402
    mva dmactls atari.dmactl

    mva adr.pmgColors atari.colpm0
    mva adr.pmgColors+1 atari.colpm1
    mva adr.pmgColors+2 atari.colpm2
    mva adr.pmgColors+3 atari.colpm3

    lda 20
    lsr:lsr:lsr:lsr 
    and #1
    sta dxslime0
    eor #1        
    sta dxslime1        

    lda flipped
    bne upside
    
    ldx #$EE ; INC HEXCODE
    lda #0 
    beq @+
upside     
    ldx #$CE ; DEC HEXCODE
    lda #19

@
    stx dl_operation
    sta dlrow

    
play_music     

    lda music_on 
    beq player_end

    ;*** RMT play routine
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY

player_end


end_of_vbl

    plr ; restore registers
    rti;



.def :putnum
    and #%1111
    ora #16
    sta TXT_RAM_ADDRESS,y;
    dey
    rts

.def :printScore
    ldy #38
    ldx #0
@              
    lda mySlime,x 
    pha
    jsr putnum
    pla
    :4 lsr 
    jsr putnum
    inx
    cpx #3
    bne @-
    rts
    
.def :showScore
    txa
    pha
    jsr printScore;
    pla
    tax
    rts
        
.def :addScore
    txa
    pha
    sed
    clc
    lda mySlime
    adc scoreadd
    sta mySlime
    lda mySlime+1
    adc scoreadd+1
    sta mySlime+1
    lda mySlime+2
    adc #0
    sta mySlime+2
    lda mySlime+3
    adc #0
    sta mySlime+3
    cld

    jsr printScore
    pla
    tax
    rts
     
.def :subScore
    txa
    pha
    sed
    sec
    lda mySlime
    sbc scoreadd
    sta mySlime
    lda mySlime+1
    sbc #0
    sta mySlime+1
    lda mySlime+2
    sbc #0
    sta mySlime+2
    lda mySlime+3
    sbc #0
    sta mySlime+3
    cld

    jsr printScore
    pla
    tax
    rts     

};
end;
