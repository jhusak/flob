program FloB;
{$librarypath 'blibs/'}
uses atari, b_pmg, b_utils, b_crt, b_system, rmt, aplib, b_maxFlash8Mb; // b_utils;

const
{$i vnumber.inc}
{$i const.inc}
{$i cart_builder/bank_0_files.inc}

{$r resources.rc}
{$i types.inc}
{$i interrupts.inc}
{$i assets/sprites.inc}

{ $define leveleditor} 

var
    sysframe: byte absolute 20;
    gametime: cardinal;
    s,s1: string[40];
    s2: string[1];
    b,joy: byte;
    world: TWorld;
    level: TLevel;
    image: TImage;
    sprites: TSprites;
    spriteData: TSpriteData;
    spritePtr: array[0..1] of word;
    spriteFrame: array[0..1] of byte;
    spriteTargetFrame: array[0..1] of byte;
    spriteStep: array[0..1] of byte;
    spriteAnimation0: TAnimationStep;
    spriteAnimation1: TAnimationStep;
    spriteAnimations: array [0..1] of pointer = (@spriteAnimation0, @spriteAnimation1);
    dlivec: pointer;
    dlvec: pointer;
    dmactls: byte;
    leveldata: array[0..0] of byte;
    banks: array [0..128] of byte absolute $D500;   
    
    actionDelay: byte;
    sfxDelay: byte;
    levelColors: array [0..3] of byte;
    revealColors: array [0..3] of byte;
    pmgColors:  array [0..3] of byte;
    topBarForeground: byte;
    
    flobX, flobY, flobFoot, flobFace, flobDir, flobFrame, flobFalling: byte; 
    flobFrameDelay: byte;

    isNTSC: boolean;
    ntscFrame:byte;
    flipped: boolean;
    colorCollision, mainGoalReached, room_changed, game_over, room_landed, first_landed, exploded, teleported: boolean;
    landedTime: byte;
    cheatmode, statsOn: boolean;
        
    levelNum: byte;
    lastLanded: TLastLanded;
    lastSafeLanded: TLastLanded;
    
    doorReveal: array [0..1] of byte;
    
    stick0: byte absolute $D300;
    strig0: byte absolute $D010;
    invulnerability: byte;
    
    slimeX: array [0..19] of byte;
    slimeGoal: array [0..19] of byte;
    dxslime0, dxslime1, dlrow: byte;
    scoreadd: word;
    mySlime: cardinal;
    pickedSlimeCount: byte;
    secretsFound: byte;
    deadCount: word;
    
    worldsBanks: array [0..WORLD_COUNT-1] of byte = (8, 16, 24, 32, 40, 48); //
    worldCurrent: byte;

    roomTriggers: array [0..3, 0..9] of byte;
    triggerCount: byte;
    msgDisplayCounter: byte;
    msgDisplayType: byte;
    messages: array [0..3] of word;
    messageTypes: array [0..3] of byte;
    sceneImg: array [0..1] of byte;
    sceneMsg: array [0..1] of word;
    sceneCol0,sceneCol1,sceneCol2: array [0..1] of byte;

    msgCount: byte;
    sceneCount: byte;

    msx: TRMT;
    music_on: byte = 0;
    lastSfxCh: byte = 0;
    music_ingame: boolean = true;
    music_track: byte;
    escPressed: byte;

    worldStats: TWorldStats;
    globalStats: TGlobalStats;
    achievement: TAchievement;
    newAchivements: byte;
    achivementsCount: byte;
    statCount, statPage: byte;
    lastSave: byte;
    saveAddress: word;
    saveBank: byte;
    
    NTSCmap: array [0..255] of byte absolute NTSC_TABLE;
    vramLineOffset: array [0..99] of word absolute VRAM_OFFSETS;
    goals:array [0..GOALS_MAX] of byte absolute GOALS_ADDRESS;
    triggers:array [0..TRIGGERS_MAX] of byte absolute TRIGGERS_ADDRESS;
    slimes: array [0..SLIMES_MAX] of byte absolute SLIMES_ADDRESS;
    achievements: array [0..ACHIEVEMENTS_MAX] of byte absolute ACHIEVEMENTS_ADDRESS;
    gameRuns: word absolute GAMERUNS_ADDRESS;
    map2bpp: array [0..3,0..15] of byte absolute MAP2BPP_ADDRESS;

    
// ********************************* COLORS

    titleColors: array [0..8] of byte;
    winColors:array [0..8] of byte =            ($82,0,0,       $18,$14,$1c,    $98,$94,$9A);
    failColors:array [0..8] of byte =           ($38,$3f,$34,   $98,$94,$9c,    $98,$94,$9A);
    achievementsColors:array [0..8] of byte =   ($08,$0c,$02,   $94,$98,$9a,    $16,$1a,$12);
    mainTitleColors:array [0..5] of byte =      ($38,$3f,$34,   $98,$32,$94);
    selectorColors:array [0..8] of byte =       ($38,$3c,$34,   $84,$78,$f,     $98,$1a,$26);
    credColors: array [0..7, 0..2] of byte = (
                                                ($38,$3f,$34),
                                                ($98,$9f,$94),
                                                ($18,$1f,$14),
                                                ($b8,$bf,$b4),
                                                ($78,$7f,$74),
                                                ($28,$2f,$24),
                                                ($e8,$ef,$e4),
                                                ($0a,$0f,$04)
    );
    achievementsBarCol: byte;
    
    statBestsBack: byte = STAT_OFF;
    statGlobalBack: byte = STAT_OFF;
    
// ***************************************************
// *************************************************** HELPERS
// ***************************************************

procedure LoadLevel(lvl:byte);forward;
procedure CheckAchievements;forward;
procedure EndWorld;forward;
procedure ShowAchievements;forward;
procedure PutTile(x,y:byte;src:word;flip:byte);forward;
procedure BackToLevel;forward;
procedure ShowCutScene(csbank,cscount:byte);forward;
procedure ShowCredits;forward;
procedure LoadWorld(w: byte);forward;

procedure SetBank0;
begin
    banks[0]:=0;
end;

procedure PlaySong(p:byte);
begin
    msx.Init(p);
    music_on := 1;
end;

procedure StopSong;
begin
    WaitFrame;
    msx.Stop;
    music_on := 0;
end;

function MapColor(c:byte):byte;
begin
    result := c;
    if isNTSC then result := NTSCmap[c];
end;

function IsStartingButtonDown:boolean;
begin
    CONSOL := 0;
    result := false;
    if (strig0 = 0) then exit(true);
    if CRT_StartPressed then exit(true);
    if (skstat and 8 = 0) then exit(true);
end;

procedure DisplayOff;
begin
    dmactls := 0;
    PMG_gractl := 0;
end;

procedure DisplayOnWithPMG;
begin
    dmactls := %00101110;
    PMG_gractl := PMG_gractl_default;
end;

procedure DisplayOn;
begin
    dmactls := %00100010;
    PMG_gractl := 0;
end;

procedure InitScreen(dladdr: word; dliptr: pointer);
begin
    WaitFrame;
    DisplayOff;
    colbk := $0;
    chbase := Hi(FONT);
    dlvec := pointer(dladdr);
    dlivec := dliptr;
    EnableVBLI(@vbl);
    EnableDLI(dliptr);
end;

procedure ResolveSaveAddress(savenum: byte);
begin
    saveBank := SAVE_BASEBANK + (savenum shr 5);
    saveAddress := ($a0 + (savenum and $1F)) shl 8;
    banks[saveBank] := 0;
end;

procedure StoreStats;
begin
    WaitFrame;
    nmien := 0;
    Inc(lastSave,SAVE_PAGES);
    if lastSave = 0 then begin
        EraseSector(SAVE_SECTOR);
    end;
    ResolveSaveAddress(lastSave);
    BurnBlock(saveBank, WORLD_STATS_ADDRESS, saveAddress, SAVE_SIZE);    
    BurnByte(saveBank, saveAddress + ($100 * SAVE_PAGES) - 1, byte('F'));
    nmien := $40;
    SetBank0;
end;

procedure SetWorldStats(worldNum:byte);
var addr:word;
begin
    addr := worldNum shl 6;
    Move(@WorldStats,pointer(WORLD_STATS_ADDRESS+addr),SizeOf(TWorldStats));
end;

procedure ClearWorldStats;
var worldNum:byte;
begin
    FillByte(@WorldStats, SizeOf(TWorldStats), 0);
    WorldStats.minDeaths := $ffff;
    WorldStats.bestTime := $ffff;
    for worldNum := 0 to 5 do SetWorldStats(worldNum);
end;

procedure GetWorldStats(worldNum:byte);
var addr:word;
begin
    addr := worldNum shl 6;
    Move(pointer(WORLD_STATS_ADDRESS+addr), @WorldStats,SizeOf(TWorldStats));
end;

procedure InitGameStats;
begin
    ClearWorldStats;
    FillByte(@achievements, ACHIEVEMENTS_COUNT, 0);
    gameRuns := 0;
    lastSave := 256 - SAVE_PAGES;
end;

procedure RestoreLastSave;
begin
    ResolveSaveAddress(lastSave);
    Move(pointer(saveAddress), pointer(WORLD_STATS_ADDRESS), SAVE_SIZE);
end;

function FindLastSave:boolean;
var save,b:byte;
begin
    result := false;
    save := 0;
    repeat 
        ResolveSaveAddress(save);
        b := peek(saveAddress + $200 - 1);
        if b = byte('F') then begin
            lastSave := save;
            result := true;
        end;
        Inc(save, SAVE_PAGES);
    until (save = 0);
end;

procedure RestoreWorldStats;
begin
    if FindLastSave then RestoreLastSave
        else InitGameStats;
    Inc(gameRuns);
    StoreStats;
    CheckAchievements;
end;

procedure CountGlobalStats;
var worldNum:byte;
begin
    FillByte(@globalStats,SizeOf(TGlobalStats),0);
    for worldNum := 0 to 5 do begin
        GetWorldStats(worldNum);
        globalStats.attempts := globalStats.attempts + worldStats.attempts;
        globalStats.winCount := globalStats.winCount + worldStats.winCount;
        globalStats.failCount := globalStats.failCount + worldStats.failCount;
        globalStats.totalScore := globalStats.totalScore + worldStats.totalScore;
        globalStats.totalTime := globalStats.totalTime + worldStats.totalTime;
        globalStats.totalSlimes := globalStats.totalSlimes + worldStats.totalSlimes;
        globalStats.totalDeaths := globalStats.totalDeaths + worldStats.totalDeaths;
        globalStats.flawlessCount := globalStats.flawlessCount + worldStats.flawlessCount;
        globalStats.allSecretsCount := globalStats.allSecretsCount + worldStats.secretAllCount;
        globalStats.allSlimesCount := globalStats.allSlimesCount + worldStats.pickAllCount;
        
        if worldStats.flawlessCount > 0 then Inc(globalStats.flawlessWorlds);
        if worldStats.pickAllCount > 0 then Inc(globalStats.allSlimesWorlds);
        if worldStats.secretAllCount > 0 then Inc(globalStats.allSecretsWorlds);
        if worldStats.mastersCount > 0 then Inc(globalStats.mastersWorlds);
        if worldStats.winCount > 0 then Inc(globalStats.goalsCount);
        if worldStats.speedRunsCount > 0 then Inc(globalStats.speedRunsWorlds);
    end;
end;

procedure LoadAchievement(achNum: byte);
var addr:word;
begin
    addr := achNum * SizeOf(TAchievement);
    Move(pointer(ACHIEVEMENTS_DEFINITIONS_ADDRESS + addr), @achievement, SizeOf(TAchievement));
end;

function isAchivementReached:boolean;
var val:cardinal;
begin
    result := false;
    val := 0;
    case achievement.value of
        //0: val := worldStats.attempts;
        1: val := worldStats.winCount;
        //2: val := worldStats.failCount;
        //3: val := worldStats.totalScore;
        4: val := worldStats.totalSlimes;
        5: val := worldStats.flawlessCount;
        6: val := worldStats.pickAllCount;
        7: val := worldStats.secretAllCount;
        8: val := worldStats.bestScore;
        9: val := worldStats.bestTime;
        10: val := worldStats.totalTime;
        11: val := worldStats.mastersCount;
        $10: val := globalStats.goalsCount;
        $11: val := globalStats.allSecretsWorlds;
        $12: val := globalStats.allSlimesWorlds;
        $13: val := globalStats.totalSlimes;
        $14: val := globalStats.winCount;
        $15: val := globalStats.totalScore;
        $16: val := globalStats.flawlessCount;
        $17: val := globalStats.totalDeaths;
        $18: val := achivementsCount;
        //$19: val := globalStats.attempts;
        $1A: val := globalStats.totalTime;
        $1B: val := globalStats.mastersWorlds;
        $1C: val := globalStats.speedRunsWorlds;
    end;
    if achievement.oper = IS_GREATER then // greater
        result := val > achievement.treshold;
    if achievement.oper = IS_LESSER then // lesser
        result := val < achievement.treshold;
    if achievement.oper = TRESHOLD_TIME_REACHED then // lesser
        result := word(val) < world.timeTreshold;
    if achievement.oper = TRESHOLD_SCORE_REACHED then // lesser
        result := word(val) > world.scoreTreshold;
end;

procedure CheckAchievements;
var world,achNum,i:byte;
procedure CheckAchievement(a: byte);
begin
    LoadAchievement(a);
    if isAchivementReached then begin
        if achievements[achNum] = 0 then begin
            Inc(newAchivements);
            achievements[achNum] := 2;
        end;
        Inc(achivementsCount);
    end else begin
        if achievements[achNum] <> 0 then achievements[achNum] := 0;
    end;
    Inc(achNum);
end;
begin
    newAchivements := 0;
    achivementsCount := 0;
    achNum := 0;
    CountGlobalStats;
    
    for world := 0 to WORLD_COUNT - 1 do begin
        LoadWorld(world);
        GetWorldStats(world);
        for i := 0 to 11 do CheckAchievement(i);
    end;
    
    for i := 12 to 23 do CheckAchievement(i);
end;

procedure MergeStr(var str1:string;str2:string[40]);
var l1,l2:byte;
begin
    l1 := Length(str1);
    l2 := Length(str2);
    str1[0] := char(l1 + l2);
    while l2>0 do begin
        str1[l1+l2] := str2[l2];
        dec(l2);
    end;
end;


procedure FSfx(patch:byte);
var sfxCh:byte;
begin
    Inc(patch, $10);
    sfxCh:=0;
    if not music_ingame then begin
        inc(lastSfxCh);
        if lastSfxCh > 3 then lastSfxCh := 0;
        sfxCh := lastSfxCh;
    end;
    msx.Sfx(patch, sfxCh, 1);
    msx.Sfx(patch, sfxCh + 4, 1);
    sfxDelay := FORCE_SFX_DELAY;
End;


procedure Sfx(patch:byte);
begin
    if not music_ingame or (sfxDelay = 0) then begin
        FSfx(patch);
        sfxDelay := DEFAULT_SFX_DELAY;
    end;
end;

function GetPix(x, y: byte):byte;register;
var px_vram: word;
    px_mask: byte;
    px_shift: byte;
    b: byte;
begin
    if y > 79 then exit(0);
    if flipped then y := 79 - y;
    px_vram := vramLineOffset[y] + (x shr 2);
    px_shift := 6 - ((x and 3) shl 1);
    px_mask := 3 shl px_shift;
    b := peek(px_vram) and px_mask;
    result := b shr px_shift;
end;

procedure InitGame;
begin
    WaitFrame;
    dmactl := 0;
    RestoreWorldStats;
    WaitFrame;
    PMG_Init(Hi(PMG));
    PMG_gractl := 0;
    fillbyte(pointer(EMPTYLINE_ADDRESS) ,40, 0);  // clear one line before VRAM (used by flip);
    for b:=0 to 99 do vramLineOffset[b] := VIDEO_RAM_ADDRESS + 40 * b;    
    FillByte(@slimeX,20,0);
    worldCurrent := 0;
    music_on := 0;
    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);
    statsOn := false;
    ntscFrame := 0;
    WaitFrame;
end;

procedure NextLevel(dir: byte);
var nextRoom: byte;
begin
    nextRoom := ROOM_NONE;
    case dir of
        DIR_TOP: nextRoom:=level.room_top;
        DIR_RIGHT: nextRoom:=level.room_right;
        DIR_BOTTOM: nextRoom:=level.room_bottom;
        DIR_LEFT: nextRoom:=level.room_left;
    end;
    if nextRoom = ROOM_ENTRANCE then exit;
    if nextRoom = ROOM_NONE then exit;
    levelNum := nextRoom;
    room_changed := true;
end;

procedure ShowSlime;assembler;
    asm {
      jsr showScore
    };
end;

procedure SubSlime(val: byte);
begin
    if val < mySlime then begin
    asm {
      lda val
      sta scoreadd
      lda #0 
      sta scoreadd+1
      jsr subScore
    };
    end
    else begin
        mySlime := 0;
        ShowSlime;
        mainGoalReached := false;
        if not game_over then EndWorld;
    end;
end;

procedure AddSlime(val: byte):overload;
begin
    asm {
      mva #0 scoreadd+1
      mva val scoreadd
      jsr addScore
    };
end;

procedure AddSlime(val: word):overload;
begin
    asm {
      mwa val scoreadd
      jsr addScore
    };
end;

procedure ArmTrigger(ptr:word);
begin	
    Move(@leveldata[ptr],@roomTriggers[triggerCount,0], 10);
	Inc(triggerCount);
end;

procedure BlackAll;
begin
    FillByte(@levelColors,4,0);
    FillByte(@pmgColors,4,0);
end;

procedure FadeOut;
var lum: byte;
    c: byte;
begin
    for lum:=0 to 16 do begin
        for c:=0 to 3 do begin
            if levelColors[c] and $f = 0 then levelColors[c] := 0
            else dec(levelColors[c]);
            if pmgColors[c] and $f = 0 then pmgColors[c] := 0
            else dec(pmgColors[c]);
        end;
        WaitFrame;
    end;
end;

procedure FadeIn;
var lum: byte;
    c: byte;
    pmgOrigin:array [0..3] of byte;
begin
    pmgOrigin[0] := MapColor(FLOB_COL0);
    pmgOrigin[1] := MapColor(FLOB_COL1);
    banks[spriteData.bank] := 0;
    pmgOrigin[2] := peek(spriteData.colors[0] + spriteFrame[0]);
    pmgOrigin[3] := peek(spriteData.colors[1] + spriteFrame[1]);
    SetBank0;
    Fillbyte(levelColors,4,0);
    for lum:=15 downto 0 do begin
        for c:=0 to 3 do begin
            if level.colors[c] and $f = lum then levelColors[c] := level.colors[c] and $f0;
            if level.colors[c] and $f > lum then Inc(levelColors[c]);
            if pmgOrigin[c] and $f = lum then pmgColors[c] := pmgOrigin[c] and $f0;
            if pmgOrigin[c] and $f > lum then Inc(pmgColors[c]);
        end;
        WaitFrame;
    end;
    move(level.colors, @levelColors ,4);
    move(@pmgOrigin, @pmgColors ,4);
end;

procedure PutChar(c:char; dest: word; color: byte);
var row,ic,il,ih:byte;
    src:word;
    m2b:array [0..0] of byte;
begin
    m2b := @map2bpp[color,0];
    row := 0;
    src := FONT + (ord(c) shl 3);
    repeat 
        ic := peek(src);
        ih := ic shr 4;
        il := ic and 15;
        poke(dest, peek(dest) or m2b[ih]);
        Inc(dest);
        poke(dest, peek(dest) or m2b[il]);
        Inc(src);
        Inc(dest, 39);
        Inc(row);
    until row = 8;
end;

procedure PutString(var s:string; dest: word; color: byte);
var i: byte;
begin
    i := 1;
    while (i <= Length(s)) do begin
        PutChar(s[i], dest, color);
        Inc(dest, 2);
        Inc(i);
    end;
end;

procedure PutCString(var s:string;dest: word;color:byte;w:byte);
var l: byte;
begin
    l:=Length(s);
    if l < w then dest := dest + (w - l);
    PutString(s, dest, color);
end;

procedure OrderDlist;
var slptr: byte;
    dlptr: word;
begin
    dlptr := DL_ROWS_PRT;
    slptr := 0;
    repeat 
        dpoke(dlptr, vramLineOffset[slptr]);
        Inc(slptr);
        Inc(dlptr, 3);
    until slptr = 80;
end;

procedure ReverseDlist;
var slptr:byte;
    dlptr:word;
begin
    dlptr := DL_ROWS_PRT;
    slptr := 80;
    repeat 
        dec(slptr);
        dpoke(dlptr, vramLineOffset[slptr]);
        Inc(dlptr, 3);
    until slptr = 0;
end;

procedure ClearVram;
begin
    FillByte(pointer(VIDEO_RAM_ADDRESS),96*40,0);
end;
    
procedure HRLine1(y:byte);
begin
    FillByte(pointer(vramLineOffset[y]),40,%01010101);    
end;

procedure Teleport(lvl, x, y: byte);
begin
    Sfx(SFX_TELEPORT);
    FadeOut;
    DisplayOff;
    levelNum := lvl;
    flobX := x;
    flobY := y;
    flipped := false;
    OrderDlist;
    room_changed := true;
    teleported := true;
end;

procedure SpriteTeleport(pnum: byte);
begin
    Teleport(sprites.goal[pnum], sprites.frameOn[pnum], sprites.frameOff[pnum]);
end;

// ***************************************************
// *************************************************** FLOB
// ***************************************************

procedure InitFlobPMG;
begin
    PMG_Clear;
    pmgColors[0] := MapColor(FLOB_COL0);
    pmgColors[1] := MapColor(FLOB_COL1);
    PMG_sizep0 := 0;
    PMG_sizep1 := 0;
    PMG_gprior := 40; // Plfld 0-1, player 0-3, plfld 2-3, bckg    
    
    //PMG_gprior := 1;  //-------------------------------------------------- DEBUG
end;

procedure SetLanded;
begin
    lastLanded.lvl := LevelNum;
    lastLanded.x := flobX;
    lastLanded.y := flobY;
    lastLanded.flipped := flipped;
end;

procedure SetLandingSafe;
begin
    Move(@lastLanded,@lastSafeLanded,SizeOf(TLastLanded));
end;


procedure ClearFlob;
var dest:word absolute $f0;
begin
    PMG_hpos0 := 0;
    PMG_hpos1 := 0;
    dest := PMG + 512 + flobY + PMG_Y_OFFSET;
asm {
        ldy #7
        lda #0
@
        sta (dest),y
        lda dest
        eor #128
        sta dest
        lda #0
        sta (dest),y
        dey
        bpl @-

    };
end;

procedure DrawFlob;
var src0:word absolute $f0;
    src1:word absolute $f2;
    dest:word absolute $f4;
begin
    dest := PMG + 512 + flobY + PMG_Y_OFFSET;
    asm {
        lda flobX
        add #PMG_X_OFFSET
        sta b_pmg.PMG_hpos0
        sta b_pmg.PMG_hpos1
  
        lda flobFrame
        add flobDir
        asl
        tay
        mwa adr.flobFrames0,y src0
        mwa adr.flobFrames1,y src1
    
        ldy #7
@
        lda (src0),y 
        sta (dest),y
        lda dest
        eor #128
        sta dest
        lda (src1),y
        sta (dest),y
        lda dest
        eor #128
        sta dest
        dey
        bpl @-

    };
end;

procedure FlobAnimate(frame:byte);
begin
    flobFrame := frame;
    flobFrameDelay := ANIMATION_DELAY;
end;

procedure FlobFootUpdate;
begin
    if flobDir = DIR_LEFT then begin
        flobFoot := flobX + 2;
        flobFace := flobX + 1;
    end;
    if flobDir = DIR_RIGHT then begin
        flobFoot := flobX + 3;
        flobFace := flobX + 4;
    end;
end;

procedure FlobTurn(dir: byte);
begin
    flobDir := dir;
    FlobFrame := 0;
    FlobFootUpdate;
    if getPix(flobFoot, flobY ) = 1 then
        if getPix(flobFoot, flobY - 1 ) = 0 then Dec(flobY);
end;

function CanMove(dir:byte):boolean;
begin
    result := true;
    if flobY < 1 then exit(false);
    if dir = DIR_LEFT then begin
        if flobX = LEFT_EDGE then exit(false);
    end;
    if dir = DIR_RIGHT then begin
        if flobX = RIGHT_EDGE then exit(false);
    end;
    if GetPix(flobFace, flobY - 3) = 1 then exit(false);
    if GetPix(flobFoot, flobY - 2) = 1 then exit(false);
    if GetPix(flobFoot, flobY - 1) = 1 then 
        if flobY>1 then Dec(flobY)
            else exit(false);
end;

procedure FlobMove(dir:byte);
begin
    if flobDir <> dir then begin
        FlobTurn(dir);
        exit;
    end;
    if CanMove(dir) then begin
        if dir = DIR_LEFT then dec(flobX) else Inc(flobX);
        FlobFootUpdate;
        FlobFrame := FRAME_MOVE;
    end;
end;

procedure FlobFall;
var drop: byte;
begin
    if flobFalling<$ff then Inc(flobFalling);
    if flobFalling > 4 then begin
        flobFrame := FRAME_FALL;
        drop := flobfalling shr 2;
        if drop > 4 then drop := 4;
        while (drop>0) do begin
            if (getPix(flobFoot, flobY) <> 1) or (flobY>79) then Inc(flobY);
            dec(drop);
        end;
    end else Inc(flobY); 
end;

procedure PickedAllSlimes;
begin
    AddSlime($2000);
end;

procedure PickSlime;
var i: byte;
    min: byte;
    dist: byte;
    pick, dy: byte;
    fx, fy: byte;
begin
    Sfx(SFX_SLIMEPICK);
    sfxDelay:=0; // for fast release
    min := $ff;
    fx := flobX + PMG_X_OFFSET + 4;
    fy := flobY;
    if flipped then fy := 80 - fy;
    
    for i:=0 to 19 do begin
        if slimeX[i] <> 0 then begin 
            dist := 0;
            if slimeX[i] > fx then dist := slimeX[i] - fx
            else dist := fx - slimeX[i];
            
            dy := i shl 2;
            if dy > fy then dist := dist + (dy - fy)
            else dist := dist + (fy - dy);
            
            if dist < min then begin
                min := dist;
                pick := i;
            end;
        end;
    end;

    slimeX[pick] := 0;
    WaitFrame;
    PMG_hitclr := $ff; 
    slimes[slimeGoal[pick]] := 0;
    AddSlime($50);
    Inc(pickedSlimeCount);
    if pickedSlimeCount = world.totalSlimeCount then PickedAllSlimes;
end;

procedure Explode;
var stepl, stepr:byte;
    p0: array [0..0] of byte absolute (PMG + 512 + PMG_Y_OFFSET);
    p1: array [0..0] of byte absolute (PMG + 512 + 128 + PMG_Y_OFFSET);
    row, top, bottom, x0, x1, mask, b0, b1, xoff:byte;
    drop:boolean;
    
begin
    FSfx(SFX_DEAD);
    sfxDelay := 0;
    ClearFlob;
    flobFrame := FRAME_POOF1;
    DrawFlob;
    WaitFrames(5);
    ClearFlob;
    flobFrame := FRAME_POOF2;
    DrawFlob;
    WaitFrames(5);
    PMG_gprior := 33;
    stepl := 0;
    stepr := 1;
    
    flobFrame := FRAME_XPLODE;
    repeat 
        ClearFlob;
        Inc(flobY);
        DrawFlob;
        x0 := PMG_X_OFFSET + flobX - stepl shr 1;
        x1 := PMG_X_OFFSET + flobX + stepr shr 1;
        PMG_hpos0 := x0;
        PMG_hpos1 := x1;
        Inc(stepl);
        Inc(stepr);
        WaitFrame;
    until stepl > 4;

    top := flobY;
    bottom := flobY + 7;
    dec(x0, PMG_X_OFFSET + 1);
    dec(x1, PMG_X_OFFSET + 1);
    
    repeat 
        for row := bottom downto top do begin
            mask := %10000000;
            xoff := 0;
            drop := false;
            b0 := p0[row];
            b1 := p1[row];

            while mask > 0 do begin
                if (b0 and mask <> 0) then 
                    if GetPix(x0 + xoff, row + 1 - 7) <> 1 then begin
                        drop := true;
                        p0[row] := p0[row] and (not mask);
                        if row < 86 then p0[row+1] := p0[row+1] or mask;                                
                    end;

                if (b1 and mask <> 0) then 
                    if GetPix(x1 + xoff, row + 1 - 7) <> 1 then begin
                        drop := true;
                        p1[row] := p1[row] and (not mask);
                        if row < 86 then p1[row+1] := p1[row+1] or mask;                                
                    end;
                Inc(xoff);
                mask := mask shr 1;
            end;
        end;

        if drop then begin 
            Inc(bottom);
            Inc(top);
        end;

    until drop = false;
    SubSlime($70);
    SubSlime($80);
    WaitFrames(50);
    InitFlobPMG;
    exploded := true;
    Inc(deadCount);
end;

procedure KeyTaken(pnum: byte);
begin
    if sprites.moves[pnum] <> MOVE_ONPATH then begin
        spriteTargetFrame[pnum] := spriteData.frameCount[pnum] - 1;
        if (sprites.param1[pnum] <> NONE) then doorReveal[pnum] := 30;
    end else begin
        if (sprites.frameOn[pnum] <> NONE) then doorReveal[pnum] := 30;
    end;
    goals[sprites.goal[pnum]] := 0;
end;


// ***************************************************
// *************************************************** WORLD
// ***************************************************

procedure LoadWorld(w: byte);
var bank:byte;
begin
    bank := worldsBanks[w];
    banks[bank] := 0;
    Move(pointer($A000), @world, SizeOf(TWorld));
    world.bank := bank;
    world.goalcolor1 := MapColor(world.goalcolor1);
    world.goalcolor2 := MapColor(world.goalcolor2);
    world.imgBank := world.imgBank + bank;
    world.assetBank := world.assetBank + bank;
    SetBank0;
end;

procedure InitWorld;
begin
    LevelNum := world.start;
    flobX := world.flobx;
    flobY := world.floby;
    flipped := false;
    if cheatmode then mySlime := $9999
        else mySlime := world.startingSlime;
    pickedSlimeCount := 0;
    gametime := 0;
    deadCount := 0;
    secretsFound := 0;
    mainGoalReached := false;
    msgDisplayCounter := 0;
    SetLanded;
    SetLandingSafe;
    room_landed := false;
    first_landed := false;
    teleported := false;
    landedTime := 0;
    doorReveal[0] := 0;
    doorReveal[1] := 0;
    FillByte(@slimes, SLIMES_MAX + 1, 1);
    FillByte(@goals, GOALS_MAX + 1, 1);
    FillByte(@triggers, TRIGGERS_MAX + 1, 1);
    banks[worldsBanks[worldCurrent]] := 0;
    unApl(pointer(world.rmtPtr), pointer(RMT_MODULE_ADDRESS));
end;

procedure putCenteredS(y, c, cs:byte);
begin
    s1 := Atascii2Antic(s);
    PutCString(s1, vramLineOffset[y+1], cs, 20);
    PutCString(s1, vramLineOffset[y], c, 20);
end;

procedure Sec2TimeInS1(seconds:word);
var 
    min,sec: word;
begin
    min := seconds div 60;
    sec := seconds - (min * 60);
    Str(min,s1);
    Str(sec,s);
    s2 := ':';
    if Length(s) = 1 then MergeStr(s1,':0')
    else MergeStr(s1,s2);
    MergeStr(s1,s);
end;

procedure MoveColors(src,dest:pointer;count:byte);
var c:byte;
begin
    while count>0 do begin
        c := MapColor(peek(word(src)));
        poke (word(dest), c);
        Inc(src);
        Inc(dest);
        dec(count);
    end;
end;

procedure EndWorld;
var icon, sec, score: word;
    n,shift: byte;
    mul, mask: cardinal;
    mastered: boolean;
begin
    FadeOut;
    StopSong;
    InitScreen(TITLE_DLIST_ADDRESS, @dliselect);
    GetWorldStats(worldCurrent);
    worldStats.attempts := worldStats.attempts + 1;
    ClearVram;
    PMG_Clear;
    mastered := true;

    if mainGoalReached then begin

        MoveColors(winColors,titleColors,9);
        titleColors[1] := world.goalcolor1;
        titleColors[2] := world.goalcolor2;
        s:='Congratulations!';
        icon := B0_BIGVIAL.GFX;
        if worldCurrent = 5 then icon := B0_BIGFORMULA.GFX; 
        if worldCurrent = 0 then begin
            icon := B0_BIGMAP.GFX; 
            titleColors[1] := $ba;
            titleColors[2] := $26;
        end; 
        worldStats.winCount := worldStats.winCount + 1;
    
    end else begin

        MoveColors(failColors,titleColors,9);
        s:='Game Over';
        icon := B0_SKULL.GFX; //$BC00;
        worldStats.failCount := worldStats.failCount + 1;

    end;

    putCenteredS(0,1,2);

    sec := gametime div 50;
    if mainGoalReached then begin
        if sec < worldStats.bestTime then worldStats.bestTime := sec;
        if sec < world.timeTreshold then worldStats.speedRunsCount := worldStats.speedRunsCount + 1;
    end;
    worldStats.totalTime := worldStats.totalTime + sec;
        
    Sec2TimeInS1(sec);
    s := 'Game Time ';
    MergeStr(s, s1); 
    putCenteredS(60,2,0);
    
    mul := 10000000;
    score := 0;
    mask := $F0000000;
    shift := 8;
    
    repeat 
        dec(shift);
        n := (mySlime and mask) shr (shift shl 2);
        score := score + (n * mul);
        mul := mul div 10;
        mask := mask shr 4;
    until shift = 0;
    
    if score > worldStats.bestScore then worldStats.bestScore := score;
    worldStats.totalScore := worldStats.totalScore + score;
    
    Str(score,s1);
    s := 'Score ';
    MergeStr(s, s1);
    putCenteredS(71,3,2);
        
    if pickedSlimeCount > worldStats.maxSlimes then worldStats.maxSlimes := pickedSlimeCount;
    worldStats.totalSlimes := worldStats.totalSlimes + pickedSlimeCount;

    Str(world.totalSlimeCount, s);
    Str(pickedSlimeCount, s1);
    MergeStr(s1, ' of ');
    MergeStr(s1, s);
    s := 'Slimes ';
    MergeStr(s,s1);
    putCenteredS(81,3,2);

    if deadCount > worldStats.maxDeaths then worldStats.maxDeaths := deadCount;
    if mainGoalReached then 
        if deadCount < worldStats.minDeaths then worldStats.minDeaths := deadCount;
    worldStats.totalDeaths := worldStats.totalDeaths + deadCount;
    
    if secretsFound > worldStats.maxSecrets then worldStats.maxSecrets := secretsFound;
    if secretsFound = world.secretsCount then worldStats.secretAllCount := worldStats.secretAllCount + 1
        else mastered := false;
    
    if mainGoalReached then begin
        if deadCount = 0 then worldStats.flawlessCount := worldStats.flawlessCount + 1
            else mastered := false;
        if pickedSlimeCount = world.totalSlimeCount then worldStats.pickAllCount := worldStats.pickAllCount + 1
            else mastered := false;
    end else mastered := false;

    if mastered then worldStats.mastersCount := worldStats.mastersCount + 1;

    SetBank0;
    PutTile(16,12,icon,0);

    SetWorldStats(worldCurrent);
    if not cheatmode then begin
        CheckAchievements;
        StoreStats;
    end;

    SetBank0;
    unApl(pointer(B0_FLOB_THEME.APU), pointer(RMT_MODULE_ADDRESS));

    waitFrame;
    DisplayOnWithPMG;
    PlaySong(MUSIC_ENDGAME);
    nmien := $c0;

    repeat until IsStartingButtonDown;
    repeat until not IsStartingButtonDown;

    if (worldCurrent = 5) and mainGoalReached then begin 
        ShowCutScene(OUTRO_BANK, OUTRO_IMAGE_COUNT);
        ShowCredits;
    end;
    
    if newAchivements > 0 then ShowAchievements;
    
    game_over := true;
    StopSong;
    DisplayOff;


end;


// ***************************************************
// *************************************************** BOARD
// ***************************************************


procedure GetImageInfo(imgBank, imgNum:byte);
var ptr:word;
begin
    banks[imgBank]:=0;
    ptr := (imgNum shl 3) + $A000;
    move(pointer(ptr),image, sizeOf(TImage));
    SetBank0;
end;

procedure LoadImage(imgNum: byte);
begin
    GetImageInfo(world.imgBank, imgNum);
    banks[image.bank] := 0;
    ExpandLZ4(image.start, VIDEO_RAM_ADDRESS);
    SetBank0;
end;


procedure ShowAchievements;
var w,a,aid,x,y,t,j,c,cur_frame:byte;
    addr:word;
    cur_sprite:pointer;
    times:array [0..6*12] of byte absolute GOALS_ADDRESS;
    tp,key:byte;
procedure showcur;
begin
    Move(cur_sprite,pointer(PMG + 512 + y),12);
end;

procedure movecur;
begin
    fillByte(pointer(PMG + 512 + y),12,0);
    fillByte(pointer(VIDEO_RAM_ADDRESS + 40 * 93),80,0);
    t:=8;
    y:= w * 11 + 26;
    x:= a * 12 + 56;
    PMG_hpos0 := x;
    showcur;
    if w < WORLD_COUNT then begin
        LoadWorld(w);
        s1 := world.name;
        LoadAchievement(a);
    end else begin
        s1 := 'Master'~;
        LoadAchievement(a + 12);
    end;
    MergeStr(s1,' Achievement'~);
    Move(@s1[1],pointer(VIDEO_RAM_ADDRESS + 40 * 93 + (19 - (byte(s1[0]) shr 1))), byte(s1[0]));
    s1:='';
    if achievement.oper = TRESHOLD_TIME_REACHED then begin
        tp:=w*12;
        move(times[tp],s1,6);
    end;
    if achievement.oper = TRESHOLD_SCORE_REACHED then begin
        tp:=w*12+6;
        move(times[tp],s1,6);
    end;
    s1:=Atascii2Antic(s1);
    s := achievement.name;
    MergeStr(s,' : '~);
    MergeStr(s,achievement.desc);
    MergeStr(s,s1);
    Move(@s[1],pointer(VIDEO_RAM_ADDRESS + 40 * 94 + (19 - (byte(s[0]) shr 1))), byte(s[0]));
    
end;


begin
    DisplayOff;
    InitScreen(ACHIEVEMENTS_DLIST_ADDRESS, @dliachieve);
    PMG_Clear;
    ClearVram;
    
    MoveColors(achievementsColors, titleColors, 9);

    Str(achivementsCount,s1);
    Str(ACHIEVEMENTS_COUNT, s);
    MergeStr(s1,' / ');
    MergeStr(s1,s);
    s := 'Achievements ';
    MergeStr(s,s1);
    putCenteredS(0,2,1);
    
    HRLine1(10);
    HRLine1(91);
    
    y:=13;
    aid:=0;
    tp:=0;
    for w:=0 to WORLD_COUNT do begin
        x:=2;
        for a:=0 to 11 do begin
            addr := word(@ach_off);
            if achievements[aid] = 1 then 
                addr := word(@ach_on);
            if achievements[aid] = 2 then begin
                addr := word(@ach_new);
                achievements[aid] := 1;
            end;
            PutTile(x,y,addr,0);
            Inc(aid);
            Inc(x, 3);
        end;
        Inc(y, 11);
        if w<WORLD_COUNT then begin
            LoadWorld(w);
            Sec2TimeInS1(world.timeTreshold);
            move(s1,times[tp],6);
            inc(tp,6);
            Str(world.scoreTreshold, s1);
            move(s1,times[tp],6);
            inc(tp,6);
        end;
    end;

    WaitFrame;
    DisplayOnWithPMG;
    PMG_gprior := 4;
    PMG_sizep0 := 1;
    
    w:=0;
    a:=0;
    t:=0;
    y:=0;

    cur_frame:=0;
    cur_sprite := @ach_cursor[cur_frame,0];
    movecur;
    CONSOL := 0;
    repeat 
        key := NONE;
   
        if t > 0 then dec(t) 
        else begin

            if (skstat and 4 = 0) then 
                key := kbcode and %00111111;
        
            j := stick0 and %1111;
            if (j <> 15) or (key <> NONE) then begin
                if ((j and %0100) = 0) or (key = KEY_LEFT_CODE) then begin  
                    dec(a);
                    if a=$ff then a:=11;
                end;
                if ((j and %1000) = 0) or (key = KEY_RIGHT_CODE) then begin
                    Inc(a);
                    if a=12 then a:=0;
                end;
                if ((j and %0001) = 0) or (key = KEY_UP_CODE) then begin
                    dec(w);
                    if w=$ff then w:=6;
                end;
                if ((j and %0010) = 0) or (key = KEY_DOWN_CODE) then begin
                    Inc(w);
                    if w=7 then w:=0;
                end;
                movecur;
            end;
        end;
        
        c:=sysframe;
        
        if c and 1 = 0 then begin
            Inc(cur_frame);
            if cur_frame = 5 then cur_frame := 0;
            cur_sprite := @ach_cursor[cur_frame, 0];
            showcur;
        end;
        
        if w = 6 then world.goalcolor2 := (c and $f0) or 8;
        
        pmgColors[0] := MapColor(world.goalcolor2-4) + ((c shr 2) and 3);
        waitFrame;
        
        if key = $1C then exit;
            
    until IsStartingButtonDown or CRT_SelectPressed;
    repeat until not IsStartingButtonDown and not CRT_SelectPressed;
end;


procedure InitTitle;
begin
    Waitframe;
    dmactls := 0;
    InitScreen(TITLE_DLIST_ADDRESS, @dlititle);
    SetBank0;
    unApl(pointer(B0_FLOB_TITLE.APU), pointer(VIDEO_RAM_ADDRESS));
    unApl(pointer(B0_FLOB_THEME.APU), pointer(RMT_MODULE_ADDRESS));
    if achievements[12 * 5] > 0 then unApl(pointer(B0_CROWN2.APU), pointer(VIDEO_RAM_ADDRESS));
    if cheatmode then PutTile(17,0,B0_CHEAT.GFX,0);
    MoveColors(mainTitleColors, titleColors, 6);
    WaitFrame;
    DisplayOn;
end;

procedure BuildCreditsDlist(firstrow: byte);
const 
    DL_BLANK8 = %01110000; // 8 blank lines
    DL_DLI = %10000000; // Order to run DLI
    DL_LMS = %01000000; // Order to set new memory address
    DL_MODE_160x96G4 = $D;
    DL_JVB = %01000001; // Jump to begining

var dl:array [0..0] of byte absolute CREDITS_DLIST_ADDRESS;
    ptr, line, lines:byte;
    
    procedure dlpushb(b:byte);
    begin
        dl[ptr]:=b;
        Inc(ptr);
    end;

    procedure dlpushw(w:word);
    begin
        dlpushb(Lo(w));
        dlpushb(Hi(w));
    end;
    
begin
    ptr := 0;
    dlpushb(DL_BLANK8);
    dlpushb(DL_BLANK8 + DL_DLI);
    dlpushb(DL_MODE_160x96G4 + DL_LMS);
    if firstrow < 100 then begin
        dlpushw(vramLineOffset[firstrow]);
    end else begin
        firstrow := 0;
        dlpushw(REVEAL_BUFFER);
        MoveColors(@titleColors[3],@titleColors[0],3);
    end;
    
    lines := 99 - firstrow;
    for line := 1 to lines do dlpushb(DL_MODE_160x96G4);
    
    if lines < 99 then begin
        lines := 99 - lines;
        dlpushb(DL_MODE_160x96G4 + DL_LMS + DL_DLI);
        dlpushw(REVEAL_BUFFER);
        Dec(lines);
        for line := 1 to lines do dlpushb(DL_MODE_160x96G4);
       
    end;
    
    if firstrow = 0 then dlpushb(DL_BLANK8 + DL_DLI);
    dlpushb(DL_JVB);
    dlpushw(CREDITS_DLIST_ADDRESS)
end;


procedure LoadAplScreen(bank:byte; imgNum:byte; address:word);
begin
    GetImageInfo(bank, imgNum);
    banks[image.bank]:=0;
    unApl(pointer(image.start), pointer(address));
end;

procedure LoadCreditsScreen(imgNum:byte; address:word; colorOfs:byte);
begin
    LoadAplScreen(CREDITS_BANK, imgNum, address);
    MoveColors(@credColors[imgNum,0],@titleColors[colorOfs],3);
    SetBank0;
end;


procedure ShowCutScene(csbank,cscount:byte);
var slide:byte;
    txtptr:word;
    slideDelay:word;
    line:byte;
    
function LastChar(line:byte):char;
var last:word;
    txt:array [0..0] of char absolute VIDEO_RAM_ADDRESS + 3200;
begin
    last := ((line + 1) * 40) - 1;
    repeat
        result := txt[last];
        dec(last);
    until (result <> char(0)) or (last = 0);
end;
    
begin
    WaitFrame;
    DisplayOff;
    msx.Stop;
    music_on := 0;
    InitScreen(INTRO_DLIST_ADDRESS, @dliintro);

    GetImageInfo(csbank,cscount);
    txtptr := image.start;
    GetImageInfo(csbank,cscount + 1);
    
    banks[csbank]:=0;
    unApl(pointer(image.start), pointer(RMT_MODULE_ADDRESS));

    Fillbyte(levelColors,3,0);
    slide := 0;

    WaitFrame;
    msx.Init(0);
    music_on := 1;
        
    repeat

        Fillbyte(messageTypes,4,0);
        LoadAplScreen(csbank, slide, VIDEO_RAM_ADDRESS);
        move(pointer(txtptr+4), pointer(VIDEO_RAM_ADDRESS + 3200), 160);
        level.colors[0]:=MapColor(peek(txtptr)); //$92);
        level.colors[1]:=MapColor(peek(txtptr+1)); //$0d);
        level.colors[2]:=MapColor(peek(txtptr+2)); //$36);
        colbk := MapColor(peek(txtptr+3)); //$00);
        SetBank0;
        WaitFrame;
        DisplayOn;
        FadeIn;
        line := $ff;
        slideDelay := 50;
        repeat 
            if slideDelay = 0 then begin
                Inc(line);
                slideDelay := 55;
                if line<5 then begin
                    if LastChar(line) = '.'~ then slideDelay := 150;
                    if LastChar(line) = '!'~ then slideDelay := 250;
                end;
            end;
            if (line < 5) and (messageTypes[line] < 15) then Inc(messageTypes[line]);
            Dec(slideDelay);
            WaitFrame;
        until (line = 5) or IsStartingButtonDown;
        if IsStartingButtonDown then break;
        FadeOut;
        inc(slide);
        inc(txtptr,164);
        DisplayOff;

    until (slide = cscount) or IsStartingButtonDown;
    
    repeat until not IsStartingButtonDown;
    
    WaitFrame;
    
    
end;


procedure ShowScene(scene:byte);
begin
    WaitFrame;
    FadeOut;
    DisplayOff;
    
    InitScreen(INTRO_DLIST_ADDRESS, @dliintro);
		
    Fillbyte(messageTypes,4,$0a);        		
        
    level.colors[0] := MapColor(sceneCol0[scene]);
    level.colors[1] := MapColor(sceneCol1[scene]);
    level.colors[2] := MapColor(sceneCol2[scene]);
    Fillbyte(levelColors,3,0);
        
    LoadImage(sceneImg[scene]);
    banks[world.assetBank] := 0;
    Move(pointer(sceneMsg[scene]), pointer(VIDEO_RAM_ADDRESS + 3200), 160);
    SetBank0;
    WaitFrame;
    DisplayOn;
    FadeIn;
    repeat 
        Waitframe;
    until IsStartingButtonDown;
    repeat until not IsStartingButtonDown;
    WaitFrame;
    FadeOut;
    DisplayOff;
    BackToLevel;
end;


procedure ShowCredits;
var offset:byte;
    twait:byte;
    screen:byte;
begin
    WaitFrame;
    DisplayOff;
    offset:=0;
    BuildCreditsDlist(offset);
    InitScreen(CREDITS_DLIST_ADDRESS, @dlicredits);
        
    LoadCreditsScreen(0, VIDEO_RAM_ADDRESS, 0);
    LoadCreditsScreen(1, REVEAL_BUFFER, 3);
    
    screen:=1;
        
    WaitFrame;
    DisplayOn;
    
    twait:=150;
    SetBank0;

    repeat 
        WaitFrame;
        if twait>0 then begin
            dec(twait);
            if twait = 0 then Inc(offset);
        end;
        if offset > 0 then begin
           BuildCreditsDlist(offset);
           Inc(offset);
        end;
        if offset = 101 then begin
            offset:=0;
            Move(pointer(REVEAL_BUFFER), pointer(VIDEO_RAM_ADDRESS), 4000);
            WaitFrame;
            BuildCreditsDlist(offset);
            Inc(screen);
            
            if screen = 8 then screen := 0;
            
            LoadCreditsScreen(screen, REVEAL_BUFFER, 3);
            twait:=200;
        end;

    until IsStartingButtonDown or CRT_HelpPressed;
    repeat until not IsStartingButtonDown and not CRT_HelpPressed;
end;

procedure ShowStats;
var page:byte;
    update:boolean;
    off:byte;
    vram:word;
        
    procedure ShowInt(i:cardinal);
    begin
        Str(i,s1);
        s1:=Atascii2Antic(s1);
        Move(s1[1],pointer(vram),byte(s1[0]));
        inc(vram,40);
    end;    
    
    procedure ShowTime(i:cardinal);
    begin
        Sec2TimeInS1(i);
        s1:=Atascii2Antic(s1);
        Move(s1[1],pointer(vram),byte(s1[0]));
        inc(vram,40);
    end;    
  
    
begin
    page := worldCurrent;
    update := true;
    CountGlobalStats;
    
    InitScreen(STATS_DLIST_ADDRESS, @dlistats);
    
    InitFlobPMG;

    Move(larrow, pointer(PMG + 768 + 10), 30);
    Move(rarrow, pointer(PMG + 768 + 128 + 10), 30);
    
    titleColors[0]:= MapColor(FLOB_COL1);
    titleColors[1]:= MapColor($3c);
    titleColors[2]:= MapColor(FLOB_COL0);
    
    pmgColors[2] := 0;
    pmgColors[3] := 0;
    PMG_hpos[2] := 48;
    PMG_hpos[3] := 200;

    ClearVram;
    WaitFrame;
    DisplayOnWithPMG;
    
    repeat
        joy:=stick0;
        pmgColors[2] := 0;
        pmgColors[3] := 0;
        if (page > 0) then begin
            pmgColors[2] := MapColor($94);
            if ((joy and %0100) = 0) then begin
                Dec(page);
                pmgColors[2] := MapColor($96);
                update := true;
            end;
        end;
        if (page < WORLD_COUNT)  then begin
            pmgColors[3] := MapColor($94);
            if ((joy and %1000) = 0) then begin
                Inc(page);
                pmgColors[3] := MapColor($96);
                update := true;
            end;
        end;

        if update then begin

            SetBank0;
            unApl(pointer(B0_STATBACK.APU), pointer(VIDEO_RAM_ADDRESS));
            
            VRAM := VIDEO_RAM_ADDRESS + 22*40 + 22;
            
            if page<6 then begin
                statBestsBack:=STAT_ON+4;
                statGlobalBack:=STAT_OFF;
                LoadWorld(page);
                GetWorldStats(page);

                ShowInt(worldStats.attempts);
                ShowInt(worldStats.winCount);
                ShowInt(worldStats.failCount);
                ShowInt(worldStats.bestScore);
                if worldStats.bestTime <> $ffff then ShowTime(worldStats.bestTime) else Inc(vram,40);
                ShowInt(worldStats.maxSlimes);
                ShowInt(worldStats.maxSecrets);
                ShowInt(worldStats.totalDeaths);
                ShowInt(worldStats.totalSlimes);
                ShowInt(worldStats.totalScore);
                ShowTime(worldStats.totalTime);
                ShowInt(worldStats.flawlessCount);
                ShowInt(worldStats.pickAllCount);
                ShowInt(worldStats.secretAllCount);

                s1 := world.name;
            end else begin
                statBestsBack:=STAT_OFF;
                statGlobalBack:=STAT_ON+4;

                ShowInt(globalStats.attempts);
                ShowInt(globalStats.winCount);
                ShowInt(globalStats.failCount);
                Inc(vram,160);
                ShowInt(globalStats.totalDeaths);
                ShowInt(globalStats.totalSlimes);
                ShowInt(globalStats.totalScore);
                ShowTime(globalStats.totalTime);
                ShowInt(globalStats.flawlessWorlds);
                ShowInt(globalStats.allSlimesWorlds);
                ShowInt(globalStats.allSecretsWorlds);
                        
                ShowInt(globalStats.goalsCount);                        
                ShowInt(achivementsCount);
                ShowInt(lastSave);
                ShowInt(gameRuns);
                        
                s1 := 'Total Game Stats'~;  
            end;
            off := (40 - byte(s1[0])) shr 1;
            Move(s1[1],pointer(VIDEO_RAM_ADDRESS + 21*40 + off),byte(s1[0]));            
            update := false;
        
        end;

        WaitFrame;
        
    until (strig0 = 0) or CRT_StartPressed;
    LoadWorld(worldCurrent);
    repeat until strig0 = 1;
end;

procedure ShowVersion;
const VER_POS = VIDEO_RAM_ADDRESS + 40*71 + 35;
begin
    DPoke(VER_POS     ,VERSION00);
    DPoke(VER_POS+40  ,VERSION01);
    DPoke(VER_POS+80  ,VERSION02);
    DPoke(VER_POS+120 ,VERSION03);
    DPoke(VER_POS+160 ,VERSION04);
    DPoke(VER_POS+2   ,VERSION10);
    DPoke(VER_POS+42  ,VERSION11);
    DPoke(VER_POS+82  ,VERSION12);
    DPoke(VER_POS+122 ,VERSION13);
    DPoke(VER_POS+162 ,VERSION14);
end;

procedure ShowTitleScreen;
var f:byte;
begin

    isNTSC := (PALNTS = 0);
    
    achievementsBarCol := MapColor(ACHIEVEMENTS_BAR_COL);
    
    InitTitle;
    f:=150;
    ShowVersion;
    PlaySong(MUSIC_THEME);
        
    repeat 
        WaitFrame;
        if f>0 then dec(f) else titleColors[4]:=0;
        if CRT_SelectPressed then begin
            ShowAchievements;
            InitTitle;        
        end;

        if CRT_OptionPressed and statsOn then begin //----------- DEBUG
            ShowStats;
            InitTitle;        
        end;

        if CRT_HelpPressed then begin
            ShowCredits;
            InitTitle;        
        end;
        
    until IsStartingButtonDown;
    repeat until not IsStartingButtonDown;
end;

procedure ClearTxt1;
begin
    FillByte(pointer(TXT_RAM_ADDRESS),40,0);
end;


procedure ClearTxt2;
begin
    FillByte(pointer(TXT_RAM_ADDRESS+40),80,0);
end;

procedure ShowLevelName;
begin
	topBarForeground := 10;
    ClearTxt1;
    banks[world.bank]:=0;
    Move(@world.name[1],pointer(TXT_RAM_ADDRESS+1),byte(world.name[0]));
    SetBank0;
    ShowSlime;
end;

procedure InitLevelScreen;
begin
    InitScreen(DISPLAY_LIST_ADDRESS, @dli1);
    OrderDlist;
    ShowLevelName;
    ClearTxt2;
end;



function MaskOr(s,b,c:byte):byte;
var mask,omask:byte;
begin
    mask := 3;
    omask := 0;
    while mask<>0 do begin
        if (b and mask) = c then omask := omask or mask;
        mask := mask shl 2;
        c := c shl 2;
    end;
    result := (s and omask) or b;
end;

procedure PutIcon(icon: word;locked:boolean);
var dest:word;
    row:byte;
begin
    if not locked then begin
        pmgColors[0]:=MapColor(FLOB_COL0);
        pmgColors[1]:=MapColor(FLOB_COL1);
        MoveColors(pointer(icon),titleColors,3)
    end else begin
        pmgColors[0]:=4;
        pmgColors[1]:=8;
        titleColors[0]:=6;
        titleColors[1]:=10;
        titleColors[2]:=2;
    end;
    Inc(icon,3);
    row := 40;
    dest := VIDEO_RAM_ADDRESS + 8 + 13*40;
    repeat 
        move(pointer(icon),pointer(dest),24);
        Inc(icon,24);
        Inc(dest,40);
        dec(row);
    until row=0;
end;

procedure PutTile(x,y:byte;src:word;flip:byte);
var w,h,i:byte;
    vram:word;
begin
    w := peek(src);
    h := peek(src+1);
    Inc(src, 2);
    if flip = 1 then Inc(src, w * h - w);
    vram := vramLineOffset[y] + x;
    while (h > 0) do begin
        for i := 0 to w - 1 do begin
            poke(vram + i, MaskOr(peek(vram + i),peek(src + i),0));
        end;
        Inc(vram,40);
        if flip = 0 then Inc(src, w);
        if flip = 1 then dec(src, w);
        dec(h);
    end;
end;
(*
procedure Vline(x, y, h, c:byte);
var vram:word;
    mask:byte;
    shift:byte;
    clearmask:byte;
    b:byte;
begin
    vram := vramLineOffset[y] + (x shr 2);
    mask := c shl 6;
    shift := (x and 3) shl 1;
    mask := mask shr shift;
    clearmask := not(%11000000 shr shift);
    while h > 0 do begin
        b := peek(vram);
        poke(vram, (b and clearmask) or mask);
        Inc(vram, 40);
        dec(h);
    end;
end;
*)

procedure ClearSlimes;
begin
    Fillbyte(pointer(PMG + 384 + PMG_Y_OFFSET + 8),80,0);
end;

procedure DrawSlimes;
var v:word;
    step:byte;
begin
    v := PMG + 384 + PMG_Y_OFFSET + 8;
    if not flipped then v:=v-1;
    step:=0;
    repeat
        poke(v,%00001100);
        poke(v+1,%00001111);
        poke(v+2,%00000011);
        poke(v+3,%00000000);
        Inc(v,4);
        Inc(step);
    until step = 20;
end;

procedure ShowNextStat;
begin
    statCount := STAT_CHANGE_DELAY;
    case statPage of

        0: begin
                Str(world.totalSlimeCount, s);
                Str(worldStats.maxSlimes, s1);
                MergeStr(s1, ' of ');
                MergeStr(s1, s);
                s := 'SLIMES ';
                MergeStr(s,s1);
            end;


        1: begin
                if (worldStats.bestTime <> $ffff) then 
                    Sec2TimeInS1(worldStats.bestTime)
                else 
                    s1:='--:--';
                s := 'BEST TIME ';
                MergeStr(s,s1);
            end;


        2: begin
                Str(world.secretsCount, s);
                Str(worldStats.maxSecrets, s1);
                MergeStr(s1, ' of ');
                MergeStr(s1, s);
                s := 'SECRETS ';
                MergeStr(s,s1);
            end;


        3: begin 
                Str(worldStats.bestScore, s1);
                s := 'BEST SCORE ';
                MergeStr(s,s1);
            end;

    end;  
    Inc(statPage);
    if statPage = 4 then statPage := 0;
    WaitFrame;
    FillByte(pointer(VIDEO_RAM_ADDRESS + 40*80),40*9,0);
    putCenteredS(80,2,0);
end;


function ShowWorldSelector:boolean;
var update:boolean;
    flobShown:boolean;
    locked:boolean;
    key:byte;
    delay:byte;
begin
    result := true;
    flobShown:=false;
    InitScreen(TITLE_DLIST_ADDRESS, @dliselect);

    MoveColors(selectorColors, titleColors, 9);
    ClearVram;
    s:='Select World:';
    putCenteredS(0,3,1);
        
    HRLine1(10);
    HRLine1(55);

    InitFlobPMG;

    Move(larrow, pointer(PMG + 768 + 38), 30);
    Move(rarrow, pointer(PMG + 768 + 128 + 38), 30);
    
    PMG_hpos[2] := 48;
    PMG_hpos[3] := 200;
    pmgColors[2] := MapColor($24);
    pmgColors[3] := MapColor($24);
    WaitFrame;
    DisplayOnWithPMG;
    update:=true;
    
    
    //PMG_gprior := 1; //40; 
    
    repeat
        joy:=stick0;
        pmgColors[2] := 0;
        pmgColors[3] := 0;
        key := NONE;
        
        if (skstat and 4 = 0) then begin
            key := kbcode and %00111111;
        end;

        if (worldCurrent > 0) then begin
            pmgColors[2] := MapColor($24);
            if (delay=0) and (((joy and %0100) = 0) or (key = KEY_LEFT_CODE)) then begin
                Dec(worldCurrent);
                pmgColors[2] := MapColor($26);
                update := true;
            end;
        end;
        if (worldCurrent < WORLD_COUNT-1)  then begin
            pmgColors[3] := MapColor($24);
            if (delay=0) and (((joy and %1000) = 0) or (key = KEY_RIGHT_CODE)) then begin
                Inc(worldCurrent);
                pmgColors[3] := MapColor($26);
                update := true;
            end;
        end;
        if update then begin
            ClearFlob;
            LoadWorld(worldCurrent);
            GetWorldStats(worldCurrent);
            FillByte(pointer(VIDEO_RAM_ADDRESS + 40*60),40*36,0);

            if (worldCurrent = 0) then pmgColors[2] := 0;
            if (worldCurrent = WORLD_COUNT-1) then pmgColors[3] := 0;
            
            locked := false;
            if worldCurrent > 0 then locked := achievements[(worldCurrent - 1) * 12] = 0; //<----- DEBUG
            if cheatmode then locked := false; 
            
            banks[world.bank]:=0;
            PutIcon(world.iconPtr, locked);
            SetBank0;

            flobX := world.iconFlobX;
            flobY := world.iconFlobY;
            flobDir := 0;
            flobFrame := world.iconFlobFrame;
            if flobFrame <> NONE then begin
                DrawFlob;
                flobShown := true;
            end;
            
            if locked then begin
                s := 'LOCKED';
                putCenteredS(76,3,2);
            end else begin
                statPage := 0;
                ShowNextStat;
            end;

            PutCString(world.name ,VIDEO_RAM_ADDRESS + 40*61, 2,20);
            PutCString(world.name ,VIDEO_RAM_ADDRESS + 40*60, 1,20);

            update:=false;
            delay := WORLD_SELECTION_DELAY;
        end;
        
        if (not locked) and (statCount > 0) then begin
            dec(statCount);
            if statCount = 0 then ShowNextStat;
        end;
        WaitFrame;
        if delay > 0 then dec(delay);
        if CRT_Keypressed and (kbcode = $1c) then exit(false);
 
    until IsStartingButtonDown and not locked and (world.state <> WORLD_LOCKED);
    repeat until not IsStartingButtonDown;
    
end;


// ***************************************************
// *************************************************** SPRITES
// ***************************************************

procedure LoadSpritesData(bank: byte; addr: word);
var ptr:word;
    f,p:byte;
begin
    banks[bank] := 0;
    Move(pointer(addr), @spriteData, 4);
    spriteData.bank := bank;
    ptr := addr + 4;
    
    for p:=0 to 1 do begin
        if spriteData.height[p] > 0 then begin
            spriteData.colors[p] := ptr;
            Inc(ptr, spriteData.frameCount[p]);
        end;
    end;
    for p:=0 to 1 do begin
        if (spriteData.height[p] > 0) and (spriteData.frameCount[p] > 0) then 
            for f:=0 to spriteData.frameCount[p] - 1 do begin
                spriteData.frames[p,f] := ptr;
                Inc(ptr, spriteData.height[p]);
            end;
        spriteStep[p] := 0;
    end;
    SetBank0;
end;

procedure ClearSprite(num:byte);
var ptr:word;
    i,h:byte;
begin
    i := 2 + num;
    PMG_hpos[i] := 0;
    h := spriteData.height[num];
    ptr := spritePtr[num];
    if ptr <> 0 then FillByte(pointer(ptr), h, 0);
    spritePtr[num] := 0;
end;

procedure ClearSprites;
begin
    if sprites.state = SPRITE_NONE then exit;
    if sprites.state > SPRITE_NONE then ClearSprite(0);
    if sprites.state > SPRITE_SINGLE then ClearSprite(1);
end;

procedure ShowSprite(num:byte);
var src, dest: word;
    i, x, y, h, w: byte;
    upside:boolean;   
begin
    spritePtr[num] := 0;
    if (sprites.stype[num] = STYPE_EXIT) and not mainGoalReached then exit;
    
    if ((sprites.stype[num] = STYPE_DECOR) or (sprites.stype[num] = STYPE_GOAL)) and (sprites.goal[num] <> NONE) and (goals[sprites.goal[num]] = 0) then exit;
    if (sprites.stype[num] = STYPE_DIAL) then 
        if (sprites.param2[num] <> $ff) and (goals[sprites.param2[num]] > 0) then exit;

    banks[spriteData.bank] := 0;
    i := 2 + num;
    h := spriteData.height[num];
    x := PMG_X_OFFSET + sprites.x[num];
    y := sprites.y[num];
    w := sprites.PMGwidth[num];
    src := spriteData.frames[num, spriteFrame[num]];
    upside := false;

    if flipped and (sprites.flipable[num] = 1) then begin
        y := 80 - (h + y);
        upside := true;
        Inc(src,h-1);
    end;
    
    dest := PMG + 768 + y + PMG_Y_OFFSET + 7;
        
    if num = 1 then begin
        Inc(dest, 128);
        if sprites.state = SPRITE_COMBINED then Inc(x, sprites.gap);
    end;

    spritePtr[num] := dest;
    while h > 0 do begin
        dec(h);                        /// <<<<<<<<<<------------ should optimize it (asm?)
        poke(dest, peek(src));
        Inc(dest);
        if upside then dec(src)
        else Inc(src)
    end;
    
    PMG_hpos[i] := x;
    pmgColors[i] := MapColor(peek(spriteData.colors[num] + spriteFrame[num]));
    PMG_sizep[i] := w;
    SetBank0;
        
end;

procedure ShowSprites;
begin
   if sprites.state = SPRITE_NONE then exit;
   if sprites.state > SPRITE_NONE then ShowSprite(0);
   if sprites.state > SPRITE_SINGLE then ShowSprite(1);
end;

procedure TurnDial(pnum:byte);
var g, dg:byte;
begin
    if actionDelay > 0 then exit;
    dg := 1;
    g := goals[sprites.goal[pnum]] + 1;
    if g > sprites.frameOff[pnum] then g := sprites.frameOn[pnum];
    if g = sprites.frameSpeed[pnum] then dg := 0;
    spriteTargetFrame[pnum] := g;
    goals[sprites.goal[pnum]] := g;
    goals[sprites.param1[pnum]] := dg;
    Sfx(SFX_BEEP);
    actionDelay := DIAL_DELAY;
end;

procedure AnimateSprite(pnum: byte);
var stepPtr: word;
    anim: ^TAnimationStep;
    sx,sy,sf: byte;
var changed: boolean;
begin
    if (sysframe and sprites.frameSpeed[pnum]) = 0 then begin

        changed := false;
        anim := spriteAnimations[pnum];
        sf := spriteFrame[pnum];
        sx := sprites.x[pnum];
        sy := sprites.y[pnum];

        if sprites.moves[pnum] = MOVE_ANIMATE then begin
            Inc(sf);
            if sf >= spritedata.frameCount[pnum] then sf := 0;
            changed := true;
        end;
        
        if sprites.moves[pnum] = MOVE_ONPATH then begin
        
            if ((spriteStep[pnum]) = 0) or (anim.times = 0) then begin
                banks[world.assetBank] := 0;
                stepPtr := sprites.param2[pnum] shl 8 + sprites.param1[pnum];
                stepPtr := stepPtr + word(spriteStep[pnum] shl 2);
                move(pointer(stepPtr), spriteAnimations[pnum], 4);
                if anim.times = 0 then spriteStep[pnum] := 0
                    else Inc(spriteStep[pnum]);
                SetBank0;
            end;
            
            if anim.times > 0 then begin
				if anim.times = $ff then begin
					Sfx(anim.deltax);
					anim.times := 0;
				end else begin
					dec(anim.times);
					sx := sx + anim.deltax;
					sy := sy + anim.deltay;
					if (anim.deltaFrame and $f0) = $70 then sf := anim.deltaFrame and $0f
						else sf := sf + anim.deltaFrame;
					if sf = spriteData.frameCount[pnum] then sf := 0;
					if sf = $ff then sf := spriteData.frameCount[pnum] - 1;
					changed := true;
				end;
            end;
                
        end;

        if sprites.moves[pnum] = MOVE_NONE then begin
            if spriteTargetFrame[pnum] < sf then begin
                Dec(sf);
                changed := true;
            end;
            
            if spriteTargetFrame[pnum] > sf then begin
                Inc(sf);
                changed := true;
            end;
        end;

        if changed then begin
            ClearSprite(pnum);
            sprites.x[pnum] := sx;
            sprites.y[pnum] := sy;
            spriteFrame[pnum] := sf;
            if (pnum = 0) and (sprites.state = SPRITE_COMBINED) then begin
                ClearSprite(1);
                spriteFrame[1] := spriteFrame[0];
                sprites.x[1] := sprites.x[0];
                sprites.y[1] := sprites.y[0];
                ShowSprite(1);
            end;
            ShowSprite(pnum);
        end;

    end;
end;



// ***************************************************
// *************************************************** LEVEL
// ***************************************************

function GoalsUpTo(topgoal:byte):boolean;
var goal:byte;
begin
    result := true;
    for goal:=0 to topgoal do if goals[goal] > 0 then result:=false; 
end;

function CheckCondition(condition, param: byte):boolean;
begin
    result := false;
    case condition of
        ALWAYS: result:=true;
        ON_GOAL: result := goals[param] = 0;
        ON_NO_GOAL: result := goals[param] > 0;
        ON_GOALS_UP_TO: result := GoalsUpTo(param);
        ON_NO_GOALS_UP_TO: result := not GoalsUpTo(param);
        ON_ACHIEVEMENT: result := achievements[param] > 0;
        ON_NO_ACHIEVEMENT: result := achievements[param] = 0;
    end;
end;

procedure DrawLevel;
var lvlptr: byte;
    id, p, goal, goalv: byte;
    cmd, x, y: byte;
    addr: word;
begin
    fillbyte(slimeX, 20, 0);
    fillbyte(slimeGoal, 20, 0);
    lvlptr := 0;
    triggerCount := 0;
    msgCount := 0;
    sceneCount := 0;
    colorCollision := true;
    repeat

        banks[world.bank] := 0;
        cmd := leveldata[lvlptr];
        case cmd of
            
            IMAGE_LZ4: begin
                LoadImage(leveldata[lvlptr + 1]);
                Inc(lvlptr, 2);
            end;
            
            COLOR_COLLISION_OFF: begin
                colorCollision := false;
                Inc(lvlptr);
            end;
            
            PUT_SLIME: begin 
                goal := slimes[leveldata[lvlptr + 3]];
                if goal > 0 then begin
                    SlimeX[leveldata[lvlptr + 2]] := leveldata[lvlptr + 1];
                    SlimeGoal[leveldata[lvlptr + 2]] := leveldata[lvlptr + 3];
                end;
                Inc(lvlptr, 4);
            end;

            PUT_SPRITES: begin
                LoadSpritesData(world.assetBank, leveldata[lvlptr+1] + (leveldata[lvlptr + 2] shl 8));
                banks[world.bank] := 0;
                Move(leveldata[lvlptr + 3], @sprites, sizeOf(TSprites));
                
                for p:=0 to 1 do begin
                    spriteFrame[p] := sprites.frameOn[p];
                    if sprites.stype[p] = STYPE_PORTAL then spriteFrame[p] := 0;
                    if sprites.stype[p] = STYPE_KEY then begin
                        if sprites.moves[p] = MOVE_ONPATH then spriteFrame[p] := 0;
                        if goals[sprites.goal[p]] = 0 then spriteFrame[p]:=sprites.FrameOff[p];
                        
                    end;
                    if sprites.stype[p] = STYPE_DIAL then begin
                        spriteFrame[p] := goals[sprites.goal[p]];
                    end;
                    
                    spriteTargetFrame[p] := spriteFrame[p];
                end;

                if (sprites.stype[0] = STYPE_SLIME) then begin
                    if goals[sprites.goal[0]] = 0 then sprites.state := SPRITE_NONE;
                end;
                ShowSprites;
                Inc(lvlptr, sizeOf(TSprites) + 3);
            end;
            
            PUT_TILE: begin 
                goalv := leveldata[lvlptr + 1]; // condition
                goal := leveldata[lvlptr + 2]; // param
                if CheckCondition(goalv, goal) then begin
                    x := leveldata[lvlptr + 3];
                    y := leveldata[lvlptr + 4];
                    addr := leveldata[lvlptr + 5] + (leveldata[lvlptr + 6] shl 8);
                    banks[world.assetBank] := 0;
                    PutTile(x, y, addr, 0);
                end;
                Inc(lvlptr, 7);
            end;
            
            SET_TRIGGER: begin	
				id := leveldata[lvlptr + 1];
                if triggers[id] > 0 then ArmTrigger(lvlptr + 1);	
                Inc(lvlptr, 11);
			end;
            
            SET_MESSAGE: begin
				messageTypes[msgCount] := leveldata[lvlptr + 1];
				messages[msgCount] := leveldata[lvlptr + 2] + (leveldata[lvlptr + 3] shl 8);
				Inc(msgCount);
				Inc(lvlptr, 4);
            end;
            
            SET_SCENE: begin
				SceneImg[sceneCount] := leveldata[lvlptr + 1];
				SceneMsg[sceneCount] := leveldata[lvlptr + 2] + (leveldata[lvlptr + 3] shl 8);
				SceneCol0[sceneCount] := leveldata[lvlptr + 4];
				SceneCol1[sceneCount] := leveldata[lvlptr + 5];
				SceneCol2[sceneCount] := leveldata[lvlptr + 6];
				Inc(sceneCount);
				Inc(lvlptr, 7);
            end;
            
            
            SET_COLORS: begin
                goalv := leveldata[lvlptr + 1]; // condition
                goal := leveldata[lvlptr + 2]; // param
                if CheckCondition(goalv, goal) then begin
                    level.colors[0] := MapColor(leveldata[lvlptr + 3]);
                    level.colors[1] := MapColor(leveldata[lvlptr + 4]);
                    level.colors[2] := MapColor(leveldata[lvlptr + 5]);
                    level.colors[3] := MapColor(leveldata[lvlptr + 6]);
                end;
                Inc(lvlptr, 7);
            end;
            
            LEVEL_END: ;
            //else exit;
        end;
    until cmd = LEVEL_END;
    SetBank0;
end;

procedure SetLevelColors;
begin
    banks[world.bank] := 0;
    move(level.colors, @levelColors ,4);
    pmgColors[0] := MapColor(FLOB_COL0);
    pmgColors[1] := MapColor(FLOB_COL1);
    banks[spriteData.bank] := 0;
    pmgColors[2] := MapColor(peek(spriteData.colors[0] + spriteFrame[0]));
    pmgColors[3] := MapColor(peek(spriteData.colors[1] + spriteFrame[1]));    
    SetBank0;
end;


procedure AdjustFlobPosition;
var pix,sx,sy,checkDist:byte;
begin
    sx := flobX;
    sy := flobY;
    checkDist := 0;
    repeat 
        pix := GetPix(flobFoot,flobY-1);
        if pix <> 1 then exit;
        if flobY > 3 then Dec(flobY);
        Inc(checkDist);
        FlobFootUpdate;
    until checkDist > 10;

    FlobY := sy;
    checkDist := 1;
    
    repeat 
        FlobX := sx - checkDist;
        FlobFootUpdate;
        pix := GetPix(flobFoot,flobY-1);
        if (pix <> 1) and (FlobX>LEFT_EDGE) then exit;
        FlobX := sx + checkDist;
        FlobFootUpdate;
        pix := GetPix(flobFoot,flobY-1);
        if (pix <> 1) and (FlobX<RIGHT_EDGE) then exit;
        Inc(checkDist);
    until checkDist = 10;

    FlobX := sx;
    FlobFootUpdate;
end;


procedure LoadLevel(lvl:byte);
var col:byte;    
begin
    WaitFrame;
    banks[world.bank] := 0;
    move(pointer(world.lvlPtr[lvl]), level, sizeOf(TLevel));
    for col:=0 to 3 do
        level.colors[col] := MapColor(level.colors[col]);
    move(level.colors, @colpf0, 4);
    colbk := level.colors[3];
    leveldata := pointer(world.lvlPtr[lvl] + sizeOf(TLevel));
    sprites.state := SPRITE_NONE;
    if room_changed then  begin
        doorReveal[0] := 0;
        doorReveal[1] := 0;
        if flobX = RIGHT_EDGE then flobX := LEFT_EDGE + START_EDGE_OFFSET
            else if flobX = LEFT_EDGE then flobX := RIGHT_EDGE - START_EDGE_OFFSET;
        if flobY >= BOTTOM_EDGE then flobY := TOP_EDGE;
    end;
    FlobFootUpdate;
    game_over := false;
    exploded := false;
    actionDelay := 0;
    spritePtr[0]:=0;
    spritePtr[1]:=0;
    DrawLevel;

    if room_changed then AdjustFlobPosition;
    
    room_changed := false;
    room_landed := false;
    first_landed := false;
    landedTime := 0;
end;


procedure Flip;
var row, slptr, shptr, step, soffset, sstep :byte;
    dlptr, dhptr:word;
    upside: boolean;
    stepPauses: array [0..5] of byte = (1, 1, 1, 1, 2, 3);
begin
    Sfx(SFX_FLIP);
    step := 0;
    upside := false;
    WaitFrame;
    ClearFlob;
    ClearSlimes;
    repeat 
        row := 0;
        dlptr := DL_ROWS_PRT;
        dhptr := DL_ROWS_PRT + 79*3;
        slptr := 0;
        shptr := 79;
        sstep := 1 shl step;
        soffset := 40 - (40 shr step);
        repeat 
            if row >= soffset then begin
                if flipped then begin
                    dpoke(dlptr, vramLineOffset[shptr]);
                    dpoke(dhptr, vramLineOffset[slptr]);
                end else begin
                    dpoke(dlptr, vramLineOffset[slptr]);
                    dpoke(dhptr, vramLineOffset[shptr]);
                end;
                Inc(slptr,sstep);
                dec(shptr,sstep);
            end else begin
                dpoke(dlptr, EMPTYLINE_ADDRESS);
                dpoke(dhptr, EMPTYLINE_ADDRESS);
            end;
            Inc(dlptr, 3);
            dec(dhptr, 3);
            Inc(row);
        until row = 40;

        if not upside then Inc(step);
        if step = 6 then begin
            flipped := not flipped;
            upside := true;
        end;
        if upside then dec(step);
        ClearSprites;
        if step<6 then WaitFrames(stepPauses[step]);
        
    until step = $ff;
    
    ShowSprites;
    DrawSlimes;
    flobY := 84 - flobY;
    DrawFlob;
    if flobFalling > 0 then flobFalling := 1;
    SubSlime(5);
    WaitFrame;
end;

procedure BackToLevel;
begin
    LoadLevel(levelNum);
    ShowSprites;
    DrawSlimes;
    DrawFlob;
    BlackAll;
    WaitFrame;
    dlvec := pointer(DISPLAY_LIST_ADDRESS);
    dlivec := @dli1;
    ShowLevelName;
    if flipped then ReverseDlist
    else OrderDlist;
    ClearTxt2;
    DisplayOnWithPMG;
    PMG_hitclr := $ff; 
    FadeIn;    
end;

procedure Reveal(rgoal,room:byte);
var times:byte;
    wasflipped: boolean;
begin
    FadeOut;
    WaitFrame;
    DisplayOff;
    PMG_Clear;
    wasflipped := flipped;
    flipped := false;
    OrderDlist;
    goals[rgoal] := 0;
    LoadLevel(room);
    move(pointer(VIDEO_RAM_ADDRESS),pointer(REVEAL_BUFFER),3200);
    move(level.colors[0], revealColors[0], 4);
    goals[rgoal] := 1;
    LoadLevel(room);
    goals[rgoal] := 0;
    ShowSprites;
    BlackAll;
    topBarForeground:=0;
    WaitFrame;
    dlvec := pointer(DISPLAY_LIST_ADDRESS);
    DisplayOnWithPMG;
    FSfx(SFX_REVEAL);
    sfxDelay:=0;
    FadeIn;
    times := 3;
    repeat
        dlvec := pointer(DISPLAY_LIST_ADDRESS);
        move(level.colors[0], levelColors[0], 4);
        WaitFrames(20);
        dlvec := pointer(REVEAL_DLIST_ADDRESS);
        move(revealColors[0], levelColors[0], 4);
        WaitFrames(20);
        dec(times);
    until times = 0;
    WaitFrames(50);
    FadeOut;
    WaitFrame;
    DisplayOff;
    PMG_Clear;
    flipped := wasflipped;
    BackToLevel;
end;

procedure RevealCountdown(pnum: byte);
begin
    if (sprites.stype[pnum] <> STYPE_KEY) then exit;
    if (sprites.moves[pnum] <> MOVE_ONPATH) then begin 
        if (spriteTargetFrame[pnum] = spriteFrame[pnum]) then begin
            dec(doorReveal[pnum]);
            if doorReveal[pnum] = 0 then Reveal(sprites.goal[pnum],sprites.param1[pnum]);
        end 
    end else begin
        dec(doorReveal[pnum]);
        if doorReveal[pnum] = 0 then Reveal(sprites.goal[pnum],sprites.frameOn[pnum]);
    end;
end;

procedure ShowMessage(msg, mtype, count: byte);
begin
	msgDisplayCounter := count;
	msgDisplayType := mtype;
	ClearTxt2;
	banks[world.assetBank] := 0;
    move(pointer(messages[msg]), pointer(TXT_RAM_ADDRESS + 40), 80);
    SetBank0;
end;

procedure LaunchTrigger(id, action, target, actparam, param: byte);
begin
    case action of
        SHOW_MSG: begin 
            ShowMessage(target, messageTypes[target], actparam);    
        end;
        REACH_GOAL: begin
            if goals[target]>0 then goals[target]:=0; 
            if target > SECRETS_OFFSET then Inc(secretsFound);
        end;
        CLEAR_GOAL: begin
            if goals[target] = 0 then goals[target] := 1; 
        end;
        TELEPORT_ME: begin
            Teleport(param, target, actparam);
        end;
        SHOW_SCENE: begin 
            ShowScene(target);    
        end;
	end;
end;


function ConditionTrigger(id, condition, param: byte):boolean;
begin
    if condition = ONCE then begin
        result := triggers[id] > 0;
        triggers[id] := 0;
    end else result := CheckCondition(condition, param);
end;

procedure CheckTrigger(t: byte);
var fy:byte;
//SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target, actparam
begin
	fy := flobY;
	if flipped then fy := 82 - fy;
	if flobX < roomTriggers[t, 1] then exit;
	if fy < roomTriggers[t, 2] then exit;
	if flobX > roomTriggers[t, 3] then exit;
	if fy > roomTriggers[t, 4] then exit;
    if not ConditionTrigger(roomTriggers[t, 0], roomTriggers[t, 5], roomTriggers[t, 6]) then exit;
	LaunchTrigger(roomTriggers[t, 0], roomTriggers[t, 7], roomTriggers[t, 8], roomTriggers[t, 9], roomTriggers[t, 6]);
end;


procedure CheckEvents;
var t: byte;
begin
    // key animation ended -> reveal doors
    if doorReveal[0] > 0 then RevealCountdown(0);
    if doorReveal[1] > 0 then RevealCountdown(1);
    
    t := triggerCount;
    while (t > 0) do begin
		Dec(t);
		if (triggers[roomTriggers[t,0]] > 0) then CheckTrigger(t);
	end;
    
    if msgDisplayCounter > 0 then begin
		Dec(msgDisplayCounter);
		topBarForeground := sysframe shr 1;
		if (msgDisplayType = MSG_INFO)then 
			topBarForeground := ((topBarForeground shr 1) and 7) + 8;
		if msgDisplayCounter = 0 then ClearTxt2;
	end;

    // slime consumption
    if gametime and %1111111 = 0 then SubSlime(1);

end;

{$IFDEF leveleditor}

procedure RunSlimeEditor;
var row:byte;
    delay:byte;
    lvl:byte;
    b:byte;
    vp:word;
    hue,lum,c:byte;
    ctrl,shift:boolean;
begin
    row:=0;
    PMG_gprior := 33;
    
    repeat 
    
        repeat 
            joy := stick0;  
            if CRT_SelectPressed then begin
                lvl:=NONE;
                if (joy and %0100) = 0 then lvl := LevelNum - 1;
                if (joy and %1000) = 0 then lvl := levelNum + 1;
            
                if lvl <> NONE then begin
                    if lvl = world.levelNum then lvl := 0;
                    if lvl = $ff then lvl := world.levelNum - 1;
                    levelNum := lvl;
                    PMG_Clear;
                    LoadLevel(levelNum);
                    InitLevelScreen;
                    SetLevelColors;
                    PMG_gractl := PMG_gractl_default;                
                    DrawSlimes;
                    WaitFrame;
                    DisplayOnWithPMG;
                end;
            end;

            if CRT_Keypressed then begin
                c := kbcode and %00111111;
                shift := (kbcode and 64) = 64;
                ctrl := (kbcode and 128) = 128;
                case c of
                    31: begin 
                            c:=0; 
                        end;
                    30: begin 
                            c:=1;
                        end;
                    26: begin 
                            c:=2;
                        end;
                    24: begin 
                            c:=3;
                        end;
                end;
                hue := levelColors[c] shr 4;    
                lum := levelColors[c] and $0f;
                if shift then hue := (hue + 1) and $0f
                else begin
                    if ctrl then lum := (lum - 2) and $0f
                        else lum := (lum + 2) and $0f;
                end;
                levelColors[c] := (hue shl 4) or lum;
                WaitFrames(20);
            end;
        
            if delay = 0 then begin
                if (joy and %0100) = 0 then dec(slimeX[row]);
                if (joy and %1000) = 0 then Inc(slimeX[row]);        
                if (joy and %0001) = 0 then begin
                    dec(row);
                    if row = $ff then row := 19;
                    delay:=20;
                end;
                if (joy and %0010) = 0 then begin
                    Inc(row);        
                    if row = 20 then row := 0;
                    delay:=20;
                end;
            end else dec(delay);
            if CRT_StartPressed then slimeX[row]:=0;
            
            WaitFrame;
        
        until strig0 = 0;
        repeat WaitFrame until strig0 = 1;

        vp := $600;
        fillbyte(pointer(vp),60,$ff);
        for b:=0 to 19 do begin
            if slimeX[b]>44 then begin
                poke(vp,slimeX[b]);
                Inc(vp);
                poke(vp,b);
                Inc(vp);
                poke(vp,0);
                Inc(vp);
            end;
        end;  
        move(@levelColors, pointer($700), 4);
    
    //move(slimeX,pointer($600),20);
    until false;
end;


{$ENDIF}


procedure Collided(pnum:byte);
var goal: byte;
	stype: byte;
begin
    if sprites.state = SPRITE_COMBINED then pnum:=0;
    
	stype := sprites.stype[pnum];
	goal := goals[sprites.goal[pnum]];
	
	case stype of

		STYPE_ENEMY: begin
			Explode;
			exit;
		end;
		
        STYPE_DIAL: begin
            TurnDial(pnum);
            exit;
        end;
        
        STYPE_PORTAL: begin
            SpriteTeleport(pnum);
            exit;
        end;
        
        STYPE_STATS: begin
            if actionDelay = 0 then begin
                FadeOut;
                ShowStats;
                DisplayOff;
                PMG_Clear;
                InitLevelScreen;
                LoadLevel(levelNum);
                SetLevelColors;
                DrawSlimes;
                ShowSlime;
                ShowSprites;
                if flipped then ReverseDlist;
                BlackAll;
                WaitFrame;
                DisplayOnWithPMG;                
                FadeIn;
                statsOn := true;
            end;
            actionDelay := 150;
        end;
        
		STYPE_KEY: begin
			if goal > 0 then begin
				if (sprites.param2[pnum] <> NONE) and (sprites.moves[pnum] <> MOVE_ONPATH) then begin
                    FSfx(sprites.param2[pnum]);
                end;
				KeyTaken(pnum);
			end;
		end;
		
		STYPE_SLIME: begin
			if goal > 0 then begin
				FSfx(SFX_BIGSLIME);
				AddSlime(word(word(sprites.param2[pnum] shl 8) + word(sprites.param1[pnum])));
				ClearSprites;
				sprites.state := SPRITE_NONE;
			end;
		end;

		STYPE_GOAL: begin
			if goal > 0 then begin
				FSfx(SFX_PICKGOAL);
				mainGoalReached := true;
				if sprites.state = SPRITE_COMBINED then ClearSprites
                else ClearSprite(pnum);
			end;
		end;

		STYPE_EXIT: begin
			if mainGoalReached then begin
				EndWorld;
			end;
		end;
	
	end;

	if goal > 0 then begin
		Dec(goal);
		goals[sprites.goal[pnum]] := goal;
	end;

end;


procedure CheckDiags;
begin
    if CONSOL = 0 then begin
        WaitFrame;
        DisplayOff;
        colbk:=$f;
        actionDelay:=0;
        repeat
            WaitFrame;
            inc(actionDelay);
        until CRT_Keypressed or (actionDelay>150);
        if CRT_HelpPressed then begin
            colbk:=$24;  
            EraseSector(SAVE_SECTOR);
            colbk:=$b6;  
            WaitFrames(10);
        end;
    end;
    kbcode := $ff;
    Waitframe;
    if CRT_HelpPressed then begin
        cheatmode := true;
        colbk := MapColor($36);
        repeat until not CRT_HelpPressed;
    end else 
        cheatmode := false;
        
end;

//**************************************************
//**************************************************   MAIN
//**************************************************


 
   
begin
    SystemOff($fe);
    CheckDiags;
    InitGame;
    
{$IFDEF leveleditor}

    InitFlobPMG;
    LoadWorld(5);    
    InitWorld;
    levelNum := 0;
    LoadLevel(levelNum);
    InitLevelScreen;
    SetLevelColors;
    DrawSlimes;
    WaitFrame;
    DisplayOnWithPMG;
    RunSlimeEditor;
    CheckAchievements;

{$ELSE}    
    
    worldCurrent := 0;
    
    repeat
        
        ShowTitleScreen;
        
        if ShowWorldSelector then begin
        //if true then begin worldCurrent := 1; LoadWorld(worldCurrent);   //--------- DEBUG

            StopSong;

            if (worldCurrent = 0) then ShowCutScene(INTRO_BANK, INTRO_IMAGE_COUNT);
            
            StopSong;
            
            InitWorld;
            music_track := worldCurrent;
            if music_track > 4 then music_track := 0;
            if music_ingame then PlaySong(MUSIC_LEVEL)
                else PlaySong(MUSIC_SILENT);
            InitLevelScreen;
            InitFlobPMG;

            repeat
                
                PMG_Clear;
                if exploded then begin
                    if lastSafeLanded.flipped then ReverseDlist else OrderDlist;
                    flipped := lastSafeLanded.flipped;
                    flobX := lastSafeLanded.x;
                    flobY := lastSafeLanded.y;
                    levelNum := lastSafeLanded.lvl;
                    flobFalling := 0;
                    if flobX < 80 then flobDir := DIR_RIGHT else flobDir := DIR_LEFT;    
                    invulnerability := INVULNERABILTY_TIME;
                end;
            
                //mainGoalReached := true;          // <-------------------------------- DEBUG
                LoadLevel(levelNum);
                DrawSlimes;
                ShowSlime;
                WaitFrame;
                
                if teleported then begin
                    DisplayOnWithPMG;
                    FadeIn;
                    teleported := false;
                end else begin
                    SetLevelColors;
                    DisplayOnWithPMG;
                end;
                
                repeat 

                    // clear collision register
                    PMG_hitclr := $ff; 
                    CONSOL := 0;
                    
                    // and hide flob, counting new position
                    WaitFrame;

                    if isNTSC then begin
                        Inc(ntscFrame);
                        if ntscFrame=6 then begin
                            ntscFrame := 0;
                            WaitFrame;
                        end;
                    end;    
                        
                    ClearFlob;
                    
                    // set default frame 0 if possible
                    if flobFrame <> 0 then 
                        if flobFrameDelay > 0 then Dec(flobFrameDelay)
                            else if flobFrameDelay = 0 then flobFrame := FRAME_STAND;
                    
                    FlobFootUpdate; 

                    // read joy and move
                    joy := stick0;
                    if (joy and %0100) = 0 then FlobMove(DIR_LEFT);
                    if (joy and %1000) = 0 then FlobMove(DIR_RIGHT);

                    // flip on fire pressed
                    if strig0 = 0 then Flip;
                    
                    if (skstat and 4 = 0) then begin
                        joy := kbcode and %00111111;
                        if joy = KEY_LEFT_CODE then FlobMove(DIR_LEFT);
                        if joy = KEY_RIGHT_CODE then FlobMove(DIR_RIGHT);
                    end;
                    if (skstat and 8 = 0) then Flip;

                    // check if flob should fall
                    if (GetPix(flobFoot, flobY) <> 1) or (flobY > 80) then FlobFall
                        else begin
                            if flobFalling > 16 then FlobAnimate(FRAME_SPLAT); // if landed show splat frame
                            if not room_landed then begin
                                first_landed := true;
                                landedTime := 0;
                                room_landed := true;
                            end;
                            flobFalling := 0;
                        end;
                        

                    // blink sometimes
                    if (sysframe and 31 = 0) and (flobFrame = 0) and (random(10) < 3) then FlobAnimate(FRAME_BLINK);

                    // finally we can
                    DrawFlob;
                    
                    if sprites.state > SPRITE_NONE then AnimateSprite(0);
                    if sprites.state = SPRITE_TWO then AnimateSprite(1);
                    
                    Inc(gametime);
                    
                    if actionDelay > 0 then Dec(actionDelay);
                    if sfxDelay > 0 then Dec(sfxDelay);
                    CheckEvents;
                    
                    if not game_over and not room_changed then begin
                        
                        // check ending conditions
                        if flobX = RIGHT_EDGE then NextLevel(DIR_RIGHT);     // right edge reached
                        if flobX = LEFT_EDGE then NextLevel(DIR_LEFT);     // right edge reached
                        if flobY > 80 then 
                            if flipped then NextLevel(DIR_TOP)
                                else NextLevel(DIR_BOTTOM);

                        if (PMG_sizep0 or PMG_sizep1) and 3 <> 0 then PickSlime;  // collision with missile (slime)


                        if invulnerability > 0 then begin
                            dec(invulnerability);
                            pmgColors[0] := MapColor(FLOB_COL0) - (invulnerability and 3);
                            pmgColors[1] := MapColor(FLOB_COL1) - (invulnerability and 3);
                        end else begin
                            if colorCollision and ((PMG_hposm1 or PMG_hposm0) and 2 <> 0) then Explode    // collision with color 2 -> explode!
                            else if (PMG_p0pl or PMG_p1pl) and %0100 <> 0 then Collided(0)   // collision with sprite 2
                                else if (PMG_p0pl or PMG_p1pl) and %1000 <> 0 then Collided(1);  // collision with sprite 3
                        end;
                        
                        if first_landed then begin
                            if landedTime = 0 then begin
                                SetLanded;
                                room_landed := true;
                            end;
                            Inc(landedTime);
                            if (landedTime>7) and (invulnerability = 0) then begin
                                SetLandingSafe;
                                first_landed := false;
                                landedTime := 0;
                            end;
                        end;
                        
                        if CRT_Keypressed and (kbcode = $1c) then begin // ESC pressed
                           
                           Inc(escPressed);
                           if escPressed = ESC_PRESS_TIME then begin
                                
                                if (flobX = lastSafeLanded.x) and (flobY = lastSafeLanded.y) then begin
                                    mainGoalReached := false;
                                    game_over := true;
                                end else begin
                                    Explode;
                                end;
                                
                                
                                escPressed := 0;
                            end;
                            
                        end else escPressed := 0;
                        
                        if CRT_StartPressed then begin
                            repeat until not CRT_StartPressed;
                            Move(pointer(TXT_RAM_ADDRESS+40),pointer(REVEAL_BUFFER),80);
                            Move(pointer(PAUSETEXT_ADDRESS),pointer(TXT_RAM_ADDRESS+40),80);
                            repeat 
                                WaitFrame;
                                topBarForeground := sysframe or 7;
                            until IsStartingButtonDown;
                            Move(pointer(REVEAL_BUFFER),pointer(TXT_RAM_ADDRESS+40),80);
                            repeat until not IsStartingButtonDown;
                        end;
                        
                        
                        if CRT_OptionPressed then
                            if (actionDelay = 0) then begin 
                                music_ingame := not music_ingame;
                                StopSong;
                                if music_ingame then PlaySong(MUSIC_LEVEL)
                                    else PlaySong(MUSIC_SILENT);
                                actionDelay := 20;
                            end;
                            
                        if CRT_SelectPressed and music_ingame then
                            if (actionDelay = 0) then begin 
                                Inc(music_track);
                                if music_track>4 then music_track := 0;
                                StopSong;
                                LoadWorld(music_track);
                                banks[world.bank] := 0;
                                unApl(pointer(world.rmtPtr), pointer(RMT_MODULE_ADDRESS));
                                LoadWorld(worldCurrent);
                                PlaySong(MUSIC_LEVEL);
                                actionDelay := 20;
                            end;
                    end;
                
                until game_over or room_changed or exploded;

                // antic off
                WaitFrame;
                DisplayOff;


            until game_over;
        
        FadeOut;
        StopSong;

        end;


    until false;
    
{$ENDIF}    

    if false then msx.Play;    
    
end.

