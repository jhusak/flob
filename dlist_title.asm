; It's always useful to include you program global constants here
    icl 'const.inc'
DL_DLI = %10000000; // Order to run DLI

display_list_title
  dta $70, $70, $70, $f0, $4d, a(VIDEO_RAM_ADDRESS), $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d + DL_DLI, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d, $0d
  dta $0d, $0d, $0d, $0d, $0d, $0d, $0d, $41, a(display_list_title)
